package cn.lxm.iMail.Email;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xbill.DNS.*;
import org.xbill.DNS.Record;

import cn.lxm.iMail.UI.Login;
import javafx.application.Platform;
import javafx.scene.control.Label;

public class AccountManage {

	public static void main(String[] arges) {
		// AccountManage AccountManage=new AccountManage();
		// System.out.println(AccountManage.accountIsExit("123546@qq.com"));
	}

	public AccountManage() {

	}

	// 检测该用户是否存在
	public boolean accountIsExit(String account) {
		System.out.println("正在解析域名");
		String domain = "verify-email.org";
		// 解析域名
		String host = account.substring(account.indexOf('@') + 1);
		Socket socket = new Socket();
		// 通过DNS组件，查找MX记录(邮件交换记录)
		try {
			// 查找mx记录
			Record[] mxRecords = new Lookup(host, Type.MX).run();
			// 查找记录为空的话
			try {
				if (mxRecords == null)
					return false;
			} catch (Exception e) {
				;
			}

			// 邮件服务器地址
			String mxHost = ((MXRecord) mxRecords[0]).getTarget().toString();
			if (mxRecords.length > 1) { // 优先级排序
				List<Record> RecordsList = new ArrayList<Record>();
				Collections.addAll(RecordsList, mxRecords);
				Collections.sort(RecordsList, new Comparator<Record>() {
					@Override
					public int compare(Record r1, Record r2) {
						return (((MXRecord) r1).getPriority()) - (((MXRecord) r2).getPriority());
						// return new CompareToBuilder().append(((MXRecord)
						// o1).getPriority(), ((MXRecord)
						// o2).getPriority()).toComparison();
					}

				});
				// 获取到该域名下优先级最高的邮件服务器主机
				mxHost = ((MXRecord) RecordsList.get(0)).getTarget().toString();
			}
			// 开始smtp
			socket.connect(new InetSocketAddress(mxHost, 25));
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(new BufferedInputStream(socket.getInputStream())));
			BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			// 超时时间(毫秒)
			long timeout = 6000;
			// 睡眠时间片段(50毫秒)
			int sleepSect = 50;

			// 连接(服务器是否就绪)
			if (getResponseCode(timeout, sleepSect, bufferedReader) != 220) {
				return false;
			}

			// 握手
			bufferedWriter.write("HELO " + domain + "\r\n");
			bufferedWriter.flush();
			if (getResponseCode(timeout, sleepSect, bufferedReader) != 250) {
				return false;
			}
			// 身份
			bufferedWriter.write("MAIL FROM: <check@" + domain + ">\r\n");
			bufferedWriter.flush();
			if (getResponseCode(timeout, sleepSect, bufferedReader) != 250) {
				return false;
			}
			// 验证
			bufferedWriter.write("RCPT TO: <" + account + ">\r\n");
			bufferedWriter.flush();
			if (getResponseCode(timeout, sleepSect, bufferedReader) != 250) {
				return false;
			}
			// 断开
			bufferedWriter.write("QUIT\r\n");
			bufferedWriter.flush();
			Platform.runLater(new Runnable() {
				public void run() {
					((Label)Login.root.lookup("#lab_tips")).setText("已经验证该邮箱存在");}
			});

			return true;
		} catch (NumberFormatException e) {
		} catch (TextParseException e) {
		} catch (IOException e) {
		} catch (InterruptedException e) {
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
			}
		}
		return false;
	}

	/* 邮件服务器返回一个响应码 */
	private int getResponseCode(long timeout, int sleepSect, BufferedReader bufferedReader)
			throws InterruptedException, NumberFormatException, IOException {
		int code = 0;
		for (long i = sleepSect; i < timeout; i += sleepSect) {
			Thread.sleep(sleepSect);
			if (bufferedReader.ready()) {
				String outline = bufferedReader.readLine();
				while (bufferedReader.ready())
					/* System.out.println( */bufferedReader.readLine()/* ) */;
				/* System.out.println(outline); */
				System.out.println(outline);
				code = Integer.parseInt(outline.substring(0, 3));
				System.out.println(code);
				break;
			}
		}
		return code;
	}

	// 正则表达式分割邮箱是否合法
	public static boolean IsValid(String account) {
		String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
		Pattern regex = Pattern.compile(check);
		Matcher matcher = regex.matcher(account);
		return matcher.matches();
	}

	@SuppressWarnings("deprecation")
	public static boolean IsConnectToPOP3Server(String pop3Server, String name, String psw) {

		Socket sc = null;
		try {

			pop3Server = "pop." + pop3Server;
			sc = new Socket(pop3Server, 110);
			DataInputStream input = new DataInputStream(sc.getInputStream());
			DataOutputStream out = new DataOutputStream(sc.getOutputStream());
			System.out.println(input.readLine());
			out.writeBytes("user " + name + "\r\n");
			System.out.println(input.readLine());
			out.writeBytes("pass " + psw + "\r\n");
			String tipsString = input.readLine();
			System.out.println(tipsString);
			if (tipsString.contains("+OK"))
			{
				out.writeBytes("quit \r\n");
				sc.close();
				return true;
			}
			else
			{
				out.writeBytes("quit \r\n");
				sc.close();
				return false;
			}

		} catch (UnknownHostException e) {
			try {
				sc.close();
			} catch (IOException e4) {

				e.printStackTrace();
			}
			e.printStackTrace();
		} catch (IOException e) {
			try {
				sc.close();
			} catch (IOException e2) {

				e.printStackTrace();
			}
			e.printStackTrace();
		}

		return false;
	}

}
