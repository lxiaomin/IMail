package cn.lxm.iMail.Email;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

import cn.lxm.iMail.Hibernate.DAO.LoginlogDAO;
import cn.lxm.iMail.Hibernate.DAO.ReceiveDAO;
import cn.lxm.iMail.Hibernate.model.Loginlog;
import cn.lxm.iMail.Hibernate.model.Receive;
import cn.lxm.iMail.Logic.UIEvents;
import cn.lxm.iMail.UI.Login;
import cn.lxm.iMail.UI.NewEmailBox;
import cn.lxm.iMail.Util.Log;
import cn.lxm.iMail.Util.ReadFile;
import javafx.application.Platform;
import javafx.stage.Stage;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import com.sun.corba.se.impl.orbutil.closure.Future;
import com.sun.corba.se.impl.orbutil.threadpool.TimeoutException;
import com.sun.org.apache.regexp.internal.recompile;

public class ConnectWithPop3 {

	static LoginlogDAO lldDao = new LoginlogDAO();
	static int q = 0;
	static boolean timeout = false;// 定时器
	public static int TotalCount=0;
	LoginlogDAO lgDAO=new LoginlogDAO();
	public ConnectWithPop3() {

	}

	public static void getEmailsCountFromServer(EmailUser eu) {
		// 当前邮箱邮件数量
		int countInBox = 0;
		try {
			// 查询服务器的邮件数量是否和原来的邮件数量一致
			Socket sc = new Socket(eu.host, 110);
			DataInputStream input = new DataInputStream(sc.getInputStream());
			DataOutputStream out = new DataOutputStream(sc.getOutputStream());
			input.readLine();
			out.writeBytes("user " + eu.getUserName() + "\r\n");
			input.readLine();
			out.writeBytes("pass " + eu.getUserPsw() + "\r\n");
			input.readLine();
			out.writeBytes("stat" + "\r\n");
			String temp[] = input.readLine().split(" ");
			int count = Integer.parseInt(temp[1]);
			eu.setEmailCount(count);
			// 先关闭本次连接
			out.writeBytes("quit \r\n");
			try {
				countInBox = Integer.parseInt(lldDao.getByEmail(eu.getUserName()).getLoginTime());
			} catch (Exception e) {
				countInBox = 0;
			}
			if (countInBox < count) {
				// 如果邮箱中有新邮件
				ReFlashEmailBox(count - countInBox, eu);

			} else {
				// 没有新邮件，则仍载入原来的邮件
				try {
					UIEvents.LoadEmails(eu);
				} catch (Exception e) {

					e.printStackTrace();
				}

			}
			// 保存登陆记录
			Loginlog lg = new Loginlog();
			lg.setLoginTime(String.valueOf(count));
			lg.setToEmail(eu.getUserName());
			setNewLoginLog(lg);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 刷新邮件，将新邮件存储到数据库中

	public static void ReFlashEmailBox(int Origalcount, EmailUser eu) {
		// 服务器回应内容
		String reply = "";
		// 一行记录
		Receive receive = null;
		// DAO类
		ReceiveDAO rDao = new ReceiveDAO();
		receive = new Receive();
		try {

			// 查询服务器的邮件数量是否和原来的邮件数量一致
			System.out.println(eu.getEmailCount());
			System.out.println(eu.getHost());
			Socket sc = new Socket(eu.host, 110);
			DataInputStream input = new DataInputStream(sc.getInputStream());
			DataOutputStream out = new DataOutputStream(sc.getOutputStream());
			input.readLine();
			out.writeBytes("user " + eu.getUserName() + "\r\n");
			input.readLine();
			out.writeBytes("pass " + eu.getUserPsw() + "\r\n");
			input.readLine();
			out.writeBytes("stat " + "\r\n");
			String temp[] = input.readLine().split(" ");
			int count = Integer.parseInt(temp[1]);
			TotalCount=count;
			System.out.println("现在邮箱服务器的邮件数是："+count);
			eu.setEmailCount(count);
			if (count > Origalcount) {
				for (int i = count; i > Origalcount; i--) {
					timeout = false;
					reply = "";
					// System.out.println(reply);
					receive.setTitle(reply);
					receive.setFromEmail("");
					receive.setToEmail(eu.getUserName());
					short s = 1;
					receive.setTime(String.valueOf(i));
					receive.setAppendix(s);
					receive.setIsInReceiveBox(0);
					receive.setIsInFlagBox(0);
					receive.setIsInChangeBox(0);
					receive.setIsInTrashBox(0);
					receive.setIsInDeleteBox(0);
					out.writeBytes("retr " + i + "\r\n");
					// 清空reply
					reply = "";
					q = 0;
					while (true) {
						String tempString = input.readLine();
						q++;
						if (timeout || q > 2000) {
							timeout = true;
							break;
						}
						reply += tempString + "\n";
						// System.out.println(tempString);
						// POP有超时限制，设置定时器，超时则退出这封邮件接收
						if (tempString.toLowerCase().equals(".")) {
							break;
						}
					}
					;
					if (!timeout) {
						// 把保存文件内容改为保存eml文件路径
						String path = ReadFile.WriteEMLFile(reply);
						receive.setEmailContent(path);
						rDao.save(receive);
						System.out.println(i);
					} else {
						// 销毁线程

					}
					timeout = false;
				}
				//更新邮件数量，重新载入邮件
				eu.setEmailCount(count);
				UIEvents.LoadEmails(eu);
				//弹出邮件提醒
				NewEmailTips();

				Loginlog lg = lldDao.getByEmail(eu.getUserName());
				lg.setLoginTime(String.valueOf(count));
				lldDao.update(lg);


			} else {
				Loginlog lg = lldDao.getByEmail(eu.getUserName());
				System.out.println("现在是设置该用户的邮件："+String.valueOf(count));
				lg.setLoginTime(String.valueOf(count));
				System.out.println("现在是刷新该用户的邮件："+Login.eu.getUserName());
				lldDao.update(lg);
				out.writeBytes("quit \r\n");
				sc.close();
				return;
			}
			out.writeBytes("quit \r\n");
			sc.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void NewEmailTips() throws Exception {

		Email email = EmailFJ.DecomposeEmail(UIEvents.list.get(UIEvents.list.size()-1).getEmailContent());
		Platform.runLater(new Runnable() {
			public void run() {
				try {
					new NewEmailBox(Login.eu.getUserName(),email.getFrom(),email.getReceived()).start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});

	}

	public static void setNewLoginLog(Loginlog lg) {
		lldDao.update(lg);
	}

	// 第一次登陆的用户将所有邮件下载到数据库，用N个线程去跑，节约时间，但是增大了开销。还会造成删除时候的错误。
	public static void downLoadAllEmailFromPOP3(EmailUser eu) throws InterruptedException, ExecutionException {
		// 服务器回应内容
		String reply = "";
		// 一行记录
		Receive receive = null;
		// DAO类
		ReceiveDAO rDao = new ReceiveDAO();
		receive = new Receive();
		try {

			// 查询服务器的邮件数量是否和原来的邮件数量一致
			Socket sc = new Socket(eu.host, 110);
			DataInputStream input = new DataInputStream(sc.getInputStream());
			DataOutputStream out = new DataOutputStream(sc.getOutputStream());
			input.readLine();
			out.writeBytes("user " + eu.getUserName() + "\r\n");
			input.readLine();
			out.writeBytes("pass " + eu.getUserPsw() + "\r\n");
			input.readLine();
			out.writeBytes("stat " + "\r\n");
			String temp[] = input.readLine().split(" ");
			int count = Integer.parseInt(temp[1]);
			TotalCount=count;
			for (int i = 1; i <= count; i++) {
				timeout = false;

				reply = "";
				// System.out.println(reply);
				receive.setTitle(reply);
				receive.setFromEmail("");
				receive.setToEmail(eu.getUserName());
				short s = 0;
				receive.setTime(String.valueOf(i));
				receive.setAppendix(s);
				receive.setIsInReceiveBox(0);
				receive.setIsInFlagBox(0);
				receive.setIsInChangeBox(0);
				receive.setIsInTrashBox(0);
				receive.setIsInDeleteBox(0);
				out.writeBytes("retr " + i + "\r\n");
				input.readLine();
				// 清空reply
				reply = "";
				// q是做下载计数器的，读了2000行没有反应的话就放弃下载这封邮件，避免与POP3服务器失去连接
				q = 0;

				while (true) {
					String tempString = input.readLine();
					q++;
					if (timeout || q > 2000) {
						System.out.println(q);
						timeout = true;
						out.writeBytes("quit \r\n");
						sc.close();//关闭原来的端口，以快速新占用这个端口
						Thread.sleep(20000);
						System.out.println("睡眠唤醒");
						try {
							sc = new Socket(eu.host, 110);
						} catch (Exception e) {
							Thread.sleep(20000);
							sc = new Socket(eu.host, 110);
						}

						System.out.println("第二次链接");
						input = new DataInputStream(sc.getInputStream());
						out = new DataOutputStream(sc.getOutputStream());
						System.out.println(input.readLine());
						out.writeBytes("user " + eu.getUserName() + "\r\n");
						System.out.println(input.readLine());
						out.writeBytes("pass " + eu.getUserPsw() + "\r\n");
						System.out.println(input.readLine());
						out.writeBytes("stat " + "\r\n");
						System.out.println(input.readLine());
						tempString = ".";
						reply = "";
						break;

					}
					reply += tempString + "\n";
					// POP有超时限制，设置定时器，超时则退出这封邮件接收
					try {
						if (tempString.toLowerCase().equals(".")) {
							break;
						}
					} catch (Exception e) {
						reply="";
						timeout=true;
						break;
					}

				}
				if (!timeout) {
					// 把保存文件内容改为保存eml文件路径
					String path = ReadFile.WriteEMLFile(reply);
					receive.setEmailContent(path);
					rDao.save(receive);
				}
				timeout = false;
			}

			System.out.println(input.readLine());
			out.writeBytes("quit \r\n");
			System.out.println("退出");
			sc.close();//关闭原来的端口，以快速新占用这个端口

		} catch (Exception e) {
			e.printStackTrace();

		}
	}


	//根据信件ID删除邮件
	public static boolean deleteEmailByID(EmailUser eu,int ID)
	{

		Log log =new Log();
		try {
			Socket sc = new Socket(eu.host, 110);
			DataInputStream input = new DataInputStream(sc.getInputStream());
			DataOutputStream out = new DataOutputStream(sc.getOutputStream());
			log.log(input.readLine());
			out.writeBytes("user " + eu.getUserName() + "\r\n");
			log.log(input.readLine());
			out.writeBytes("pass " + eu.getUserPsw() + "\r\n");
			log.log(input.readLine());
			out.writeBytes("dele "+ID+" " + "\r\n");
			log.log(input.readLine());
			out.writeBytes("quit \r\n");
			log.log(input.readLine());
			System.out.println("删除成功");
			return true;

		} catch (Exception e) {
			return false;
		}
	}
	private static void Timer() {

	}

	public static void timer1() {

	}
}
