package cn.lxm.iMail.Email;

public class Email {

	String received;

	String From;
	String To;
	String Subject;
	String Mine_Version;
	String Content_type;
	String Content_Transfer_Encoding;
	String Time;
	String Content;
	String host;
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public Email() {

	}
	public String getReceived() {
		return received;
	}
	public void setReceived(String received) {
		this.received = received;
	}
	public String getFrom() {
		return From;
	}
	public void setFrom(String from) {
		From = from;
	}
	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}
	public String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public String getMine_Version() {
		return Mine_Version;
	}
	public void setMine_Version(String mine_Version) {
		Mine_Version = mine_Version;
	}
	public String getContent_type() {
		return Content_type;
	}
	public void setContent_type(String content_type) {
		Content_type = content_type;
	}
	public String getContent_Transfer_Encoding() {
		return Content_Transfer_Encoding;
	}
	public void setContent_Transfer_Encoding(String content_Transfer_Encoding) {
		Content_Transfer_Encoding = content_Transfer_Encoding;
	}
	public String getTime() {
		return Time;
	}
	public void setTime(String time) {
		Time = time;
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	public Email(String received, String from, String to, String subject, String mine_Version, String content_type,
			String content_Transfer_Encoding, String time, String content) {
		super();
		this.received = received;
		From = from;
		To = to;
		Subject = subject;
		Mine_Version = mine_Version;
		Content_type = content_type;
		Content_Transfer_Encoding = content_Transfer_Encoding;
		Time = time;
		Content = content;
	}
	public Email(String from, String to, String subject, String time, String content) {
		super();
		From = from;
		To = to;
		Subject = subject;
		Time = time;
		Content = content;
	}



}
