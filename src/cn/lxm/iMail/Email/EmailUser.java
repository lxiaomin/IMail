package cn.lxm.iMail.Email;

public class EmailUser {

	String userName = "";

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPsw() {
		return userPsw;
	}

	public void setUserPsw(String userPsw) {
		this.userPsw = userPsw;
	}

	String userPsw = "";

	int EmailCount;

	public int getEmailCount() {
		return EmailCount;
	}

	public void setEmailCount(int emailCount) {
		EmailCount = emailCount;
	}

	String host = "";

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public EmailUser(String userName, String userPsw) {
		super();
		this.userName = userName;
		this.userPsw = userPsw;
	}

	public EmailUser() {

	}

}
