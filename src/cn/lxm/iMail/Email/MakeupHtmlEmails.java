package cn.lxm.iMail.Email;

import java.io.IOException;

import cn.lxm.iMail.Util.ReadFile;

public class MakeupHtmlEmails {

	public MakeupHtmlEmails() {

	}

	// 返回完成之后的路径
	public static String MakeupEmail(Email email) throws IOException {
		String path = "tinymce_4.5.5/tinymce/js/tinymce/viewer.html";
		// 读入邮件模板文件
		String template = ReadFile.readFileByChars("tinymce_4.5.5/tinymce/js/tinymce/viewer.html");
		String t1[] = template.split("598681919Title");
		template = "";
		template = t1[0] + email.Subject + t1[1];
		t1 = template.split("598681919Sender");
		template = "";
		template = t1[0] + email.From + t1[1];
		t1 = template.split("598681919recieve");
		template = "";
		template = t1[0] + email.To + t1[1];
		// template=t1[0]+email.From+t1[1];
		t1 = template.split("598681919Time1");
		template = "";
		template = t1[0] + email.Time + t1[1];
		t1 = template.split("598681919Time2");
		template = "";
		template = t1[0] + email.Time + t1[1];
		String temppath = "";
		if (email.Content != null) {
			temppath = ReadFile.WriteFileUTF_8_2(email.Content);
		}
		t1 = template.split("598681919Content");
		template = "";
		template = t1[0] + "src=\"" + temppath + "\"" + t1[1];
		if (email.Mine_Version != null) {
			String filesString[] = email.getMine_Version().split(";");
			if (filesString.length > 0) {
				System.out.println(filesString[0]);
				if (filesString[0].length()>2) {

					t1 = template.split("附件(无)");
					template = "";
					template = t1[0] + "<div onclick=\"downloadfile(this)\" class=\"attach-total\">附件(<span class=\"attach-len55\">"
							+ filesString.length
							+ "</span>) <a class=\"attach-save-btn\"  onclick=\"app.savefile()\" id=\"attach-save-btn55\" style=\"visibility: visible;\">保存全部</a></br>";
					for (int i = 0; i < filesString.length; i++) {
						template += "<a id=\"attdownload\"" + i + " href=\"../../../../" + filesString[i]
								+ "\"  download=\"" + filesString[i] + "\" >" + filesString[i] + "</a>";
					}
					template += "</div>";
				}
			}

		}
		path = ReadFile.WriteFileUTF_8(template);
		return path;
	}
}
