package cn.lxm.iMail.Email;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;

import cn.lxm.iMail.Hibernate.DAO.SendDAO;
import cn.lxm.iMail.Hibernate.model.Send;
import cn.lxm.iMail.UI.Login;
import cn.lxm.iMail.Util.Base64Encode;
import cn.lxm.iMail.Util.Log;
import cn.lxm.iMail.Util.MimeTypeFactory;
import cn.lxm.iMail.Util.ReadFile;
import cn.lxm.iMail.Util.ResponseCodeMeaning;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public class SmtpSendMails {

	private final static int SMTP_PORT = 25;
	private final static String BOUNDARY = "--=_PartSpilt_lxm_1014_";// MIME分格符,分割各个MIME消息
	private final static String CHARSET = "GB2312";// 邮件发送的编码
	private final static int RERTY_TIME = 3;
	private final static long SLEEP_TIME = 5000; // 睡眠时间。
	private String smtp;// SMTP服务器地址
	private String user;// 用户名
	private String password;// 密码
	private String sender;// 发件人名字
	private String senderAddress;// 发送人邮箱
	private String receiverAddress;// 发送人邮箱
	private static Log log = new Log();
	private ResponseCodeMeaning getCodeMeaning = new ResponseCodeMeaning();
	static SendDAO sendDAO ;//存储邮件的DAO层实现

	public SmtpSendMails() {

	}

	public static Email createSMTPMail(String SMTPhost, String from, String to) {
		Email email = new Email();
		email.setFrom(from);
		email.setTo(to);
		email.setHost(SMTPhost);
		return email;
	}

	/* 邮件服务器返回一个响应码 */
	private int getResponseCode(long timeout, int sleepSect, BufferedReader bufferedReader)
			throws InterruptedException, NumberFormatException, IOException {
		int code = 0;
		for (long i = sleepSect; i < timeout; i += sleepSect) {
			Thread.sleep(sleepSect);
			if (bufferedReader.ready()) {
				String outline = bufferedReader.readLine();
				while (bufferedReader.ready())
					/* System.out.println( */bufferedReader.readLine()/* ) */;
				/* System.out.println(outline); */
				// System.out.println(outline);
				log.log(outline);
				code = Integer.parseInt(outline.substring(0, 3));
				// System.out.println(code);
				log.log("响应码为：" + code);
				break;
			}
		}
		return code;
	}

	public boolean sendMail(EmailUser eu, String[] to, String subject, String content, File[] attachments,
			boolean isHtml, boolean isUrgent) throws IllegalArgumentException, IOException, InterruptedException {

		Socket socket = null;
		InputStream in = null;
		OutputStream out = null;
		receiverAddress=to[0];
		boolean needBoundary = attachments != null && attachments.length > 0;// 是否分割
		byte[] data;
		String host = "smtp" + eu.getHost().split("pop3")[1];
		System.out.println(host);
		// 尝试连接STMP服务器主机,超出重连次数则链接失败
		for (int i = 0;; i++) {
			try {
				log.log("连接: 主机:\"" + host + "\" 端口:\"" + SMTP_PORT + "\"");
				socket = new Socket(host, SMTP_PORT);
				log.log("连接成功");
				break;
			} catch (Exception e) {
				e.printStackTrace();
				log.log("第" + i + "次重新连接");
				Thread.sleep(SLEEP_TIME);
				if (i == RERTY_TIME) {
					log.log("连接失败,可能是网络原因");
					return false;
				}
			}

		}
		// 获取输入输出流，简历输出输入缓冲区
		in = socket.getInputStream();
		out = socket.getOutputStream();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(new BufferedInputStream(socket.getInputStream()), "gbk"));
		BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "gbk"));
		// 第一次响应
		int code = getResponseCode(SLEEP_TIME, RERTY_TIME, bufferedReader);
		log.log("服务器响应代码 " + code + ":" + getCodeMeaning.match(code));

		if (code != 220) {
			log.log("失败");
			return false;
		}

		// log.log("检测收件人地址是否存在");//由于在之前已经验证了所以这里不用再次验证
		// 和服务器问候
		bufferedWriter.write("HELO " + host + "\r\n");
		bufferedWriter.flush();
		code = getResponseCode(SLEEP_TIME, RERTY_TIME, bufferedReader);
		log.log("服务器响应代码 " + code + ":" + getCodeMeaning.match(code));

		if (code != 250) {
			log.log("失败");
			return false;
		}
		/* 登陆 */
		log.log("登陆开始");
		bufferedWriter.write("AUTH LOGIN \r\n");
		bufferedWriter.flush();
		code = getResponseCode(SLEEP_TIME, RERTY_TIME, bufferedReader);
		log.log("服务器响应代码 " + code + ":" + getCodeMeaning.match(code));
		if (code != 334) {
			log.log("失败");
			return false;
		}

		/* 用户名 */
		log.log("用户名开始");
		bufferedWriter.write(Base64.encode(eu.getUserName().getBytes()) + " \r\n");
		bufferedWriter.flush();
		code = getResponseCode(SLEEP_TIME, RERTY_TIME, bufferedReader);
		log.log("服务器响应代码 " + code + ":" + getCodeMeaning.match(code));
		if (code != 334) {
			log.log("失败");
			return false;
		}

		/* 密码 */
		log.log("密码开始");
		bufferedWriter.write(Base64.encode(eu.getUserPsw().getBytes()) + " \r\n");
		bufferedWriter.flush();
		code = getResponseCode(SLEEP_TIME, RERTY_TIME, bufferedReader);
		log.log("服务器响应代码 " + code + ":" + getCodeMeaning.match(code));
		if (code != 235) {
			log.log("失败");
			return false;
		}

		/* From 发件人 */
		log.log("发件人开始");
		bufferedWriter.write("MAIL FROM: <" + eu.getUserName() + ">\r\n");
		bufferedWriter.flush();
		code = getResponseCode(SLEEP_TIME, RERTY_TIME, bufferedReader);
		log.log("服务器响应代码 " + code + ":" + getCodeMeaning.match(code));
		if (code != 250) {
			log.log("失败");
			return false;
		}

		/* To 收件人 */
		log.log("收件人开始");
		for (int i = 0; i < to.length; i++) {
			log.log("第" + i + "个收件人");
			bufferedWriter.write("RCPT TO: <" + to[i] + ">\r\n");
			System.out.println("RCPT TO: <" + to[i] + ">");
			bufferedWriter.flush();
			code = getResponseCode(SLEEP_TIME, RERTY_TIME, bufferedReader);
			log.log("服务器响应代码 " + code + ":" + getCodeMeaning.match(code));
			if (code != 250) {
				log.log("失败");
				return false;
			}
		}

		/* 邮件传输开始 */
		log.log("邮件传输开始");
		bufferedWriter.write("DATA" + "\r\n");
		System.out.println("DATA" + "\r\n");
		bufferedWriter.flush();
		code = getResponseCode(SLEEP_TIME, RERTY_TIME, bufferedReader);
		log.log("服务器响应代码 " + code + ":" + getCodeMeaning.match(code));
		if (code != 354) {
			log.log("失败");
			return false;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("From: " + eu.getUserName() + "\r\n");
		sb.append("To: " + to[0] + "\r\n");
		sb.append("Subject: " + subject + "\r\n");
		sb.append("Date: " + new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z (z)", Locale.CHINA).format(new Date())
				+ "\r\n");
		sb.append("DMIME-Version: 1.0" + "\r\n");
		if (needBoundary) {
			sb.append("Content-Type: multipart/mixed; BOUNDARY=\"" + BOUNDARY + "\"" + "\r\n");
		} else {
			if (isHtml) {
				sb.append("Content-Type: text/html; charset=\"" + CHARSET + "\"" + "\r\n");
			} else {
				sb.append("Content-Type: text/plain; charset=\"" + CHARSET + "\"" + "\r\n");
			}
		}

		if (isUrgent) {
			sb.append("X-Priority: 1\r\n");
		} else {
			sb.append("-Priority: 1\r\n");
		}
		if (!needBoundary)
		{
			sb.append("Content-Transfer-Encoding: base64" + "\r\n");
			sb.append("\r\n");
			sb.append(Base64.encode(content.getBytes()));
			sb.append("\r\n");
			sb.append("\r\n");

		}


		if (needBoundary) {
			sb.append("\r\n");
			sb.append("--" + BOUNDARY + "\r\n");
			sb.append("type=\"multipart/alternative\"");
			sb.append("Content-Transfer-Encoding: base64" + "\r\n");
			if (isHtml) {
				sb.append("Content-Type: text/html; charset=\"" + CHARSET + "\"" + "\r\n");
			} else {
				sb.append("Content-Type: text/plain; charset=\"" + CHARSET + "\"" + "\r\n");
			}
			sb.append("Content-Transfer-Encoding: base64" + "\r\n");
			sb.append("\r\n");
			sb.append(Base64.encode(content.getBytes()));
			sb.append("\r\n");
			sb.append("\r\n");
			sb.append("--" + BOUNDARY + "\r\n");
			//正文结束开始附件E
			RandomAccessFile attachment = null;// 访问那些保存数据记录的文件的
			int fileIndex = 0;
			String fileName;
			int k;
			data = new byte[64];

			try {
				for (; fileIndex < attachments.length; fileIndex++) {
					sb.append("Content-Transfer-Encoding: base64" + "\r\n");
					fileName = attachments[fileIndex].getName();
					attachment = new RandomAccessFile(attachments[fileIndex], "r");
					sb.append("Content-Type: "
							+ MimeTypeFactory.getMimeType(fileName.indexOf(".") == -1 ? "*"
									: fileName.substring(fileName.lastIndexOf(".") + 1))
							+ "; name=\"" + fileName + "\"" + "\r\n");
					sb.append("Content-Disposition: attachment; filename=\"" + fileName + "\"" + "\r\n");
					/* 将文件编码成base64 */
					sb.append("\r\n");
					String sb2=new String();
					sb2=Base64.encode(getBytes(attachments[fileIndex].getPath()));
					int k2=0;
					/*大文件分段*/
					for(int i =0;i<sb2.length();i++)
					{
						if(i!=0 && i%54==0)
							sb.append("\r\n");
						sb.append(sb2.charAt(i));
					}
					//sb.append(Base64.encode(getBytes(attachments[fileIndex].getPath())) + "\r\n");
					sb.append("\r\n");
					sb.append("--" + BOUNDARY + "\r\n");
				}
			} catch (FileNotFoundException e) {
				log.log("错误: 附件\"" + attachments[fileIndex].getAbsolutePath() + "\"不存在");
				return false;
			} catch (IOException e) {
				log.log("错误: 无法读取附件\"" + attachments[fileIndex].getAbsolutePath() + "\"");
				return false;
			} finally {
				if (attachment != null) {
					try {
						attachment.close();
					} catch (IOException e) {
					}
				}
			}



		}


		sb.append(".\r\n"); //纯文本邮件就要这个
		//sb.append(".\r\n");
		System.out.println(sb.toString());
		//保存所发送的邮件
		bufferedWriter.write(sb.toString());
		bufferedWriter.flush();
		code = getResponseCode(SLEEP_TIME, RERTY_TIME, bufferedReader);
		log.log("服务器响应代码 " + code + ":" + getCodeMeaning.match(code));
		if (code != 250) {
			log.log("失败");
			return false;
		}

		bufferedWriter.write("QUIT" + "\r\n");
		bufferedWriter.flush();

		code = getResponseCode(SLEEP_TIME, RERTY_TIME, bufferedReader);
		log.log("服务器响应代码 " + code + ":" + getCodeMeaning.match(code));
		if (code != 221) {
			log.log("失败");
			return false;
		}

		if (in != null) {
			try {
				in.close();
			} catch (IOException e) {
			}
		}

		if (out != null) {
			try {
				out.close();
			} catch (IOException e) {
			}
		}

		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e) {
			}
		}
		//保存成功发送的邮件
		saveSendEmail(sb.toString());
		return true;
	}

	//保存已发邮件的eml文件的路径
	 private void saveSendEmail(String content) throws IOException {
		 	String tmp1[]=content.split("Subject:");
		 	String tmp2[]=tmp1[1].split("Date:");
		 	content=tmp1[0]+"Subject: =?gbk?b?"+Base64.encode(tmp2[0].getBytes())+"?="+"\nDate:"+tmp2[1];
		    sendDAO = new SendDAO();
			Send ISendEmail = new Send();
			ISendEmail.setSenderEmail(Login.eu.getUserName());
			String path=ReadFile.WriteEMLFile(content);
			ISendEmail.setEmailContent(path);
			ISendEmail.setTitle("");
			ISendEmail.setIsInSendBox(0);
			ISendEmail.setIsInAreadySendBox(1);
			ISendEmail.setReciverEmail(receiverAddress);
			ISendEmail.setAppendix((short) 0);
			ISendEmail.setSendTime((new Date().toLocaleString()));
			sendDAO.save(ISendEmail);


	}

	public static byte[] getBytes(String filePath){
	        byte[] buffer = null;
	        try {
	            File file = new File(filePath);
	            FileInputStream fis = new FileInputStream(file);
	            ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
	            byte[] b = new byte[1000];
	            int n;
	            while ((n = fis.read(b)) != -1) {
	                bos.write(b, 0, n);
	            }
	            fis.close();
	            bos.close();
	            buffer = bos.toByteArray();
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return buffer;
	    }

}
