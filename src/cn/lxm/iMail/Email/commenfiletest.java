package cn.lxm.iMail.Email;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.mail.SimpleEmail;
import org.apache.commons.mail.util.MimeMessageParser;
import org.apache.commons.mail.util.MimeMessageUtils;

import com.sun.management.jmx.Trace;

public class commenfiletest {
	private static final String folder = "res\\atta";
	public commenfiletest() {

	}

	public static void main(String[] args) {

		File file = new File("D:\\Eclipce4.51SDK\\newWorkspace\\IMail\\eml\\asd-.eml");
        try {
            MimeMessage mimeMessage = MimeMessageUtils.createMimeMessage(null, file);
            MimeMessageParser parser = new MimeMessageParser(mimeMessage);
            System.out.println("发件人："+parser.getFrom());
            System.out.println("主题："+parser.getSubject());
            System.out.println("收件人："+parser.getTo());
            System.out.println("发件人2："+parser.parse().getBcc());
            System.out.println("发件人3："+parser.parse().getCc());
            System.out.println("发件人3："+parser.parse().getReplyTo());
            System.out.println("发件人4："+parser.parse().getContentIds());
            System.out.println(parser.parse().getPlainContent());  //结果为空
            System.out.println(parser.parse().getHtmlContent()); //结果为空

            List<DataSource> attachments = parser.getAttachmentList(); // 获取附件，并写入磁盘
            ArrayList<String> NameList=new ArrayList<String>();
			for (DataSource ds : attachments) {
				BufferedOutputStream outStream = null;
				BufferedInputStream ins = null;
				try {
					String fileName = folder + File.separator + ds.getName();
					if(fileName.equals(folder + File.separator +"null"))  continue;
					if(NameList.contains(fileName)) continue;
					outStream = new BufferedOutputStream(new FileOutputStream(fileName));
					ins = new BufferedInputStream(ds.getInputStream());
					byte[] data = new byte[2048];
					int length = -1;
					while ((length = ins.read(data)) != -1) {
						outStream.write(data, 0, length);
					}
					outStream.flush();
					System.out.println("附件:" + fileName);
					NameList.add(fileName);
				} finally {
					if (ins != null) {
						ins.close();
					}
					if (outStream != null) {
						outStream.close();
					}
				}
			}
           // parser.parse();//方法返回的也是MimeMessageParser对象，不调用parse()方法就无法的到邮件内容，只能得到主题和收件人等信息
            if(parser.parse().hasPlainContent()){  //这里一定要调用parser.parse方法
                System.out.println(parser.parse().getPlainContent());
            }



        } catch (MessagingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
	}

}
