package cn.lxm.iMail.Email;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.mail.util.MimeMessageParser;
import org.apache.commons.mail.util.MimeMessageUtils;

import com.sun.mail.handlers.text_html;

import cn.lxm.iMail.Util.ReadFile;

public class EmailFJ {

	/*
	 * 邮件分解
	 */
	static sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
	private static final String folder = "res\\atta";
	public EmailFJ() {

	}

	/* 拆分解析邮件，仅限于普通类型的带HTML文本的邮件，能解析出内容、发送者、发送时间、主题等主要数据。采用的是字段划分的方法。 */
	public static Email DecomposeEmail(String contentPath) throws Exception {
		Email email = new Email("", "", "", "", "", "", "", "", "");
		String content = ReadFile.readFileByChars(contentPath);
		try {
			email.setFrom(getEmail(content));
			email.setContent(getContent(content));
			email.setSubject(getSubject(content));
			email.setTime(getTime(content));
			email.setMine_Version(getAttach(content));
			email.setTo(getTo(content));

			/* 自己的解析失败，还是采用阿帕奇的Common Mail来解析EML文件吧 */
			if (email.getFrom().equals("") || email.getSubject().equals("") || email.getTo().length() > 40) {
				String emlPath = contentPath;
				File file = new File(emlPath);
				MimeMessage mimeMessage = MimeMessageUtils.createMimeMessage(null, file);
				MimeMessageParser parser = new MimeMessageParser(mimeMessage);
				if (email.getFrom().trim().equals(""))
					email.setFrom(parser.parse().getFrom());

				email.setSubject(parser.parse().getSubject());
				// email.setReceived(parser.parse().getPlainContent());
				email.setReceived(delHTMLTag(parser.parse().getHtmlContent()));
				email.setTo(parser.parse().getTo().get(0).toString());
				email.setContent(parser.parse().getHtmlContent());


				/*附件的处理*/
				 List<DataSource> attachments = parser.getAttachmentList(); // 获取附件，并写入磁盘
		            ArrayList<String> NameList=new ArrayList<String>();
					for (DataSource ds : attachments) {
						BufferedOutputStream outStream = null;
						BufferedInputStream ins = null;
						try {
							String fileName = folder + File.separator + ds.getName();
							if(fileName.equals(folder + File.separator +"null"))  continue;
							if(NameList.contains(fileName)) continue;
							outStream = new BufferedOutputStream(new FileOutputStream(fileName));
							ins = new BufferedInputStream(ds.getInputStream());
							byte[] data = new byte[2048];
							int length = -1;
							while ((length = ins.read(data)) != -1) {
								outStream.write(data, 0, length);
							}
							outStream.flush();
							//System.out.println("附件:" + fileName);
							NameList.add(fileName);

						} finally {
							if (ins != null) {
								ins.close();
							}
							if (outStream != null) {
								outStream.close();
							}
						}
					}
					String re="";
					if(NameList.size()>0)
					{
						Iterator<String> iterator=NameList.iterator();
						while (iterator.hasNext()) {
							String string = (String) iterator.next();
							re+=string+";";
						}
						email.setMine_Version(re);//将附件放到这个字段
					}
			}
		} catch (Exception e) {
			String emlPath = contentPath;
			File file = new File(emlPath);
			MimeMessage mimeMessage = MimeMessageUtils.createMimeMessage(null, file);
			MimeMessageParser parser = new MimeMessageParser(mimeMessage);
			if (email.getFrom().trim().equals(""))
				email.setFrom(parser.parse().getFrom());
			email.setSubject(parser.parse().getSubject());
			email.setReceived(delHTMLTag(parser.parse().getHtmlContent()));
			email.setTo(parser.parse().getTo().get(0).toString());
			email.setContent(parser.parse().getHtmlContent());
			/*附件的处理*/
			 List<DataSource> attachments = parser.getAttachmentList(); // 获取附件，并写入磁盘
	         ArrayList<String> NameList=new ArrayList<String>();
				for (DataSource ds : attachments) {
					BufferedOutputStream outStream = null;
					BufferedInputStream ins = null;
					try {
						String fileName = folder + File.separator + ds.getName();
						if(fileName.equals(folder + File.separator +"null"))  continue;
						if(NameList.contains(fileName)) continue;
						outStream = new BufferedOutputStream(new FileOutputStream(fileName));
						ins = new BufferedInputStream(ds.getInputStream());
						byte[] data = new byte[2048];
						int length = -1;
						while ((length = ins.read(data)) != -1) {
							outStream.write(data, 0, length);
						}
						outStream.flush();
						//System.out.println("附件:" + fileName);
						NameList.add(fileName);

					} finally {
						if (ins != null) {
							ins.close();
						}
						if (outStream != null) {
							outStream.close();
						}
					}
				}
				String re="";
				if(NameList.size()>0)
				{
					Iterator<String> iterator=NameList.iterator();
					while (iterator.hasNext()) {
						String string = (String) iterator.next();
						re+=string+";";
					}
					email.setMine_Version(re);//将附件放到这个字段
				}


		} finally {
			return email;
		}

	}

	//获取附件的
	private static String getAttach(String content) {

		return null;
	}

	public static void main(String[] args) throws Exception {
		// DecomposeEmail("Received:from cmpassport139@139.com
		// (unknown[172.16.202.6]) by rmoda-17134 (RichMail) with ODA id
		// 42ee542dcc45bdb-5c19e; Fri, 03 Oct 2014 06:05:57 +0800
		// (CST)X-RM-TRANSID:42ee542dcc45bdb-5c19eX-OP_RICHINFO_MAILTYPE:0X-RICHINFO:NOTIFYTYPE:0;ISWAPNOTIFY:0;BUSSINESSID:1633;MAILORDERID:149430;PRODUCTOPERATIONMAIL:2005;CHANNEL:30;CATEGORY:3068;ISMULTITEMPLATE:1;TEMPLATELIST:4;MJ:0;M:From:
		// =?gbk?b?u6XBqs34zajQ0Nak?= <cmpassport139@139.com>To:
		// 13557330070@139.com");
		// getContent("");
		// delHTMLTag("");
	}

	public static String getEmail(String txt) {
		String resultString = "";
		String temp[] = txt.split("From:");
		String temp2[] = temp[1].split("To:");
		String temp3[] = temp2[0].split("\\?");

		try {
			// 加上两个=，编码
			byte[] bStrings = decoder.decodeBuffer(temp3[3] + "");
			resultString = new String(bStrings);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(resultString);
		return resultString;
	}

	public static String getTo(String txt) {
		String resultString = "";
		String temp[] = txt.split("To:");
		String temp2[] = temp[1].split("Subject: ");
		resultString = temp2[0];
		// String temp3[]=temp2[0].split("\\?");
		//
		// try {
		// //加上两个=，编码
		// byte[] bStrings=decoder.decodeBuffer(temp3[3]+"");
		// resultString=new String(bStrings);
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// System.out.println(resultString);
		return resultString;
	}

	public static String getSubject(String txt) {
		String resultString = "";
		String temp[] = txt.split("Subject: ");
		String temp2[] = temp[1].split("=?=");
		String temp3[] = temp2[1].split("\\?");

		try {
			// 加上两个=，编码
			byte[] bStrings = decoder.decodeBuffer(temp3[3] + "");
			resultString = new String(bStrings);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(resultString);
		return resultString;
	}

	public static String getTime(String txt) {
		String resultString = "";
		String temp[] = txt.split("(CST)");
		String temp2[] = temp[0].split(",");
		String temp3[] = temp2[1].split("\\+0800");
		resultString = temp3[0];
		// System.out.println(resultString);
		try {
			String time[] = resultString.split("\\s+");
			int month = getMonth(time[2]);
			return time[3] + "-" + month + "-" + time[1] + " " + time[4];
		} catch (Exception e) {
			return resultString;
		}
	}

	private static int getMonth(String month) {
		if (month.equals("Jan"))
			return 1;
		if (month.equals("Feb"))
			return 2;
		if (month.equals("Mar"))
			return 3;
		if (month.equals("Apr"))
			return 4;
		if (month.equals("May"))
			return 5;
		if (month.equals("Jun"))
			return 6;
		if (month.equals("Jul"))
			return 7;
		if (month.equals("Aug"))
			return 8;
		if (month.equals("Sept"))
			return 9;
		if (month.equals("Oct"))
			return 10;
		if (month.equals("Nov"))
			return 11;
		if (month.equals("Dec"))
			return 12;
		return 0;
	}

	public static String getContent(String txt) {
		String resultString = "";
		String temp[] = txt.split("Content-Transfer-Encoding: base64");
		resultString = temp[temp.length - 1];
		try {
			byte[] bStrings = decoder.decodeBuffer(resultString);
			resultString = new String(bStrings);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(resultString);
		return resultString;
	}

	// 去HTML标签，获得纯文本文件
	public static String delHTMLTag(String htmlStr) {
		try {
			String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
			String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
			String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式

			Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
			Matcher m_script = p_script.matcher(htmlStr);
			htmlStr = m_script.replaceAll(""); // 过滤script标签

			Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
			Matcher m_style = p_style.matcher(htmlStr);
			htmlStr = m_style.replaceAll(""); // 过滤style标签

			Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
			Matcher m_html = p_html.matcher(htmlStr);
			htmlStr = m_html.replaceAll(""); // 过滤html标签
			htmlStr = htmlStr.trim().replaceAll("\n", " ");
			htmlStr = htmlStr.trim().replaceAll("&nbsp;", " ");
			htmlStr = htmlStr.trim().replaceAll(" +", ",");
		} catch (Exception e) {
			htmlStr = "";
		}

		return htmlStr;// 返回文本字符串
	}

}
