package cn.lxm.iMail.Logic;

import java.util.HashMap;

import cn.lxm.iMail.UI.EmailDetail;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.effect.ImageInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class EmailDetailsEvents {
	static HashMap<String, String[]> map = new HashMap<String, String[]>();
	static String tempMeneListItemName = "";
	static boolean showlistFlag=false;

	public EmailDetailsEvents() {
		map.put("btn_message", new String[] { "mail0.png", "mail1.png", "mail2.png" });
		map.put("btn_friends", new String[] { "contact0 (2).png", "contact1 (2).png", "contact2 (2).png" });
		map.put("btn_search", new String[] { "calendar0.png", "calendar1.png", "calendar2.png" });
		map.put("btn_settings", new String[] { "setting0.png", "setting1.png", "setting2.png" });

		map.put("btn_close", new String[] { "white_close_0.png", "white_close_1.png", "white_close_2.png" });
		map.put("btn_big", new String[] { "white_max_0.png", "white_max_1.png", "white_max_2.png" });
		map.put("btn_small", new String[] { "white_min_0.png", "white_min_1.png", "white_min_2.png" });
		map.put("btn_replyTome",new String[] { "white_feedback_0.png", "white_feedback_1.png", "white_feedback_2.png" });

		map.put("pane_oneuserHbox1", new String[] { "accountlist_inbox.png", "accountlist_inbox_pressed.png" });
		map.put("pane_oneuserHbox2", new String[] { "accountlist_flag.png", "accountlist_flag_pressed.png" });
		map.put("pane_oneuserHbox3", new String[] { "accountlist_draft.png", "accountlist_draft_pressed.png" });
		map.put("pane_oneuserHbox4", new String[] { "accountlist_outbox.png", "accountlist_outbox_pressed.png" });
		map.put("pane_oneuserHbox5", new String[] { "accountlist_send.png", "accountlist_send_pressed.png" });
		map.put("pane_oneuserHbox6", new String[] { "accountlist_spam.png", "accountlist_spam_pressed.png" });
		map.put("pane_oneuserHbox7", new String[] { "accountlist_delete.png", "accountlist_delete_pressed.png" });

		// map.put("btn_writeEmail", new
		// String[]{"setting0.png","setting1.png","setting2.png"});

	}

	// 左边工具栏的三个事件调用刷新UI
	public static void mouseEvent_ImgReplace(Node node) {
		if (map.get(node.getId()) != null) {
			ImageInput imageInput = new ImageInput();
			Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[1]);
			imageInput.setSource(image);
			((Button) EmailDetail.root.lookup("#" + node.getId())).setEffect(imageInput);
		} else {
			return;
		}
	}

	public static void left_mouseEvent_press(Node node) {
		if (map.get(node.getId()) != null) {
			ImageInput imageInput = new ImageInput();
			Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[2]);
			imageInput.setSource(image);
			((Button) EmailDetail.root.lookup("#" + node.getId())).setEffect(imageInput);
		} else {
			return;
		}
	}

	public static void left_mouseEvent_exit(Node node) {
		if (map.get(node.getId()) != null) {
			ImageInput imageInput = new ImageInput();
			Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[0]);
			imageInput.setSource(image);
			((Button) EmailDetail.root.lookup("#" + node.getId())).setEffect(imageInput);
		} else {
			return;
		}
	}

	// 上面的按钮的4个事件
	public static void top_mouseEvent_ImgReplace(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:35px"); // 设置css样式
		}

		if (node.getId().equals("btn_writeEmail")) {
		}
		if (node.getId().equals("btn_writeEmail1")) {
		}
		if (node.getId().equals("btn_writeEmail2")) {
		}
		if (node.getId().equals("btn_reSend")) {
		}
		if (node.getId().equals("btn_delete")) {
		}
		if (node.getId().equals("btn_move")) {
		}
		if (node.getId().equals("btn_more")) {
		}

	}

	public static void top_mouseEvent_press(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#cee3f5;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#cee3f5;-fx-background-radius:35px"); // 设置css样式
		}

	}

	public static void top_mouseEvent_exit(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#fafbfc;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#fafbfc;-fx-background-radius:35px"); // 设置css样式
		}

	}

	public static void top_mouseEvent_relesed(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:35px"); // 设置css样式
		}

	}

	// 右上角的事件
	public static void right_top_mouseEvent_ImgReplace(Node node) {

		if (map.get(node.getId()) != null) {

			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
		} else {
			return;
		}

	}

	public static void right_top_mouseEvent_press(Node node) {

		if (map.get(node.getId()) != null) {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[2]));
		} else {
			return;
		}
	}

	public static void right_top_mouseEvent_exit(Node node) {

		if (map.get(node.getId()) != null) {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[0]));
		} else {
			return;
		}
	}

	public static void right_top_mouseEvent_relesed(Node node) {

		if (map.get(node.getId()) != null) {
			((Button) EmailDetail.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
		} else {
			return;
		}
	}

	// 菜单的三个事件
	public static void menu_btn_move(Node node) {

		if (!node.getId().equals("btn_menu0")) {
			String orid = node.getId();
			String buttonID = "btn_menu" + orid.charAt(orid.length() - 1);
			((Button) EmailDetail.root.lookup("#" + buttonID))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
		}

	}

	public static void menu_btn_click(Node node) {

		if (!(node.getId().equals("btn_menu0")) && node.getId().length() > 15) {
			String orid = node.getId();
			String buttonID = "btn_menu" + orid.charAt(orid.length() - 1);
			((Button) EmailDetail.root.lookup("#" + buttonID))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
			ClearSelect(orid);

		}
		if (node.getId().length() < 15) {

			String btnid = node.getId();
			String paneID = "pane_oneuserHbox" + btnid.charAt(btnid.length() - 1);
			menu_btn_click(EmailDetail.root.lookup("#" + paneID));
		}

	}

	// 清除其他选中事件
	private static void ClearSelect(String orid) {
		if (!tempMeneListItemName.equals("")) {
			String buttonID = "btn_menu" + tempMeneListItemName.charAt(tempMeneListItemName.length() - 1);
			((Button) EmailDetail.root.lookup("#" + buttonID))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(tempMeneListItemName)[0]));
			//((HBox) EmailDetail.root.lookup("#" + tempMeneListItemName)).setOnMouseExited(EmailDetail.menu_exit);
		}
		for (int i = 1; i < 8; i++) {
			String name = "pane_oneuserHbox" + i;
			if (!name.equals(orid)) {
				((Button) EmailDetail.root.lookup("#" + "btn_menu" + i)).setStyle("-fx-background-color:#fafbfc;");
				((HBox) EmailDetail.root.lookup("#" + name))
						.setStyle("-fx-background-color:#fafbfc;-fx-background-radius:5px");

			} else {
				((Button) EmailDetail.root.lookup("#" + "btn_menu" + i)).setStyle("-fx-background-color:#cee3f5;");
				((HBox) EmailDetail.root.lookup("#" + name))
						.setStyle("-fx-background-color:#cee3f5;-fx-background-radius:5px");
				((HBox) EmailDetail.root.lookup("#" + name)).setOnMouseExited(null);
			}
		}
		tempMeneListItemName = orid;

	}

	public static void menu_btn_exit(Node node) {

		if (!node.getId().equals("btn_menu0")) {
			String orid = node.getId();
			String buttonID = "btn_menu" + orid.charAt(orid.length() - 1);
			((Button) EmailDetail.root.lookup("#" + buttonID))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[0]));
		}

	}

	public static void showList_click(Node node) {

		if(!showlistFlag)
		{
			((Button) EmailDetail.root.lookup("#" + "btn_menu0")).setStyle("-fx-background-color:#fafbfc;-fx-text-alignment:left;-fx-background-radius:5px;");
			((Pane)EmailDetail.root.lookup("#pane_oneuser_list")).setVisible(true);
			((Pane)EmailDetail.root.lookup("#pane_oneuser_list")).setPrefHeight(222);
			((ImageView)EmailDetail.root.lookup("#img_dropdown")).setImage(new Image("cn/lxm/iMail/img/icon_up(2).png"));
			final Timeline timeline=new Timeline();
			//将x的位置在500ms内移动到300处
			RotateTransition rTransition=new RotateTransition();
		    final KeyValue kv=new KeyValue(((Pane)EmailDetail.root.lookup("#pane_oneuser_list")).translateYProperty(), 0);
		    final KeyFrame kf=new KeyFrame(Duration.millis(300), kv);
		    //将关键帧加到时间轴中
		    timeline.getKeyFrames().add(kf);
		   // timeline.getKeyFrames().add(kf2);
		    timeline.play();//运行
		    showlistFlag=!showlistFlag;
		}
		else {

			((Button) EmailDetail.root.lookup("#" + "btn_menu0")).setStyle("-fx-background-color:#cee3f5;-fx-text-alignment:left;-fx-background-radius:5px;");
			((Pane)EmailDetail.root.lookup("#pane_oneuser_list")).setVisible(false);
			((Pane)EmailDetail.root.lookup("#pane_oneuser_list")).setPrefHeight(0);
			((ImageView)EmailDetail.root.lookup("#img_dropdown")).setImage(new Image("cn/lxm/iMail/img/icon_down (2).png"));
			final Timeline timeline=new Timeline();
			//将x的位置在500ms内移动到300处
			RotateTransition rTransition=new RotateTransition();
		    final KeyValue kv=new KeyValue(((Pane)EmailDetail.root.lookup("#pane_oneuser_list")).translateYProperty(), 0);
		    final KeyFrame kf=new KeyFrame(Duration.millis(300), kv);
		    //将关键帧加到时间轴中
		    timeline.getKeyFrames().add(kf);
		   // timeline.getKeyFrames().add(kf2);
		    timeline.play();//运行
		    showlistFlag=!showlistFlag;
		}

	}

}
