package cn.lxm.iMail.Logic;
import java.util.HashMap;

import cn.lxm.iMail.UI.Calendar;
import cn.lxm.iMail.UI.Contact;
import cn.lxm.iMail.UI.Index;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.ImageInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
public class CalendarEvents {

	static HashMap<String, String[]> map = new HashMap<String, String[]>();
	static String tempMeneListItemName = "";
	static boolean showlistFlag = false;
	static CalendarEvents calendarEvents=new CalendarEvents();
	public CalendarEvents() {
		map.put("btn_message", new String[] { "mail0.png", "mail1.png", "mail2.png" });
		map.put("btn_friends", new String[] { "contact0 (2).png", "contact1 (2).png", "contact2 (2).png" });
		map.put("btn_search", new String[] { "calendar0.png", "calendar1.png", "calendar2.png" });
		map.put("btn_settings", new String[] { "setting0.png", "setting1.png", "setting2.png" });

		map.put("btn_close", new String[] { "white_close_0.png", "white_close_1.png", "white_close_2.png" });
		map.put("btn_big", new String[] { "white_max_0.png", "white_max_1.png", "white_max_2.png" });
		map.put("btn_small", new String[] { "white_min_0.png", "white_min_1.png", "white_min_2.png" });
		map.put("btn_replyTome",
				new String[] { "white_feedback_0.png", "white_feedback_1.png", "white_feedback_2.png" });


	}
	// 左边工具栏的三个事件调用刷新UI
		public static void mouseEvent_ImgReplace(Node node) {
			if (map.get(node.getId()) != null) {
				ImageInput imageInput = new ImageInput();
				Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[1]);
				imageInput.setSource(image);
				((Button) Calendar.root.lookup("#" + node.getId())).setEffect(imageInput);
			} else {
				return;
			}
		}

		public static void left_mouseEvent_press(Node node) {
			if (map.get(node.getId()) != null) {
				ImageInput imageInput = new ImageInput();
				Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[2]);
				imageInput.setSource(image);
				((Button) Calendar.root.lookup("#" + node.getId())).setEffect(imageInput);

				//切换面板
				if((node.getId().equals("btn_message")))
				{
					Platform.runLater(new Runnable() {
						public void run() {
							try {
								new Index().start(new Stage());
								Calendar.root.getScene().getWindow().hide();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				}
				else if((node.getId().equals("btn_friends")))
				{
					Platform.runLater(new Runnable() {
						public void run() {
							try {
								new Index().start(new Stage());
								Contact.root.getScene().getWindow().hide();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				}
				else if(node.getId().equals("btn_search"))
				{

				}
				else
				{

				}
			} else {
				return;
			}
		}

		public static void left_mouseEvent_exit(Node node) {
			if (map.get(node.getId()) != null) {
				ImageInput imageInput = new ImageInput();
				Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[0]);
				imageInput.setSource(image);
				((Button) Calendar.root.lookup("#" + node.getId())).setEffect(imageInput);
			} else {
				return;
			}
		}

		// 上面的按钮的4个事件
		public static void top_mouseEvent_ImgReplace(Node node) {

			if (!node.getId().equals("btn_writeEmail1")) {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:5px"); // 设置css样式
			} else {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:35px"); // 设置css样式
			}

			if (node.getId().equals("btn_writeEmail")) {
			}
			if (node.getId().equals("btn_writeEmail1")) {
			}
			if (node.getId().equals("btn_writeEmail2")) {
			}
			if (node.getId().equals("btn_reSend")) {
			}
			if (node.getId().equals("btn_delete")) {
			}
			if (node.getId().equals("btn_move")) {
			}
			if (node.getId().equals("btn_more")) {
			}

		}

		public static void top_mouseEvent_press(Node node) {

			if (!node.getId().equals("btn_writeEmail1")) {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setStyle("-fx-background-color:#cee3f5;-fx-background-radius:5px"); // 设置css样式
			} else {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setStyle("-fx-background-color:#cee3f5;-fx-background-radius:35px"); // 设置css样式
			}

		}

		public static void top_mouseEvent_exit(Node node) {

			if (!node.getId().equals("btn_writeEmail1")) {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setStyle("-fx-background-color:#fafbfc;-fx-background-radius:5px"); // 设置css样式
			} else {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setStyle("-fx-background-color:#fafbfc;-fx-background-radius:35px"); // 设置css样式
			}

		}

		public static void top_mouseEvent_relesed(Node node) {

			if (!node.getId().equals("btn_writeEmail1")) {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:5px"); // 设置css样式
			} else {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:35px"); // 设置css样式
			}

		}

		// 右上角的事件
		public static void right_top_mouseEvent_ImgReplace(Node node) {
			if (map.get(node.getId()) != null) {

				((Button) Calendar.root.lookup("#" + node.getId()))
						.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
			} else {
				return;
			}

		}

		public static void right_top_mouseEvent_press(Node node) {

			if (map.get(node.getId()) != null) {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[2]));
			} else {
				return;
			}
		}

		public static void right_top_mouseEvent_exit(Node node) {

			if (map.get(node.getId()) != null) {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[0]));
			} else {
				return;
			}
		}

		public static void right_top_mouseEvent_relesed(Node node) {

			if (map.get(node.getId()) != null) {
				((Button) Calendar.root.lookup("#" + node.getId()))
						.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
			} else {
				return;
			}
		}


}
