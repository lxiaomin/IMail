package cn.lxm.iMail.Logic;

import java.util.HashMap;

import cn.lxm.iMail.UI.Contact;
import cn.lxm.iMail.UI.Index;
import cn.lxm.iMail.UI.WriteEmail;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.ImageInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class WriteEmailUIEvents {

	static HashMap<String, String[]> map = new HashMap<String, String[]>();
	static String tempMeneListItemName = "";
	static boolean showlistFlag = false;
	static boolean IsShowContacts = false;

	public WriteEmailUIEvents() {
		map.put("btn_message", new String[] { "mail0.png", "mail1.png", "mail2.png" });
		map.put("btn_friends", new String[] { "contact0 (2).png", "contact1 (2).png", "contact2 (2).png" });
		map.put("btn_search", new String[] { "calendar0.png", "calendar1.png", "calendar2.png" });
		map.put("btn_settings", new String[] { "setting0.png", "setting1.png", "setting2.png" });

		map.put("btn_close", new String[] { "white_close_0.png", "white_close_1.png", "white_close_2.png" });
		map.put("btn_big", new String[] { "white_max_0.png", "white_max_1.png", "white_max_2.png" });
		map.put("btn_small", new String[] { "white_min_0.png", "white_min_1.png", "white_min_2.png" });
		map.put("btn_replyTome", new String[] { "contact0.png", "contact1.png", "contact2.png" });

		map.put("pane_oneuserHbox1", new String[] { "accountlist_inbox.png", "accountlist_inbox_pressed.png" });
		map.put("pane_oneuserHbox2", new String[] { "accountlist_flag.png", "accountlist_flag_pressed.png" });
		map.put("pane_oneuserHbox3", new String[] { "accountlist_draft.png", "accountlist_draft_pressed.png" });
		map.put("pane_oneuserHbox4", new String[] { "accountlist_outbox.png", "accountlist_outbox_pressed.png" });
		map.put("pane_oneuserHbox5", new String[] { "accountlist_send.png", "accountlist_send_pressed.png" });
		map.put("pane_oneuserHbox6", new String[] { "accountlist_spam.png", "accountlist_spam_pressed.png" });
		map.put("pane_oneuserHbox7", new String[] { "accountlist_delete.png", "accountlist_delete_pressed.png" });

		// map.put("btn_writeEmail", new
		// String[]{"setting0.png","setting1.png","setting2.png"});

	}

	// 左边工具栏的三个事件调用刷新UI
	public static void mouseEvent_ImgReplace(Node node) {
		if (map.get(node.getId()) != null) {
			ImageInput imageInput = new ImageInput();
			Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[1]);
			imageInput.setSource(image);
			((Button) WriteEmail.root.lookup("#" + node.getId())).setEffect(imageInput);
		} else {
			return;
		}
	}

	public static void left_mouseEvent_press(Node node) {
		if (map.get(node.getId()) != null) {
			ImageInput imageInput = new ImageInput();
			Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[2]);
			imageInput.setSource(image);
			((Button) WriteEmail.root.lookup("#" + node.getId())).setEffect(imageInput);
		} else {
			return;
		}
	}

	public static void left_mouseEvent_exit(Node node) {
		if (map.get(node.getId()) != null) {
			ImageInput imageInput = new ImageInput();
			Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[0]);
			imageInput.setSource(image);
			((Button) WriteEmail.root.lookup("#" + node.getId())).setEffect(imageInput);
		} else {
			return;
		}
	}

	// 上面的按钮的4个事件
	public static void top_mouseEvent_ImgReplace(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:35px"); // 设置css样式
		}

		if (node.getId().equals("btn_writeEmail")) {
		}
		if (node.getId().equals("btn_writeEmail1")) {
		}
		if (node.getId().equals("btn_writeEmail2")) {
		}
		if (node.getId().equals("btn_reSend")) {
		}
		if (node.getId().equals("btn_delete")) {
		}
		if (node.getId().equals("btn_move")) {
		}
		if (node.getId().equals("btn_more")) {
		}

	}

	public static void top_mouseEvent_press(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#cee3f5;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#cee3f5;-fx-background-radius:35px"); // 设置css样式
		}

	}

	public static void top_mouseEvent_exit(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#fafbfc;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#fafbfc;-fx-background-radius:35px"); // 设置css样式
		}

	}

	public static void top_mouseEvent_relesed(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:35px"); // 设置css样式
		}

	}

	// 右上角的事件
	public static void right_top_mouseEvent_ImgReplace(Node node) {
		if (map.get(node.getId()) != null) {

			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
		} else {
			return;
		}

	}

	public static void right_top_mouseEvent_press(Node node) {

		if (map.get(node.getId()) != null) {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[2]));
		} else {
			return;
		}
	}

	public static void right_top_mouseEvent_exit(Node node) {

		if (map.get(node.getId()) != null) {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[0]));
		} else {
			return;
		}
	}

	public static void right_top_mouseEvent_relesed(Node node) {

		if (map.get(node.getId()) != null) {
			((Button) WriteEmail.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
		} else {
			return;
		}
	}

	public static void right_top_Img_click_showContact(Node node) {
		if (!IsShowContacts) {
			((Pane) WriteEmail.root.lookup("#pane_contacts")).setVisible(true);
			((Pane) WriteEmail.root.lookup("#pane_contacts")).setPrefWidth(275);
			final Timeline timeline = new Timeline();
			// 将x的位置在500ms内移动到300处
			final KeyValue kv = new KeyValue(((Pane) WriteEmail.root.lookup("#pane_contacts")).translateXProperty(), 0);
			final KeyFrame kf = new KeyFrame(Duration.millis(300), kv);
			// 将关键帧加到时间轴中
			timeline.getKeyFrames().add(kf);
			timeline.play();// 运行
			((Button) WriteEmail.root.lookup("#" + node.getId())).setOnMouseExited(null);
			IsShowContacts = !IsShowContacts;
		} else {

			final Timeline timeline = new Timeline();
			// 将x的位置在500ms内移动到300处
			final KeyValue kv = new KeyValue(((Pane) WriteEmail.root.lookup("#pane_contacts")).translateXProperty(),
					275);
			final KeyFrame kf = new KeyFrame(Duration.millis(300), kv);
			// 将关键帧加到时间轴中
			timeline.getKeyFrames().add(kf);
			timeline.play();// 运行
			((Button) WriteEmail.root.lookup("#" + node.getId())).setOnMouseExited(WriteEmail.right_top_Img_exit);
			((Pane) WriteEmail.root.lookup("#pane_contacts")).setPrefWidth(0);
			((Pane) WriteEmail.root.lookup("#pane_contacts")).setVisible(false);
			IsShowContacts = !IsShowContacts;
		}
	}

	// 菜单的三个事件
	public static void menu_btn_move(Node node) {

		if (!node.getId().equals("btn_menu0")) {
			String orid = node.getId();
			String buttonID = "btn_menu" + orid.charAt(orid.length() - 1);
			((Button) WriteEmail.root.lookup("#" + buttonID))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
		}

	}

	public static void menu_btn_click(Node node) {

		// 获取ID
		String orid = node.getId();
		// 获取已经选中的按钮的编号
		int selectItem = Integer.parseInt(String.valueOf(orid.charAt(orid.length() - 1)));
		WriteEmail.selectItemNum = selectItem;
		if (!(node.getId().equals("btn_menu_0")) && node.getId().length() > 15) {
			String buttonID = "btn_menu" + orid.charAt(orid.length() - 1);
			ClearSelect(orid);
			// 把号码添加到收件人栏目中
			if (((TextField) WriteEmail.root.lookup("#textbox_people")).getText().equals("")) {
				((TextField) WriteEmail.root.lookup("#textbox_people"))
						.setText(WriteEmail.Contacts.get(selectItem).getEmailAdreess());
				return;
			} else {
				((TextField) WriteEmail.root.lookup("#textbox_people"))
						.setText(((TextField) WriteEmail.root.lookup("#textbox_people")).getText() + ";"
								+ WriteEmail.Contacts.get(selectItem).getEmailAdreess());
			}

		}
		if (node.getId().length() < 15) {
			String btnid = node.getId();
			String paneID = "pane_oneuserHbox" + btnid.charAt(btnid.length() - 1);
			menu_btn_click(WriteEmail.root.lookup("#" + paneID));
		}

	}

	// 清除其他选中事件
	private static void ClearSelect(String orid) {
		for (int i = 0; i < WriteEmail.Contacts.size(); i++) {
			String name = "pane_oneuserHbox" + i;
			if (!name.equals(orid)) {
				((Button) WriteEmail.root.lookup("#" + "btn_menu" + i)).setStyle("-fx-background-color:#fafbfc;");
			} else {
				((Button) WriteEmail.root.lookup("#" + "btn_menu" + i)).setStyle("-fx-background-color:#cee3f5;");
			}
		}

		tempMeneListItemName = orid;

	}

	public static void menu_btn_exit(Node node) {

		if (!node.getId().equals("btn_menu0")) {
			String orid = node.getId();
			String buttonID = "btn_menu" + orid.charAt(orid.length() - 1);
			((Button) WriteEmail.root.lookup("#" + buttonID))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[0]));
		}

	}

	public static void showList_click(Node node) {

		if (!showlistFlag) {
			((Button) WriteEmail.root.lookup("#" + "btn_menu0"))
					.setStyle("-fx-background-color:#fafbfc;-fx-text-alignment:left;-fx-background-radius:5px;");
			((Pane) WriteEmail.root.lookup("#pane_oneuser_list")).setVisible(true);
			((Pane) WriteEmail.root.lookup("#pane_oneuser_list")).setPrefHeight(222);
			((ImageView) WriteEmail.root.lookup("#img_dropdown"))
					.setImage(new Image("cn/lxm/iMail/img/icon_up(2).png"));
			final Timeline timeline = new Timeline();
			// 将x的位置在500ms内移动到300处
			RotateTransition rTransition = new RotateTransition();
			final KeyValue kv = new KeyValue(((Pane) WriteEmail.root.lookup("#pane_oneuser_list")).translateYProperty(),
					0);
			final KeyFrame kf = new KeyFrame(Duration.millis(300), kv);
			// 将关键帧加到时间轴中
			timeline.getKeyFrames().add(kf);
			// timeline.getKeyFrames().add(kf2);
			timeline.play();// 运行
			showlistFlag = !showlistFlag;
		} else {

			((Button) WriteEmail.root.lookup("#" + "btn_menu0"))
					.setStyle("-fx-background-color:#cee3f5;-fx-text-alignment:left;-fx-background-radius:5px;");
			((Pane) WriteEmail.root.lookup("#pane_oneuser_list")).setVisible(false);
			((Pane) WriteEmail.root.lookup("#pane_oneuser_list")).setPrefHeight(0);
			((ImageView) WriteEmail.root.lookup("#img_dropdown"))
					.setImage(new Image("cn/lxm/iMail/img/icon_down (2).png"));
			final Timeline timeline = new Timeline();
			// 将x的位置在500ms内移动到300处
			RotateTransition rTransition = new RotateTransition();
			final KeyValue kv = new KeyValue(((Pane) WriteEmail.root.lookup("#pane_oneuser_list")).translateYProperty(),
					0);
			final KeyFrame kf = new KeyFrame(Duration.millis(300), kv);
			// 将关键帧加到时间轴中
			timeline.getKeyFrames().add(kf);
			// timeline.getKeyFrames().add(kf2);
			timeline.play();// 运行
			showlistFlag = !showlistFlag;
		}

	}

}
