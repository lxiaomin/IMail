package cn.lxm.iMail.Logic;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.persistence.criteria.Root;

import cn.lxm.iMail.Email.ConnectWithPop3;
import cn.lxm.iMail.Email.Email;
import cn.lxm.iMail.Email.EmailFJ;
import cn.lxm.iMail.Email.EmailUser;
import cn.lxm.iMail.Email.MakeupHtmlEmails;
import cn.lxm.iMail.Hibernate.DAO.ReceiveDAO;
import cn.lxm.iMail.Hibernate.DAO.SendDAO;
import cn.lxm.iMail.Hibernate.model.Receive;
import cn.lxm.iMail.Hibernate.model.Send;
import cn.lxm.iMail.UI.Calendar;
import cn.lxm.iMail.UI.Contact;
import cn.lxm.iMail.UI.EmailDetail;
import cn.lxm.iMail.UI.Index;
import cn.lxm.iMail.UI.Login;
import cn.lxm.iMail.UI.WriteEmail;
import cn.lxm.iMail.UI.WriteEmailTipsBox;
import cn.lxm.iMail.UI.WriteEmailTipsBox3;
import cn.lxm.iMail.Util.CodeTranslate;
import cn.lxm.iMail.Util.ReadFile;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.ImageInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import javassist.compiler.ast.NewExpr;

public class UIEvents {

	static HashMap<String, String[]> map = new HashMap<String, String[]>();
	static String tempMeneListItemName = "";
	static boolean showlistFlag = false;
	public static List<Receive> list;
	static String ClickID = "rebox_0";
	static int newEmailCount = 0;
	static ReceiveDAO rd;
	static boolean IsDeleteFlag = false;
	static SendDAO sDao=new SendDAO();

	public UIEvents() {
		map.put("btn_message", new String[] { "mail0.png", "mail1.png", "mail2.png" });
		map.put("btn_friends", new String[] { "contact0 (2).png", "contact1 (2).png", "contact2 (2).png" });
		map.put("btn_search", new String[] { "calendar0.png", "calendar1.png", "calendar2.png" });
		map.put("btn_settings", new String[] { "setting0.png", "setting1.png", "setting2.png" });

		map.put("btn_close", new String[] { "white_close_0.png", "white_close_1.png", "white_close_2.png" });
		map.put("btn_big", new String[] { "white_max_0.png", "white_max_1.png", "white_max_2.png" });
		map.put("btn_small", new String[] { "white_min_0.png", "white_min_1.png", "white_min_2.png" });
		map.put("btn_replyTome",
				new String[] { "white_feedback_0.png", "white_feedback_1.png", "white_feedback_2.png" });

		map.put("pane_oneuserHbox1", new String[] { "accountlist_inbox.png", "accountlist_inbox_pressed.png" });
		map.put("pane_oneuserHbox2", new String[] { "accountlist_flag.png", "accountlist_flag_pressed.png" });
		map.put("pane_oneuserHbox3", new String[] { "accountlist_draft.png", "accountlist_draft_pressed.png" });
		map.put("pane_oneuserHbox4", new String[] { "accountlist_outbox.png", "accountlist_outbox_pressed.png" });
		map.put("pane_oneuserHbox5", new String[] { "accountlist_send.png", "accountlist_send_pressed.png" });
		map.put("pane_oneuserHbox6", new String[] { "accountlist_spam.png", "accountlist_spam_pressed.png" });
		map.put("pane_oneuserHbox7", new String[] { "accountlist_delete.png", "accountlist_delete_pressed.png" });

	}

	// 左边工具栏的三个事件调用刷新UI
	public static void mouseEvent_ImgReplace(Node node) {
		if (map.get(node.getId()) != null) {
			ImageInput imageInput = new ImageInput();
			Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[1]);
			imageInput.setSource(image);
			((Button) Index.root.lookup("#" + node.getId())).setEffect(imageInput);
		} else {
			return;
		}
	}

	public static void left_mouseEvent_press(Node node) {
		if (map.get(node.getId()) != null) {
			ImageInput imageInput = new ImageInput();
			Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[2]);
			imageInput.setSource(image);
			((Button) Index.root.lookup("#" + node.getId())).setEffect(imageInput);

			//切换面板
			if((node.getId().equals("btn_message")))
			{

			}
			else if((node.getId().equals("btn_friends")))
			{
				Platform.runLater(new Runnable() {
					public void run() {
						try {
							new Contact().start(new Stage());
							Index.root.getScene().getWindow().hide();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
			else if(node.getId().equals("btn_search"))
			{
				Platform.runLater(new Runnable() {
					public void run() {
						try {
							new Calendar().start(new Stage());
							Index.root.getScene().getWindow().hide();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
			else
			{

			}

		} else {
			return;
		}
	}

	public static void left_mouseEvent_exit(Node node) {
		if (map.get(node.getId()) != null) {
			ImageInput imageInput = new ImageInput();
			Image image = new Image("cn/lxm/iMail/img/" + map.get(node.getId())[0]);
			imageInput.setSource(image);
			((Button) Index.root.lookup("#" + node.getId())).setEffect(imageInput);
		} else {
			return;
		}
	}

	// 上面的按钮的4个事件
	public static void top_mouseEvent_ImgReplace(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) Index.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) Index.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:35px"); // 设置css样式
		}

	}

	public static void top_mouseEvent_press(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) Index.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#cee3f5;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) Index.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#cee3f5;-fx-background-radius:35px"); // 设置css样式
		}
		if (node.getId().equals("btn_writeEmail")) {
			Platform.runLater(new Runnable() {
				public void run() {
					try {
						new WriteEmail().start(new Stage());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			});
		}
		if (node.getId().equals("btn_writeEmail1")) {
			new Thread() {
				public void run() {
					try {
						 ConnectWithPop3.ReFlashEmailBox(Login.eu.getEmailCount(),
						 Login.eu);
					} catch (Exception e) {
					}

				}
			}.start();
		}
		if (node.getId().equals("btn_writeEmail2")) {
			Platform.runLater(new Runnable() {
				public void run() {
					try {
						Email email = new Email();
						email = EmailFJ
								.DecomposeEmail(list.get(Integer.parseInt(ClickID.split("_")[1])).getEmailContent());
						CommitWebBroswer.htmlcode = "<br><br><br><br><br><br><br><br><br><br><div style=\\\"border-radius: 5px;height:36px;width:834px;background-color: #ddd;\\\">"
								+ "<span style=\\\"" + "       margin-left: 15px;" + "    line-height: 2.15;"
								+ "    margin-top: auto;" + "    margin-bottom: auto;"
								+ "    font-family: Microsoft YaHei;" + "\\\">在" + email.getTime() + "，</span>"
								+ "<span style=\\\"" + "    line-height: 2.15;" + "    margin-top: auto;"
								+ "    margin-bottom: auto;" + "    font-family: Microsoft YaHei, Tahoma;"
								+ "    color: #339FFF;" + "\\\">" + email.getFrom() + "</span><span style=\\\""
								+ "    line-height: 2.15;" + "    margin-top: auto;" + "    margin-bottom: auto;"
								+ "    font-family: Microsoft YaHei, Tahoma;" + "\\\"> 写道："
								+ email.getContent().replaceAll("\"", "\\\\\"").replaceAll("\\n", "") + "</span>"
								+ "</div>";
						WriteEmail writeEmail = new WriteEmail();
						writeEmail.start(new Stage());
						((TextField) (WriteEmail.root.lookup("#textbox_people"))).setText(email.getFrom());
						((TextField) (WriteEmail.root.lookup("#textbox_theme"))).setText("回复：" + email.getSubject());

					} catch (Exception e) {
						try {
							new WriteEmailTipsBox("您还没有选择邮件.").start(new Stage());
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						e.printStackTrace();
					}
				}

			});
		}
		if (node.getId().equals("btn_reSend")) {
			Platform.runLater(new Runnable() {
				public void run() {
					try {
						Email email = new Email();
						email = EmailFJ
								.DecomposeEmail(list.get(Integer.parseInt(ClickID.split("_")[1])).getEmailContent());
						CommitWebBroswer.htmlcode = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><div style=\\\"border-radius: 5px;height:36px;width:834px;background-color: #ddd;\\\">"
								+ "<span style=\\\"" + "       margin-left: 15px;" + "    line-height: 2.15;"
								+ "    margin-top: auto;" + "    margin-bottom: auto;"
								+ "    font-family: Microsoft YaHei;" + "\\\">在" + email.getTime() + "，</span>"
								+ "<span style=\\\"" + "    line-height: 2.15;" + "    margin-top: auto;"
								+ "    margin-bottom: auto;" + "    font-family: Microsoft YaHei, Tahoma;"
								+ "    color: #339FFF;" + "\\\">" + email.getFrom() + "</span><span style=\\\""
								+ "    line-height: 2.15;" + "    margin-top: auto;" + "    margin-bottom: auto;"
								+ "    font-family: Microsoft YaHei, Tahoma;" + "\\\"> 写道：<br><br><br><br>"
								+ email.getContent().replaceAll("\"", "\\\\\"").replaceAll("\\n", "") + "</span>"
								+ "</div>";
						WriteEmail writeEmail = new WriteEmail();
						writeEmail.start(new Stage());
						((TextField) (WriteEmail.root.lookup("#textbox_people"))).setText(email.getFrom());
						((TextField) (WriteEmail.root.lookup("#textbox_theme"))).setText("转发：" + email.getSubject());

					} catch (Exception e) {
						try {
							new WriteEmailTipsBox("您还没有选择邮件.").start(new Stage());
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						e.printStackTrace();
					}
				}

			});
		}
		if (node.getId().equals("btn_delete")) {
			new Thread() {
				public void run() {
					try {

						// 保存到垃圾站，等待彻底回收
						Receive receive = new Receive();
						receive = list.get(Integer.parseInt(ClickID.split("_")[1]));
						receive.setIsInDeleteBox(1);
						rd.update(receive);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								((VBox) (Index.root.lookup("#vbox_email"))).getChildren()
										.remove(Index.root.lookup("#rebox_" + Integer.parseInt(ClickID.split("_")[1])));
								System.out.println("删除到回收站成功");
							}

						});

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}.start();
		}
		if (node.getId().equals("btn_move")) {
		}
		if (node.getId().equals("btn_more")) {
		}

	}

	public static void top_mouseEvent_exit(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) Index.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#fafbfc;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) Index.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#fafbfc;-fx-background-radius:35px"); // 设置css样式
		}

	}

	public static void top_mouseEvent_relesed(Node node) {

		if (!node.getId().equals("btn_writeEmail1")) {
			((Button) Index.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:5px"); // 设置css样式
		} else {
			((Button) Index.root.lookup("#" + node.getId()))
					.setStyle("-fx-background-color:#d9eafa;-fx-background-radius:35px"); // 设置css样式
		}

	}

	// 右上角的事件
	public static void right_top_mouseEvent_ImgReplace(Node node) {
		if (map.get(node.getId()) != null) {

			((Button) Index.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
		} else {
			return;
		}

	}

	public static void right_top_mouseEvent_press(Node node) {

		if (map.get(node.getId()) != null) {
			((Button) Index.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[2]));
		} else {
			return;
		}
	}

	public static void right_top_mouseEvent_exit(Node node) {

		if (map.get(node.getId()) != null) {
			((Button) Index.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[0]));
		} else {
			return;
		}
	}

	public static void right_top_mouseEvent_relesed(Node node) {

		if (map.get(node.getId()) != null) {
			((Button) Index.root.lookup("#" + node.getId()))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
		} else {
			return;
		}
	}

	// 菜单的三个事件
	public static void menu_btn_move(Node node) {

		if (!node.getId().equals("btn_menu0")) {
			String orid = node.getId();
			String buttonID = "btn_menu" + orid.charAt(orid.length() - 1);
			((Button) Index.root.lookup("#" + buttonID))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
		}

	}

	public static void menu_btn_click(Node node) throws Exception {

		if (!(node.getId().equals("btn_menu0")) && node.getId().length() > 15) {
			String orid = node.getId();
			String buttonID = "btn_menu" + orid.charAt(orid.length() - 1);
			((Button) Index.root.lookup("#" + buttonID))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[1]));
			ClearSelect(orid);
			// 根据按钮载入相应的信箱的邮件
			LoadOtherEmails(node.getId().toString());

		}
		if (node.getId().length() < 15) {

			String btnid = node.getId();
			String paneID = "pane_oneuserHbox" + btnid.charAt(btnid.length() - 1);
			menu_btn_click(Index.root.lookup("#" + paneID));
		}

	}

	private static void LoadOtherEmails(String id) throws Exception {

//		EmailUser eu = new EmailUser();
//		eu.setUserName("13557330070@139.com");

		((Pane) Index.root.lookup("#pane_noemails")).setVisible(false);// 关闭没有邮件的提示
		String ButtonKey = "";// 要打开的ile按钮的关键字
		String valueString = "";// 打开的关键字的值
		if (id.equals("btn_menu1") || id.equals("pane_oneuserHbox1"))

		{
			((Button) (Index.root.lookup("#btn_delete"))).setText("删除");
			((Button) (Index.root.lookup("#btn_delete"))).setOnMousePressed(Index.top_Img_press);
			LoadEmails(Login.eu);
			return;
		}
		if (id.equals("btn_menu2") || id.equals("pane_oneuserHbox2")) {
			ButtonKey = "isInFlagBox";
			valueString = "1";
			((Button) (Index.root.lookup("#btn_delete"))).setText("删除");
			((Button) (Index.root.lookup("#btn_delete"))).setOnMousePressed(Index.top_Img_press);
		}
		//草稿箱
		if (id.equals("btn_menu3") || id.equals("pane_oneuserHbox3")) {
			ButtonKey = "isInChangeBox";
			valueString = "1";
			((Button) (Index.root.lookup("#btn_delete"))).setText("删除");
			((Button) (Index.root.lookup("#btn_delete"))).setOnMousePressed(Index.top_Img_press);
		}

		//发件箱
		if (id.equals("btn_menu4") || id.equals("pane_oneuserHbox4")) {
//			ButtonKey = "isInChangeBox";
//			valueString = "1";
			((Pane) Index.root.lookup("#pane_noemails")).setVisible(true);
			return;
//			((Button) (Index.root.lookup("#btn_delete"))).setText("删除");
//			((Button) (Index.root.lookup("#btn_delete"))).setOnMousePressed(Index.top_Img_press);
		}
		//已发送
		if (id.equals("btn_menu5") || id.equals("pane_oneuserHbox5")) {
//			ButtonKey = "isInChangeBox";
//			valueString = "1";
			((Pane) Index.root.lookup("#pane_noemails")).setVisible(true);
			LoadAreadSend(Login.eu);
			return;
//			((Button) (Index.root.lookup("#btn_delete"))).setText("删除");
//			((Button) (Index.root.lookup("#btn_delete"))).setOnMousePressed(Index.top_Img_press);
		}
		if (id.equals("btn_menu6") || id.equals("pane_oneuserHbox6")) {
			ButtonKey = "isInTrashBox";
			valueString = "1";
			((Button) (Index.root.lookup("#btn_delete"))).setText("删除");
			((Button) (Index.root.lookup("#btn_delete"))).setOnMousePressed(Index.top_Img_press);
		}
		if (id.equals("btn_menu7") || id.equals("pane_oneuserHbox7")) {
			ButtonKey = "isInDeleteBox";
			valueString = "1";
			((Button) (Index.root.lookup("#btn_delete"))).setText("彻删");
			((Button) (Index.root.lookup("#btn_delete"))).setOnMousePressed(finalDelete_click);
		}
		Receive receive = new Receive();
		rd = new ReceiveDAO();
		// list = rd.UserOtherEmail(eu.getUserName());
		list = rd.UserOtherEmail(Login.eu.getUserName(), ButtonKey, "1");
		String title, subject, content;
		VBox vBox = new VBox();
		vBox.setPrefWidth(278);
		vBox.setSpacing(5);
		vBox.setId("vbox_email");
		Email email = new Email();
		if (list.size() == 0) {
			((Pane) Index.root.lookup("#pane_noemails")).setVisible(true);
			return;
		}
		for (int i = list.size() - 1; i >= 0; i--) {
			email = EmailFJ.DecomposeEmail(list.get(i).getEmailContent());

			Pane outPane = new Pane();
			outPane.setStyle("-fx-background-radius: 10px; -fx-background-color: #fafbfc;");
			outPane.setPrefWidth(278);
			outPane.setPrefHeight(98);
			outPane.setId("rebox_" + i);
			outPane.setOnMouseEntered(email_Enter);
			outPane.setOnMouseExited(email_Exit);
			outPane.setOnMouseClicked(email_click);

			Pane SenderPane = new Pane();
			SenderPane.setStyle("");
			SenderPane.setPrefWidth(280);
			SenderPane.setPrefHeight(25);
			SenderPane.setLayoutX(-1);
			SenderPane.setLayoutY(8);

			Label senderLabel = new Label();
			senderLabel.setLayoutX(18);
			senderLabel.setLayoutY(1);
			senderLabel.setPrefHeight(15);
			senderLabel.setPrefWidth(212);
			try {
				if (CodeTranslate.getEncoding(email.getFrom()).equals("UTF-8"))
					continue;
			} catch (Exception e) {
				continue;
			}

			// {email.setFrom(CodeTranslate.getUTF8XMLString(email.getFrom()));System.out.println(email.getFrom());}
//			if(!email.getFrom().equals(""))
//			else senderLabel.setText("（无收件人）");
			senderLabel.setText(email.getFrom());
			senderLabel.setTextFill(Color.rgb(50, 50, 50));
			senderLabel.setStyle("-fx-font-size:18px;-fx-font-weight: bold;");
			senderLabel.setWrapText(false);

			ImageView tipsImageView = new ImageView();
			tipsImageView.setImage(new Image("cn/lxm/iMail/img/1.png"));
			tipsImageView.setFitHeight(16);
			tipsImageView.setFitWidth(16);
			tipsImageView.setLayoutX(3);
			tipsImageView.setLayoutY(5);
			if (list.get(i).getAppendix() > 0) {

				tipsImageView.setVisible(true);
			} else
				tipsImageView.setVisible(false);
			tipsImageView.setId("Isnew_" + i);

			ImageView topImageView = new ImageView();
			if (list.get(i).getIsInFlagBox() > 0) {
				topImageView.setImage(new Image("cn/lxm/iMail/img/star1.png"));
				topImageView.setVisible(true);
			} else {
				topImageView.setImage(new Image("cn/lxm/iMail/img/star0.png"));
				topImageView.setVisible(false);
			}
			topImageView.setFitHeight(16);
			topImageView.setFitWidth(16);
			topImageView.setLayoutX(250);
			topImageView.setLayoutY(2);
			topImageView.setId("flag_" + i);
			topImageView.setCursor(Cursor.HAND);

			topImageView.setOnMouseClicked(flag_click);

			Label TitleLabel = new Label();
			TitleLabel.setLayoutX(17);
			TitleLabel.setLayoutY(42);
			TitleLabel.setPrefHeight(15);
			TitleLabel.setPrefWidth(231);
			TitleLabel.setText(email.getSubject());
			TitleLabel.setTextFill(Color.rgb(14, 14, 14));
			TitleLabel.setStyle("-fx-font-size:14px;");
			TitleLabel.setWrapText(false);

			ImageView centerImageView = new ImageView();
			centerImageView.setImage(new Image("cn/lxm/iMail/img/accountlist_inbox.png"));
			centerImageView.setFitHeight(16);
			centerImageView.setFitWidth(16);
			centerImageView.setLayoutX(250);
			centerImageView.setLayoutY(41);
			centerImageView.setId("IsAdd_" + i);
			centerImageView.setVisible(false);

			Label contentLabel = new Label();
			contentLabel.setLayoutX(17);
			contentLabel.setLayoutY(66);
			contentLabel.setPrefHeight(18);
			contentLabel.setPrefWidth(221);
			// 邮件有文字内容的话就放上去，没有的话提示用户没文字内容
			if (email.getReceived().equals(""))
				contentLabel.setText("此邮件没有文字内容");
			else
				contentLabel.setText(email.getReceived());
			contentLabel.setTextFill(Color.rgb(84, 84, 84));
			contentLabel.setStyle("-fx-font-size:13px;");
			contentLabel.setWrapText(false);


			Label Time = new Label();
			Time.setLayoutX(231);
			Time.setLayoutY(66);
			Time.setPrefHeight(18);
			Time.setPrefWidth(44);
			// 将时间分割后组装到时间标签中
			if (!email.getTime().equals("")) {
				String[] time = email.getTime().split("-");
				String[] time2 = time[2].split("\\s+");
				Time.setText(time[1] + "-" + time2[0]);
			}
			Time.setTextFill(Color.rgb(111, 111, 84));
			Time.setStyle("-fx-font-size:13px;");
			Time.setWrapText(false);

			SenderPane.getChildren().addAll(topImageView, tipsImageView);
			SenderPane.getChildren().add(senderLabel);
			outPane.getChildren().add(SenderPane);
			outPane.getChildren().addAll(TitleLabel, centerImageView, contentLabel, Time);
			// Node node=Index.root.lookup("#pane_messageBox");
			// ((ScrollPane)Index.root.lookup("#pane_messageBox")).setContent(outPane);
			vBox.getChildren().add(outPane);
		}
		((ScrollPane) Index.root.lookup("#pane_messageBox")).setContent(vBox);

	}

	//载入已经发送的邮件
	private static void LoadAreadSend(EmailUser eu) throws Exception {
		((Pane) Index.root.lookup("#pane_noemails")).setVisible(false);// 关闭没有邮件的提示
		String ButtonKey = "IsInAreadySendBox";// 要打开的ile按钮的关键字
		String valueString = "1";// 打开的关键字的值

		List<Send> sendList = sDao.UserOtherEmail(Login.eu.getUserName(), ButtonKey, "1");
		String title, subject, content;
		VBox vBox = new VBox();
		vBox.setPrefWidth(278);
		vBox.setSpacing(5);
		vBox.setId("vbox_email");
		Email email = new Email();
		if (sendList.size() == 0) {
			((Pane) Index.root.lookup("#pane_noemails")).setVisible(true);
			return;
		}
		for (int i = sendList.size() - 1; i >= 0; i--) {
			email = EmailFJ.DecomposeEmail(sendList.get(i).getEmailContent());

			Pane outPane = new Pane();
			outPane.setStyle("-fx-background-radius: 10px; -fx-background-color: #fafbfc;");
			outPane.setPrefWidth(278);
			outPane.setPrefHeight(98);
			outPane.setId("rebox_" + i);
			outPane.setOnMouseEntered(email_Enter);
			outPane.setOnMouseExited(email_Exit);
			outPane.setOnMouseClicked(email_click);

			Pane SenderPane = new Pane();
			SenderPane.setStyle("");
			SenderPane.setPrefWidth(280);
			SenderPane.setPrefHeight(25);
			SenderPane.setLayoutX(-1);
			SenderPane.setLayoutY(8);

			Label senderLabel = new Label();
			senderLabel.setLayoutX(18);
			senderLabel.setLayoutY(1);
			senderLabel.setPrefHeight(15);
			senderLabel.setPrefWidth(212);
			try {
				if (CodeTranslate.getEncoding(email.getFrom()).equals("UTF-8"))
					continue;
			} catch (Exception e) {
				continue;
			}

			// {email.setFrom(CodeTranslate.getUTF8XMLString(email.getFrom()));System.out.println(email.getFrom());}
//			if(!email.getFrom().equals(""))
//			else senderLabel.setText("（无收件人）");//会引发空对象报错
			senderLabel.setText(email.getFrom());
			senderLabel.setTextFill(Color.rgb(50, 50, 50));
			senderLabel.setStyle("-fx-font-size:18px;-fx-font-weight: bold;");
			senderLabel.setWrapText(false);

			ImageView tipsImageView = new ImageView();
			tipsImageView.setImage(new Image("cn/lxm/iMail/img/1.png"));
			tipsImageView.setFitHeight(16);
			tipsImageView.setFitWidth(16);
			tipsImageView.setLayoutX(3);
			tipsImageView.setLayoutY(5);
			if (sendList.get(i).getAppendix() > 0) {

				tipsImageView.setVisible(true);
			} else
				tipsImageView.setVisible(false);
			tipsImageView.setId("Isnew_" + i);

			ImageView topImageView = new ImageView();
			topImageView.setImage(new Image("cn/lxm/iMail/img/star0.png"));
			topImageView.setFitHeight(16);
			topImageView.setFitWidth(16);
			topImageView.setLayoutX(250);
			topImageView.setLayoutY(2);
			topImageView.setId("flag_" + i);
			topImageView.setCursor(Cursor.HAND);

			topImageView.setOnMouseClicked(flag_click);

			Label TitleLabel = new Label();
			TitleLabel.setLayoutX(17);
			TitleLabel.setLayoutY(42);
			TitleLabel.setPrefHeight(15);
			TitleLabel.setPrefWidth(231);
			TitleLabel.setText(email.getSubject());
			TitleLabel.setTextFill(Color.rgb(14, 14, 14));
			TitleLabel.setStyle("-fx-font-size:14px;");
			TitleLabel.setWrapText(false);

			ImageView centerImageView = new ImageView();
			centerImageView.setImage(new Image("cn/lxm/iMail/img/accountlist_inbox.png"));
			centerImageView.setFitHeight(16);
			centerImageView.setFitWidth(16);
			centerImageView.setLayoutX(250);
			centerImageView.setLayoutY(41);
			centerImageView.setId("IsAdd_" + i);
			centerImageView.setVisible(false);

			Label contentLabel = new Label();
			contentLabel.setLayoutX(17);
			contentLabel.setLayoutY(66);
			contentLabel.setPrefHeight(18);
			contentLabel.setPrefWidth(221);
			// 邮件有文字内容的话就放上去，没有的话提示用户没文字内容
			if (email.getReceived().equals(""))
				contentLabel.setText("此邮件没有文字内容");
			else
				contentLabel.setText(email.getReceived());
			contentLabel.setTextFill(Color.rgb(84, 84, 84));
			contentLabel.setStyle("-fx-font-size:13px;");
			contentLabel.setWrapText(false);


			Label Time = new Label();
			Time.setLayoutX(231);
			Time.setLayoutY(66);
			Time.setPrefHeight(18);
			Time.setPrefWidth(44);
			// 将时间分割后组装到时间标签中
			if (!email.getTime().equals("")) {
				String[] time = email.getTime().split("-");
				String[] time2 = time[2].split("\\s+");
				Time.setText(time[1] + "-" + time2[0]);
			}
			Time.setTextFill(Color.rgb(111, 111, 84));
			Time.setStyle("-fx-font-size:13px;");
			Time.setWrapText(false);

			SenderPane.getChildren().addAll(topImageView, tipsImageView);
			SenderPane.getChildren().add(senderLabel);
			outPane.getChildren().add(SenderPane);
			outPane.getChildren().addAll(TitleLabel, centerImageView, contentLabel, Time);
			vBox.getChildren().add(outPane);
		}
		((ScrollPane) Index.root.lookup("#pane_messageBox")).setContent(vBox);


	}

	// 清除其他选中事件
	private static void ClearSelect(String orid) {
		if (!tempMeneListItemName.equals("")) {
			String buttonID = "btn_menu" + tempMeneListItemName.charAt(tempMeneListItemName.length() - 1);
			((Button) Index.root.lookup("#" + buttonID))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(tempMeneListItemName)[0]));
			((HBox) Index.root.lookup("#" + tempMeneListItemName)).setOnMouseExited(Index.menu_exit);
		}
		for (int i = 1; i < 8; i++) {
			String name = "pane_oneuserHbox" + i;
			if (!name.equals(orid)) {
				((Button) Index.root.lookup("#" + "btn_menu" + i)).setStyle("-fx-background-color:#fafbfc;");
				((HBox) Index.root.lookup("#" + name))
						.setStyle("-fx-background-color:#fafbfc;-fx-background-radius:5px");

			} else {
				((Button) Index.root.lookup("#" + "btn_menu" + i)).setStyle("-fx-background-color:#cee3f5;");
				((HBox) Index.root.lookup("#" + name))
						.setStyle("-fx-background-color:#cee3f5;-fx-background-radius:5px");
				((HBox) Index.root.lookup("#" + name)).setOnMouseExited(null);
			}
		}
		tempMeneListItemName = orid;

	}

	public static void menu_btn_exit(Node node) {

		if (!node.getId().equals("btn_menu0")) {
			String orid = node.getId();
			String buttonID = "btn_menu" + orid.charAt(orid.length() - 1);
			((Button) Index.root.lookup("#" + buttonID))
					.setGraphic(new ImageView("cn/lxm/iMail/img/" + map.get(node.getId())[0]));
		}

	}

	public static void showList_click(Node node) {

		if (!showlistFlag) {
			((Button) Index.root.lookup("#" + "btn_menu0"))
					.setStyle("-fx-background-color:#fafbfc;-fx-text-alignment:left;-fx-background-radius:5px;");
			((Pane) Index.root.lookup("#pane_oneuser_list")).setVisible(true);
			((Pane) Index.root.lookup("#pane_oneuser_list")).setPrefHeight(222);
			((ImageView) Index.root.lookup("#img_dropdown")).setImage(new Image("cn/lxm/iMail/img/icon_up(2).png"));
			final Timeline timeline = new Timeline();
			// 将x的位置在500ms内移动到300处
			RotateTransition rTransition = new RotateTransition();
			final KeyValue kv = new KeyValue(((Pane) Index.root.lookup("#pane_oneuser_list")).translateYProperty(), 0);
			final KeyFrame kf = new KeyFrame(Duration.millis(300), kv);
			// 将关键帧加到时间轴中
			timeline.getKeyFrames().add(kf);
			// timeline.getKeyFrames().add(kf2);
			timeline.play();// 运行
			showlistFlag = !showlistFlag;
		} else {

			((Button) Index.root.lookup("#" + "btn_menu0"))
					.setStyle("-fx-background-color:#cee3f5;-fx-text-alignment:left;-fx-background-radius:5px;");
			((Pane) Index.root.lookup("#pane_oneuser_list")).setVisible(false);
			((Pane) Index.root.lookup("#pane_oneuser_list")).setPrefHeight(0);
			((ImageView) Index.root.lookup("#img_dropdown")).setImage(new Image("cn/lxm/iMail/img/icon_down (2).png"));
			final Timeline timeline = new Timeline();
			// 将x的位置在500ms内移动到300处
			RotateTransition rTransition = new RotateTransition();
			final KeyValue kv = new KeyValue(((Pane) Index.root.lookup("#pane_oneuser_list")).translateYProperty(), 0);
			final KeyFrame kf = new KeyFrame(Duration.millis(300), kv);
			// 将关键帧加到时间轴中
			timeline.getKeyFrames().add(kf);
			// timeline.getKeyFrames().add(kf2);
			timeline.play();// 运行
			showlistFlag = !showlistFlag;
		}

	}

	public static void LoadEmails(EmailUser eu) throws Exception {
		Receive receive = new Receive();
		rd = new ReceiveDAO();
		list = rd.UserAllEmail(eu.getUserName());
		String title, subject, content;
		VBox vBox = new VBox();
		vBox.setPrefWidth(278);
		vBox.setSpacing(5);
		vBox.setId("vbox_email");
		Email email = new Email();

		for (int i = list.size() - 1; i >= 0; i--) {
			// System.out.println(i);
			email = EmailFJ.DecomposeEmail(list.get(i).getEmailContent());

			Pane outPane = new Pane();
			outPane.setStyle("-fx-background-radius: 10px; -fx-background-color: #fafbfc;");
			outPane.setPrefWidth(278);
			outPane.setPrefHeight(98);
			outPane.setId("rebox_" + i);
			outPane.setOnMouseEntered(email_Enter);
			outPane.setOnMouseExited(email_Exit);
			outPane.setOnMouseClicked(email_click);

			Pane SenderPane = new Pane();
			SenderPane.setStyle("");
			SenderPane.setPrefWidth(280);
			SenderPane.setPrefHeight(25);
			SenderPane.setLayoutX(-1);
			SenderPane.setLayoutY(8);

			Label senderLabel = new Label();
			senderLabel.setLayoutX(18);
			senderLabel.setLayoutY(1);
			senderLabel.setPrefHeight(15);
			senderLabel.setPrefWidth(212);
			try {
				if (CodeTranslate.getEncoding(email.getFrom()).equals("UTF-8"))
					continue;
			} catch (Exception e) {
				continue;
			}

			// {email.setFrom(CodeTranslate.getUTF8XMLString(email.getFrom()));System.out.println(email.getFrom());}
			senderLabel.setText(email.getFrom());
			senderLabel.setTextFill(Color.rgb(50, 50, 50));
			senderLabel.setStyle("-fx-font-size:18px;-fx-font-weight: bold;");
			senderLabel.setWrapText(false);

			ImageView tipsImageView = new ImageView();
			tipsImageView.setImage(new Image("cn/lxm/iMail/img/1.png"));
			tipsImageView.setFitHeight(16);
			tipsImageView.setFitWidth(16);
			tipsImageView.setLayoutX(3);
			tipsImageView.setLayoutY(5);
			if (list.get(i).getAppendix() > 0) {
				newEmailCount++;
				tipsImageView.setVisible(true);
			} else
				tipsImageView.setVisible(false);
			tipsImageView.setId("Isnew_" + i);

			ImageView topImageView = new ImageView();
			if (list.get(i).getIsInFlagBox() > 0) {
				topImageView.setImage(new Image("cn/lxm/iMail/img/star1.png"));
				topImageView.setVisible(true);
			} else {
				topImageView.setImage(new Image("cn/lxm/iMail/img/star0.png"));
				topImageView.setVisible(false);
			}
			topImageView.setFitHeight(16);
			topImageView.setFitWidth(16);
			topImageView.setLayoutX(250);
			topImageView.setLayoutY(2);
			topImageView.setId("flag_" + i);
			topImageView.setCursor(Cursor.HAND);

			topImageView.setOnMouseClicked(flag_click);

			Label TitleLabel = new Label();
			TitleLabel.setLayoutX(17);
			TitleLabel.setLayoutY(42);
			TitleLabel.setPrefHeight(15);
			TitleLabel.setPrefWidth(231);
			TitleLabel.setText(email.getSubject());
			TitleLabel.setTextFill(Color.rgb(14, 14, 14));
			TitleLabel.setStyle("-fx-font-size:14px;");
			TitleLabel.setWrapText(false);

			ImageView centerImageView = new ImageView();
			centerImageView.setImage(new Image("cn/lxm/iMail/img/accountlist_inbox.png"));
			centerImageView.setFitHeight(16);
			centerImageView.setFitWidth(16);
			centerImageView.setLayoutX(250);
			centerImageView.setLayoutY(41);
			centerImageView.setId("IsAdd_" + i);
			centerImageView.setVisible(false);

			Label contentLabel = new Label();
			contentLabel.setLayoutX(17);
			contentLabel.setLayoutY(66);
			contentLabel.setPrefHeight(18);
			contentLabel.setPrefWidth(221);
			// 邮件有文字内容的话就放上去，没有的话提示用户没文字内容
			if (email.getReceived().equals(""))
				contentLabel.setText("此邮件没有文字内容");
			else
				contentLabel.setText(email.getReceived());
			contentLabel.setTextFill(Color.rgb(84, 84, 84));
			contentLabel.setStyle("-fx-font-size:13px;");
			contentLabel.setWrapText(false);

			Label Time = new Label();
			Time.setLayoutX(231);
			Time.setLayoutY(66);
			Time.setPrefHeight(18);
			Time.setPrefWidth(44);
			// 将时间分割后组装到时间标签中
			if (!email.getTime().equals("")) {
				String[] time = email.getTime().split("-");
				String[] time2 = time[2].split("\\s+");
				Time.setText(time[1] + "-" + time2[0]);
			}
			Time.setTextFill(Color.rgb(111, 111, 84));
			Time.setStyle("-fx-font-size:13px;");
			Time.setWrapText(false);

			SenderPane.getChildren().addAll(topImageView, tipsImageView);
			SenderPane.getChildren().add(senderLabel);
			outPane.getChildren().add(SenderPane);
			outPane.getChildren().addAll(TitleLabel, centerImageView, contentLabel, Time);
			// Node node=Index.root.lookup("#pane_messageBox");
			// ((ScrollPane)Index.root.lookup("#pane_messageBox")).setContent(outPane);
			vBox.getChildren().add(outPane);

			//保存一些数据
			list.get(i).setFromEmail(email.getFrom());
			rd.update(list.get(i));
		}
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				((ScrollPane) Index.root.lookup("#pane_messageBox")).setContent(vBox);
				if (newEmailCount > 0) {
					((Button) Index.root.lookup("#btn_menu1")).setText("收件箱(" + newEmailCount + ")");
				}
			}
		});

	}

	public static EventHandler<MouseEvent> email_Enter = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			String node = ((Node) event.getSource()).getId();
			((Pane) Index.root.lookup("#" + node))
					.setStyle("-fx-background-radius: 10px; -fx-background-color: #d9eafa;");
			String[] flagString = node.split("_");
			((ImageView) Index.root.lookup("#flag_" + flagString[1])).setVisible(true);
		}

	};
	public static EventHandler<MouseEvent> email_Exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			String node = ((Node) event.getSource()).getId();
			((Pane) Index.root.lookup("#" + node))
					.setStyle("-fx-background-radius: 10px; -fx-background-color: #fafbfc;");
			String[] flagString = node.split("_");
			if (list.get(Integer.parseInt(flagString[1])).getIsInFlagBox() <= 0)
				((ImageView) Index.root.lookup("#flag_" + flagString[1])).setVisible(false);
		}

	};
	public static EventHandler<MouseEvent> email_click = new EventHandler<MouseEvent>() {
		String urlPath = "";

		@Override
		public void handle(MouseEvent event) {

			if (event.getClickCount() == 1) {
				String node = ((Node) event.getSource()).getId();
				String names[] = node.split("_");
				try {
					((Pane) Index.root.lookup("#" + ClickID)).setOnMouseExited(email_Exit);
					((Pane) Index.root.lookup("#" + ClickID))
							.setStyle("-fx-background-radius: 10px; -fx-background-color: #fafbfc;");
				} catch (Exception e) {
					System.out.println("找不到这个ID");
				}

				((Pane) Index.root.lookup("#" + node))
						.setStyle("-fx-background-radius: 10px; -fx-background-color: #d9eafa;");
				((Pane) Index.root.lookup("#" + node)).setOnMouseExited(null);
				ClickID = node;
				Email email = new Email();
				try {
					email = EmailFJ.DecomposeEmail(list.get(Integer.parseInt(names[1])).getEmailContent());
				} catch (NumberFormatException e1) {

					e1.printStackTrace();
				} catch (Exception e1) {

					e1.printStackTrace();
				}
				final WebEngine webEngine = ((WebView) Index.root.lookup("#web_webview")).getEngine();
				try {
					urlPath = MakeupHtmlEmails.MakeupEmail(email);
					webEngine.load(new File(urlPath).toURI().toString());
				} catch (IOException e) {
					e.printStackTrace();
				}

				// 去掉新邮件提醒
				if (list.get(Integer.parseInt(names[1])).getAppendix() > 0) {
					newEmailCount--;
					((ImageView) Index.root.lookup("#" + "Isnew_" + Integer.parseInt(names[1]))).setVisible(false);
					((ImageView) Index.root.lookup("#" + "Isnew_" + Integer.parseInt(names[1]))).setImage(null);
					list.get(Integer.parseInt(names[1])).setAppendix(Short.valueOf((short) 0));
					rd.update(list.get(Integer.parseInt(names[1])));
					if (newEmailCount > 0) {
						((Button) Index.root.lookup("#btn_menu1")).setText("收件箱(" + newEmailCount + ")");
					} else {
						((Button) Index.root.lookup("#btn_menu1")).setText("收件箱");
					}

				}

			}
			// 双击事件，弹出大邮件详情
			else if (event.getClickCount() == 2) {
				Platform.runLater(new Runnable() {
					public void run() {
						try {
							new EmailDetail().start(new Stage());
							final WebEngine webEngine = ((WebView) (EmailDetail.root.lookup("#webview_detail")))
									.getEngine();
							webEngine.load(new File(urlPath).toURI().toString());
						} catch (Exception e) {

							e.printStackTrace();
						}
					}

				});
			}
		}

	};

	public static EventHandler<MouseEvent> flag_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {

			String node = ((Node) event.getSource()).getId();
			String names[] = node.split("_");
			if (list.get(Integer.parseInt(names[1])).getIsInFlagBox() > 0) {
				((ImageView) Index.root.lookup("#" + node)).setVisible(false);
				((ImageView) Index.root.lookup("#" + node)).setImage(new Image("cn/lxm/iMail/img/star0.png"));
				list.get(Integer.parseInt(names[1])).setIsInFlagBox(0);
				rd.update(list.get(Integer.parseInt(names[1])));
			} else {
				((ImageView) Index.root.lookup("#" + node)).setVisible(true);
				((ImageView) Index.root.lookup("#" + node)).setImage(new Image("cn/lxm/iMail/img/star1.png"));
				list.get(Integer.parseInt(names[1])).setIsInFlagBox(1);
				rd.update(list.get(Integer.parseInt(names[1])));
			}

		}

	};

	// 彻底删除邮件
	public static EventHandler<MouseEvent> finalDelete_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {

//			EmailUser eu = new EmailUser();
//			eu.setUserName("13557330070@139.com");
//			eu.setUserPsw("81440223");
//			eu.setEmailCount(114);
//			eu.setHost("pop3.139.com");

			// new Thread() {
			// @Override
			// public void run() {

			IsDeleteFlag = ConnectWithPop3.deleteEmailByID(Login.eu,
					Integer.parseInt(list.get(Integer.parseInt(ClickID.split("_")[1])).getTime()));
			if (IsDeleteFlag) {
				System.out.println("已彻底删除");
				// 如果已经删除的话，把数据库中所有邮件的Time字段-1
				List<Receive> list2 = rd.UserAllEmail(Login.eu.getUserName());
				for (int i = Integer.parseInt(ClickID.split("_")[1]); i < list2.size(); i++) {
					Receive rv = list2.get(i);
					rv.setTime(String.valueOf((Integer.parseInt(rv.getTime()) - 1)));
					rd.update(rv);
				}
			} else
				System.out.println("删除失败");
			// super.run();
			// }
			// };

			Receive receive = new Receive();
			receive = list.get(Integer.parseInt(ClickID.split("_")[1]));
			rd.delete(receive.getId());
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					((VBox) (Index.root.lookup("#vbox_email"))).getChildren()
							.remove(Index.root.lookup("#rebox_" + Integer.parseInt(ClickID.split("_")[1])));
					// 显示没有邮件的页面
					if (list.size() == 0) {
						((Pane) Index.root.lookup("#pane_noemails")).setVisible(true);
						return;
					}

				}

			});

		}

	};

}
