package cn.lxm.iMail.Logic;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JOptionPane;

import com.sun.org.apache.regexp.internal.recompile;

import cn.lxm.iMail.UI.WriteEmail;
import cn.lxm.iMail.Util.Log;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import sun.util.logging.resources.logging;

public class CommitWebBroswer {
	public static String htmlcode="";
	Log log = new Log();
	// 负责与浏览器的交互
	public void getHtmlEditorContent(String str) {
		log.log(str);
		WriteEmail.email.setContent(str);
		return;

	}
	public void savefile() {

		log.log("文件");
	}

	public void setHtmlEditorContent() {

		String script = "SetTinyMceContent(\""+htmlcode+"\")";
		WriteEmail.webEngine.executeScript(script);// 执行脚本
		return;

	}
	public String selectFile() {
		String AticleURL = "";
		String UserAticleName = null;
		String UserAticleContent = "";
		BufferedReader in = null;
		/*------------------用缓冲流来实现文章读取--------------*/

		try {
			FileDialog fdopen = new FileDialog(new Frame(), "打开", FileDialog.LOAD);// 框属性为"LOAD加载"，附于JFrame对象
			fdopen.setVisible(true);
			UserAticleName = fdopen.getFile();
			in = new BufferedReader(new FileReader(fdopen.getDirectory() + fdopen.getFile()));// 选择路径，为文件流对象,fdopen.getFile()是获取文件名的方法
			if (!(UserAticleName == null)) {
				UserAticleName = fdopen.getFile();
				AticleURL = fdopen.getDirectory() + fdopen.getFile();// 获取文件的路径
				WriteEmail.fileName.add(AticleURL);
				WriteEmail.FileCount++;
				addFile(fdopen.getFile());
				in.close();
				fdopen.show(false);
				fdopen=null;
				fdopen.dispose();
				return AticleURL;
			}

		} catch (IOException e) {

			System.out.println("文件读取失败");

		}
		return null;
	}

	private void addFile(String file) {

		Pane p1=new Pane();
		p1.setStyle("-fx-border-color:#eee");
		String type=getType(file);
		ImageView iView=new ImageView(new Image("cn/lxm/iMail/img/"+type));
		iView.setFitHeight(35);
		iView.setFitWidth(34);
		p1.setId("addFilePane_"+WriteEmail.FileCount);
		p1.setOnMouseEntered(entry);
		p1.setOnMouseExited(exit);

		Text fileName=new Text();
		fileName.setText(file);
		fileName.setLayoutX(36);
		fileName.setLayoutY(21);
		fileName.setStyle("-fx-fill:#222;-fx-font-size:13px;");

		ImageView close=new ImageView(new Image("cn/lxm/iMail/img/accountlist_spam_pressed.png"));
		close.setFitHeight(13);
		close.setFitWidth(13);
		close.setLayoutX(105);
		close.setLayoutY(-2);
		close.setCursor(Cursor.HAND);
		close.setId("close_"+WriteEmail.FileCount);
		close.setOnMouseClicked(delete_click);

		p1.getChildren().addAll(iView,fileName,close);

		((HBox)WriteEmail.root.lookup("#pane_addFile")).getChildren().add(p1);
	}

	private String getType(String file) {
		String names[]=file.split("\\.");
		if(names.length<=1) return "icon_unkown.png";
		String type=names[1];
		if(type.equals("ai")) return "icon_ai.png";
		else if(type.equals("xlsx")) return "icon_excel.png";
		else if(type.equals("eml")) return "icon_exchange.png";
		else if(type.equals("exe")) return "icon_exe.png";
		else if(type.equals("fl")) return "icon_fl.png";
		else if(type.equals("eml")) return "icon_gmail.png";
		else if(type.equals("html")) return "icon_html.png";
		else if(type.equals("mp3") || type.equals("wav") || type.toLowerCase().equals("flac")) return "icon_music.png";
		else if(type.equals("pdf") ) return "icon_pdf.png";
		else if(type.equals("png") || type.equals("jpg") || type.equals("jpeg") || type.toLowerCase().equals("gif")) return "icon_pic.png";
		else if(type.equals("pptx")) return "icon_ppt.png";
		else if(type.equals("psd")) return "icon_ps.png";
		else if(type.equals("swf")) return "icon_swf.png";
		else if(type.equals("docx")) return "icon_word.png";
		else if(type.equals("txt")) return "icon_txt.png";
		else if(type.equals("mp4") || type.equals("mkv") ||type.equals("rmvb")) return "icon_video.png";
		else
			return "icon_unkown.png";
	}

	public  EventHandler<MouseEvent> exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {

			String name[]=((Node)event.getSource()).getId().split("_");
			((Pane)WriteEmail.root.lookup("#addFilePane_"+name[1])).setStyle("-fx-border-color:#eee;-fx-background-color:#fafbfc");


		}

	};

	public  EventHandler<MouseEvent> entry = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			String name[]=((Node)event.getSource()).getId().split("_");
			((Pane)WriteEmail.root.lookup("#addFilePane_"+name[1])).setStyle("-fx-border-color:#eee;-fx-background-color:#d9eafa");

		}

	};

	public  EventHandler<MouseEvent> delete_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			WriteEmail.FileCount--;
			String name[]=((Node)event.getSource()).getId().split("_");
			((HBox)WriteEmail.root.lookup("#pane_addFile")).getChildren().remove((WriteEmail.root.lookup("#addFilePane_"+name[1])));
		}

	};

}
