package cn.lxm.iMail.Util;

import java.beans.Encoder;
import java.io.UnsupportedEncodingException;

import javax.mail.internet.MimeMessage;

import com.sun.org.apache.bcel.internal.generic.NEW;

import javassist.compiler.ast.NewExpr;

public class decode7bit {
	public static String DecodeUCS2(String src) throws Exception {
		byte[] bytes = new byte[src.length() / 2];

		for (int i = 0; i < src.length(); i += 2) {
			bytes[i / 2] = (byte) (Integer.parseInt(src.substring(i, i + 2), 16));
		}
		String reValue;
		try {
			reValue = new String(bytes, "UTF-16BE");
		} catch (Exception e) {
			throw new Exception(e);
		}
		return reValue;

	}

	public static String decode7bit(byte[] d) {
		byte[] other_mask = { (byte) 0x80, (byte) 0xc0, (byte) 0xe0, (byte) 0xf0, (byte) 0xf8, (byte) 0xfc,
				(byte) 0xfe };
		byte[] my_mask = { 0x7f, 0x3f, 0x1f, 0x0f, 0x07, 0x03, 0x01 };
		byte other = 0;
		byte temp = 0;
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < d.length; i++) {
			int index = i % 7;
			temp = d[i];
			// 得到我的数据
			d[i] = (byte) (d[i] & my_mask[index]);
			d[i] = (byte) (d[i] << index);
			if (index != 0) {
				d[i] = (byte) (d[i] & other_mask[7 - index]);
				other = (byte) (other >> (8 - index));
				other = (byte) (other & my_mask[7 - index]);
				d[i] = (byte) (d[i] | other);
			}
			// 先把下一个数据信息拿走
			other = (byte) (temp & other_mask[index]);
			sb.append((char) d[i]);
			if (index == 6) {
				other = (byte) (other >> 1);
				other = (byte) (other & 0x7f);
				sb.append((char) other);
			}
		}
		return sb.toString();
	}

	public static String EncodeUCS2(String src) throws Exception {

		byte[] bytes;
		try {
			bytes = src.getBytes("UTF-16BE");
		} catch (UnsupportedEncodingException e) {
			throw new Exception(e);
		}

		StringBuffer reValue = new StringBuffer();
		StringBuffer tem = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			tem.delete(0, tem.length());
			tem.append(Integer.toHexString(bytes[i] & 0xFF));
			if (tem.length() == 1) {
				tem.insert(0, '0');
			}
			reValue.append(tem);
		}
		//System.out.println(reValue.toString().toUpperCase());
		return reValue.toString().toUpperCase();
	}

	public static void main(String[] args) throws Exception {

		String subject = new String("xPq1xNLGtq+7pcGqzfjNqNDQ1qTS0b+qzaijrLu2062808jro6E==".getBytes("ISO8859_1"), "GBK");
		sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
		sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
		byte[] bString = decoder.decodeBuffer(subject);
		String a = encoder.encode("【139邮箱】100%中奖，超多实物奖品免费领，水果、数码、电影票等，快来试试吧".getBytes("gbk"));
		System.out.println(a);
		String sadString = new String(bString);
		System.out.println(sadString);
		// MimeMessage mimeMessage = new MimeMessage(mimeMessage);
		// mimeMessage.getHeader("Subject");
	}
}
