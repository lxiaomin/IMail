package cn.lxm.iMail.Util;

import java.io.*;
import java.util.Date;

public class ReadFile {

	public ReadFile() {

	}

	public static String readFileByChars(String fileName) throws IOException {
		String FileContent = "";
		FileInputStream fis = new FileInputStream(fileName);
		InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		String line = null;
		while ((line = br.readLine()) != null) {
			FileContent += line;
			FileContent += "\r\n"; // 补上换行符
		}
		return FileContent;
	}

	// public static void main(String[] args) throws IOException {
	// readFileByChars("tinymce_4.5.5/tinymce/js/tinymce/viewer.html");
	//
	// }
	public static String WriteFileUTF_8(String fileContent) throws IOException {
		String Dir = "tinymce_4.5.5/tinymce/js/tinymce/view" + new Date().hashCode()+".html";
		File cacheDir = new File(Dir);
		FileOutputStream fos = new FileOutputStream(Dir);
		OutputStreamWriter osw = new OutputStreamWriter(fos, "gbk");
		osw.write(fileContent);
		osw.flush();
		return Dir;
	}
	public static String WriteFileUTF_8_2(String fileContent) throws IOException {
		long name=new Date().hashCode();
		String Dir = "tinymce_4.5.5/tinymce/js/tinymce/view" +name +".html";
		File cacheDir = new File(Dir);
		FileOutputStream fos = new FileOutputStream(Dir);
		OutputStreamWriter osw = new OutputStreamWriter(fos, "gbk");
		osw.write(fileContent);
		osw.flush();
		return "view"+name+".html";
	}

	public static String WriteEMLFile(String fileContent) throws IOException {
		String Dir = "eml/" + new Date().hashCode()+".eml";
		File cacheDir = new File(Dir);
		FileOutputStream fos = new FileOutputStream(Dir);
		OutputStreamWriter osw = new OutputStreamWriter(fos, "gbk");
		osw.write(fileContent);
		osw.flush();
		return Dir;
	}



	/**
     * 删除单个文件
     *
     * @param fileName 要删除的文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
