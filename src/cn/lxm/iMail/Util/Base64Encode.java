package cn.lxm.iMail.Util;

public class Base64Encode {

	static sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
	public Base64Encode() {

	}

	public static String encode(String code) {
		String result = "";
		encoder = new sun.misc.BASE64Encoder();
		return encoder.encode(code.getBytes());
	}

	public static String encodeByByte(byte[] code) {
		String result = "";
		encoder = new sun.misc.BASE64Encoder();
		return encoder.encode(code);
	}

}
