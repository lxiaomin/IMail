package cn.lxm.iMail.Util;

import java.util.HashMap;
import java.util.Map;

public class ResponseCodeMeaning {

	private static HashMap<Integer, String> codeMatchMap=new HashMap<Integer, String>();
	public ResponseCodeMeaning() {
		codeMatchMap.put(501,"参数格式错误 ");
		codeMatchMap.put(502,"命令不可实现 ");
		codeMatchMap.put(503,"错误的命令序列，接收邮箱格式错误（例如xx@dd,正确格式应该xx@dd.es) ");
		codeMatchMap.put(504,"命令参数不可实现 ");
		codeMatchMap.put(211,"系统状态或系统帮助响应 ");
		codeMatchMap.put(214,"帮助信息 ");
		codeMatchMap.put(220,"＜domain＞服务就绪 ");
		codeMatchMap.put(221,"＜domain＞服务关闭 ");
		codeMatchMap.put(421,"＜domain＞服务未就绪，关闭传输信道 ");
		codeMatchMap.put(250,"要求的邮件操作完成 ");
		codeMatchMap.put(251,"用户非本地，将转发向＜forward-path＞ ");
		codeMatchMap.put(450,"要求的邮件操作未完成，邮箱不可用 ");
		codeMatchMap.put(550,"要求的邮件操作未完成，邮箱不可用 ");
		codeMatchMap.put(451,"放弃要求的操作；处理过程中出错 ");
		codeMatchMap.put(551,"用户非本地，请尝试＜forward-path＞ ");
		codeMatchMap.put(452,"系统存储不足，要求的操作未执行 ");
		codeMatchMap.put(454,"临时认证失败,可能账号被临时冻结");
		codeMatchMap.put(552,"过量的存储分配，要求的操作未执行 ");
		codeMatchMap.put(553,"邮箱名不可用，要求的操作未执行(有的服务器在邮箱账号被临时这样提示的)");
		codeMatchMap.put(354,"开始邮件输入，以\".\"结束 ");
		codeMatchMap.put(554,"操作失败");
	}
	public String match(int code)
	{
		return codeMatchMap.get(code);
	}

}
