package cn.lxm.iMail.Hibernate.DAO;

import java.util.List;

import org.hibernate.*;
import org.hibernate.query.Query;

import cn.lxm.iMail.Hibernate.model.Loginlog;
import cn.lxm.iMail.Hibernaye.Utils.HibernateUtils;
import cn.lxm.iMail.Hibernaye.Utils.*;

public class LoginlogDAO {

    /*
     * 保存
     */
    public void save(Loginlog Loginlog) {
        Session session = HibernateUtils.openSession();
        try {
            Transaction tx = session.beginTransaction(); // 开启事务
            session.save(Loginlog);
            tx.commit(); // 提交事务
        } catch (RuntimeException e) {
            session.getTransaction().rollback(); // 回滚事务
            throw e;
        } finally {
            session.close(); // 关闭session
        }
    }

    /*
     * 更新
     */
    public void update(Loginlog Loginlog) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            session.update(Loginlog);// 操作

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 删除
     */
    public void delete(int id) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Object Loginlog = session.get(Loginlog.class, id); // 要先获取到这个对象
            session.delete(Loginlog); // 删除的是实体对象

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 根据id查询一个Loginlog数据
     */
    public Loginlog getById(int id) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Loginlog Loginlog = (Loginlog) session.get(Loginlog.class, id);// 操作
            tx.commit();
            return Loginlog;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 根据邮箱号查询一个Loginlog数据
     */
    @SuppressWarnings("deprecation")
	public Loginlog getByEmail(String email) {
    	 Session session = HibernateUtils.openSession();
        try {

             String hqlString="SELECT l FROM Loginlog l WHERE l.toEmail = :toEmail";
             Query query=session.createQuery(hqlString);
             query.setString("toEmail", email);
             List<Loginlog> list=query.list();
             if(list.isEmpty()) return null;
             return list.get(0);
        } catch (RuntimeException e) {

            throw e;
        } finally {
        	session.close();
        }
    }

    /*
     * 查询所有
     */
    public List<Loginlog> findAll() {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            // 方式一：使用HQL语句
            List<Loginlog> list = session.createQuery("FROM Loginlog").list(); // 使用HQL查询

            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * 分页的查询数据列表
     * @param firstResult 从结果列表中的哪个索引开始取数据
     * @param maxResults 最多取多少条数据
     * @return 一页的数据列表
     */
    @SuppressWarnings("unchecked")
    public QueryResult findAll(int firstResult, int maxResults) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            // 查询一页的数据列表
            // 方式一：
            // Query query = session.createQuery("FROM Loginlog");
            // query.setFirstResult(firstResult);
            // query.setMaxResults(maxResults);
            // List<Loginlog> list = query.list(); // 使用HQL查询

            // 方式二：方法链
            List<Loginlog> list = session.createQuery( //
                    "FROM Loginlog") //
                    .setFirstResult(firstResult) //
                    .setMaxResults(maxResults) //
                    .list();

            // 查询总记录数
            // session.createQuery("SELECT COUNT(*) FROM Loginlog").list().get(0);
            // Long count = (Long) session.createQuery("SELECT COUNT(*) FROM Loginlog").uniqueResult();
            Long count = (Long) session.createQuery( //
                    "SELECT COUNT(*) FROM Loginlog") //
                    .uniqueResult();
            tx.commit();

            // 返回结果
            return new QueryResult(count.intValue(), list);
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}