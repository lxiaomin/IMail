package cn.lxm.iMail.Hibernate.DAO;

import java.util.List;

import org.hibernate.*;
import org.hibernate.query.Query;
import cn.lxm.iMail.Hibernate.model.Send;
import cn.lxm.iMail.Hibernaye.Utils.HibernateUtils;
import cn.lxm.iMail.Hibernaye.Utils.QueryResult;

public class SendDAO {

    /*
     * 保存
     */
    public void save(Send Send) {
        Session session = HibernateUtils.openSession();
        try {
            Transaction tx = session.beginTransaction(); // 开启事务
            session.save(Send);
            tx.commit(); // 提交事务
        } catch (RuntimeException e) {
            session.getTransaction().rollback(); // 回滚事务
            throw e;
        } finally {
            session.close(); // 关闭session
        }
    }

    /*
     * 更新
     */
    public void update(Send Send) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            session.update(Send);// 操作

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 删除
     */
    public void delete(int id) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Object Send = session.get(Send.class, id); // 要先获取到这个对象
            session.delete(Send); // 删除的是实体对象

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 根据id查询一个Send数据
     */
    public Send getById(int id) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Send Send = (Send) session.get(Send.class, id);// 操作
            tx.commit();
            return Send;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 查询所有
     */
    public List<Send> findAll() {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            // 方式一：使用HQL语句
            List<Send> list = session.createQuery("FROM Send").list(); // 使用HQL查询

            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }



    /*
     * 查找特定的邮件
     * */

    public List<Send> UserOtherEmail(String userEmail,String key,String value) {

        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String hqlString="SELECT s FROM Send s WHERE s.senderEmail = :senderEmail";
           System.out.println(hqlString);
            Query query=session.createQuery(hqlString);
            query.setString("senderEmail", userEmail);
            List<Send> list=query.list();
//            // 方式一：使用HQL语句
//            List<Send> list = session.createQuery("SELECT r FROM Send r WHERE r.toEmail = :toEmail").list(); // 使用HQL查询
            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    public List<Send> Isend(String userEmail1,String userEmail2) {

        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String hqlString="SELECT s FROM Send s WHERE s.reciverEmail = :reciverEmail AND s.senderEmail = :senderEmail";
            System.out.println(hqlString);
            Query query=session.createQuery(hqlString);
            query.setString("reciverEmail", userEmail2);
            query.setString("senderEmail", userEmail1);
            List<Send> list=query.list();
//            // 方式一：使用HQL语句
//            List<Send> list = session.createQuery("SELECT r FROM Send r WHERE r.toEmail = :toEmail").list(); // 使用HQL查询
            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }


    /**
     * 分页的查询数据列表
     * @param firstResult 从结果列表中的哪个索引开始取数据
     * @param maxResults 最多取多少条数据
     * @return 一页的数据列表
     */
    @SuppressWarnings("unchecked")
    public QueryResult findAll(int firstResult, int maxResults) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            // 查询一页的数据列表
            // 方式一：
            // Query query = session.createQuery("FROM Send");
            // query.setFirstResult(firstResult);
            // query.setMaxResults(maxResults);
            // List<Send> list = query.list(); // 使用HQL查询

            // 方式二：方法链
            List<Send> list = session.createQuery( //
                    "FROM Send") //
                    .setFirstResult(firstResult) //
                    .setMaxResults(maxResults) //
                    .list();

            // 查询总记录数
            // session.createQuery("SELECT COUNT(*) FROM Send").list().get(0);
            // Long count = (Long) session.createQuery("SELECT COUNT(*) FROM Send").uniqueResult();
            Long count = (Long) session.createQuery( //
                    "SELECT COUNT(*) FROM Send") //
                    .uniqueResult();
            tx.commit();

            // 返回结果
            return new QueryResult(count.intValue(), list);
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}