package cn.lxm.iMail.Hibernate.DAO;

import java.util.List;

import org.hibernate.*;
import org.hibernate.query.Query;

import cn.lxm.iMail.Hibernate.model.Contacts;
import cn.lxm.iMail.Hibernate.model.Receive;
import cn.lxm.iMail.Hibernaye.Utils.HibernateUtils;
import cn.lxm.iMail.Hibernaye.Utils.QueryResult;
import cn.lxm.iMail.UI.Contact;

public class ContactsDAO {

    /*
     * 保存
     */
    public void save(Contacts Contacts) {
        Session session = HibernateUtils.openSession();
        try {
            Transaction tx = session.beginTransaction(); // 开启事务
            session.save(Contacts);
            tx.commit(); // 提交事务
        } catch (RuntimeException e) {
            session.getTransaction().rollback(); // 回滚事务
            throw e;
        } finally {
            session.close(); // 关闭session
        }
    }

    /*
     * 更新
     */
    public void update(Contacts Contacts) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            session.update(Contacts);// 操作

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 删除
     */
    public void delete(int id) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Object Contacts = session.get(Contacts.class, id); // 要先获取到这个对象
            session.delete(Contacts); // 删除的是实体对象

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 根据id查询一个Contacts数据
     */
    public Contacts getById(int id) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Contacts Contacts = (Contacts) session.get(Contacts.class, id);// 操作
            tx.commit();
            return Contacts;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 查询所有
     */
    public List<Contacts> findAll() {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            // 方式一：使用HQL语句
            List<Contacts> list = session.createQuery("FROM Contacts").list(); // 使用HQL查询

            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }
    public List<Contacts> findUserContact(String userEmail) {

        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String hqlString="SELECT c FROM Contacts c WHERE c.toEmail = :toEmail";
            Query query=session.createQuery(hqlString);
            query.setString("toEmail", userEmail);
            List<Contacts> list=query.list();
            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * 分页的查询数据列表
     * @param firstResult 从结果列表中的哪个索引开始取数据
     * @param maxResults 最多取多少条数据
     * @return 一页的数据列表
     */
    @SuppressWarnings("unchecked")
    public QueryResult findAll(int firstResult, int maxResults) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            // 查询一页的数据列表
            // 方式一：
            // Query query = session.createQuery("FROM Contacts");
            // query.setFirstResult(firstResult);
            // query.setMaxResults(maxResults);
            // List<Contacts> list = query.list(); // 使用HQL查询

            // 方式二：方法链
            List<Contacts> list = session.createQuery( //
                    "FROM Contacts") //
                    .setFirstResult(firstResult) //
                    .setMaxResults(maxResults) //
                    .list();

            // 查询总记录数
            // session.createQuery("SELECT COUNT(*) FROM Contacts").list().get(0);
            // Long count = (Long) session.createQuery("SELECT COUNT(*) FROM Contacts").uniqueResult();
            Long count = (Long) session.createQuery( //
                    "SELECT COUNT(*) FROM Contacts") //
                    .uniqueResult();
            tx.commit();

            // 返回结果
            return new QueryResult(count.intValue(), list);
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}