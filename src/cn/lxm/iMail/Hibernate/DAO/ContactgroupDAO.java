package cn.lxm.iMail.Hibernate.DAO;

import java.util.List;

import org.hibernate.*;

import cn.lxm.iMail.Hibernate.model.Contactgroup;
import cn.lxm.iMail.Hibernaye.Utils.HibernateUtils;
import cn.lxm.iMail.Hibernaye.Utils.QueryResult;

public class ContactgroupDAO {

    /*
     * 保存
     */
    public void save(Contactgroup Contactgroup) {
        Session session = HibernateUtils.openSession();
        try {
            Transaction tx = session.beginTransaction(); // 开启事务
            session.save(Contactgroup);
            tx.commit(); // 提交事务
        } catch (RuntimeException e) {
            session.getTransaction().rollback(); // 回滚事务
            throw e;
        } finally {
            session.close(); // 关闭session
        }
    }

    /*
     * 更新
     */
    public void update(Contactgroup Contactgroup) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            session.update(Contactgroup);// 操作

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 删除
     */
    public void delete(int id) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Object Contactgroup = session.get(Contactgroup.class, id); // 要先获取到这个对象
            session.delete(Contactgroup); // 删除的是实体对象

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 根据id查询一个Contactgroup数据
     */
    public Contactgroup getById(int id) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Contactgroup Contactgroup = (Contactgroup) session.get(Contactgroup.class, id);// 操作
            tx.commit();
            return Contactgroup;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 查询所有
     */
    public List<Contactgroup> findAll() {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            // 方式一：使用HQL语句
            List<Contactgroup> list = session.createQuery("FROM Contactgroup").list(); // 使用HQL查询

            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * 分页的查询数据列表
     * @param firstResult 从结果列表中的哪个索引开始取数据
     * @param maxResults 最多取多少条数据
     * @return 一页的数据列表
     */
    @SuppressWarnings("unchecked")
    public QueryResult findAll(int firstResult, int maxResults) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            // 查询一页的数据列表
            // 方式一：
            // Query query = session.createQuery("FROM Contactgroup");
            // query.setFirstResult(firstResult);
            // query.setMaxResults(maxResults);
            // List<Contactgroup> list = query.list(); // 使用HQL查询

            // 方式二：方法链
            List<Contactgroup> list = session.createQuery( //
                    "FROM Contactgroup") //
                    .setFirstResult(firstResult) //
                    .setMaxResults(maxResults) //
                    .list();

            // 查询总记录数
            // session.createQuery("SELECT COUNT(*) FROM Contactgroup").list().get(0);
            // Long count = (Long) session.createQuery("SELECT COUNT(*) FROM Contactgroup").uniqueResult();
            Long count = (Long) session.createQuery( //
                    "SELECT COUNT(*) FROM Contactgroup") //
                    .uniqueResult();
            tx.commit();

            // 返回结果
            return new QueryResult(count.intValue(), list);
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}