package cn.lxm.iMail.Hibernate.DAO;

import java.util.List;

import org.hibernate.*;
import org.hibernate.query.Query;

import cn.lxm.iMail.Hibernate.model.Loginlog;
import cn.lxm.iMail.Hibernate.model.Receive;
import cn.lxm.iMail.Hibernaye.Utils.HibernateUtils;
import cn.lxm.iMail.Hibernaye.Utils.QueryResult;

public class ReceiveDAO {

    /*
     * 保存
     */
    public void save(Receive Receive) {
        Session session = HibernateUtils.openSession();
        try {
            Transaction tx = session.beginTransaction(); // 开启事务
            session.save(Receive);
            tx.commit(); // 提交事务
        } catch (RuntimeException e) {
            session.getTransaction().rollback(); // 回滚事务
            throw e;
        } finally {
            session.close(); // 关闭session
        }
    }

    /*
     * 更新
     */
    public void update(Receive Receive) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            session.update(Receive);// 操作

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 删除
     */
    public void delete(int id) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            Object Receive = session.get(Receive.class, id); // 要先获取到这个对象
            session.delete(Receive); // 删除的是实体对象

            tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 根据id查询一个Receive数据
     */
    public Receive getById(int id) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Receive Receive = (Receive) session.get(Receive.class, id);// 操作
            tx.commit();
            return Receive;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 查询所有
     */
    public List<Receive> findAll() {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            // 方式一：使用HQL语句
            List<Receive> list = session.createQuery("FROM Receive").list(); // 使用HQL查询

            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 查找所有特定邮件的集合
     * */
    public List<Receive> UserAllEmail(String userEmail) {

        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String hqlString="SELECT r FROM Receive r WHERE r.toEmail = :toEmail AND r.isInDeleteBox = :value";
            Query query=session.createQuery(hqlString);
            query.setString("toEmail", userEmail);
            query.setInteger("value", 0);
            List<Receive> list=query.list();
//            // 方式一：使用HQL语句
//            List<Receive> list = session.createQuery("SELECT r FROM Receive r WHERE r.toEmail = :toEmail").list(); // 使用HQL查询
            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 查找两个人的邮件的集合
     * */
    public List<Receive> UserAllEmail(String userEmail1,String userEmail2) {

        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String hqlString="SELECT r FROM Receive r WHERE r.toEmail = :toEmail AND r.isInDeleteBox = :value  AND r.fromEmail = :fromEmail";
            Query query=session.createQuery(hqlString);
            query.setString("toEmail", userEmail1);
            query.setString("fromEmail", userEmail2);
            query.setInteger("value", 0);
            List<Receive> list=query.list();
//            // 方式一：使用HQL语句
//            List<Receive> list = session.createQuery("SELECT r FROM Receive r WHERE r.toEmail = :toEmail").list(); // 使用HQL查询
            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /*
     * 查找所有特定邮件的集合
     * */
    public List<Receive> UserOtherEmail(String userEmail,String key,String value) {

        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String hqlString="SELECT r FROM Receive r WHERE r.toEmail = :toEmail and  r."+key+" = :value";
            System.out.println(hqlString);
            Query query=session.createQuery(hqlString);
            query.setString("toEmail", userEmail);
            query.setInteger("value", Integer.parseInt(value));
            List<Receive> list=query.list();
//            // 方式一：使用HQL语句
//            List<Receive> list = session.createQuery("SELECT r FROM Receive r WHERE r.toEmail = :toEmail").list(); // 使用HQL查询
            tx.commit();
            return list;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * 分页的查询数据列表
     * @param firstResult 从结果列表中的哪个索引开始取数据
     * @param maxResults 最多取多少条数据
     * @return 一页的数据列表
     */
    @SuppressWarnings("unchecked")
    public QueryResult findAll(int firstResult, int maxResults) {
        Session session = HibernateUtils.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            // 查询一页的数据列表
            // 方式一：
            // Query query = session.createQuery("FROM Receive");
            // query.setFirstResult(firstResult);
            // query.setMaxResults(maxResults);
            // List<Receive> list = query.list(); // 使用HQL查询

            // 方式二：方法链
            List<Receive> list = session.createQuery( //
                    "FROM Receive") //
                    .setFirstResult(firstResult) //
                    .setMaxResults(maxResults) //
                    .list();

            // 查询总记录数
            // session.createQuery("SELECT COUNT(*) FROM Receive").list().get(0);
            // Long count = (Long) session.createQuery("SELECT COUNT(*) FROM Receive").uniqueResult();
            Long count = (Long) session.createQuery( //
                    "SELECT COUNT(*) FROM Receive") //
                    .uniqueResult();
            tx.commit();

            // 返回结果
            return new QueryResult(count.intValue(), list);
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}