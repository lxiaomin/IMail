package cn.lxm.iMail.Hibernate.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LXM
 */
@Entity
@Table(name = "receive")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Receive.findAll", query = "SELECT r FROM Receive r"),
    @NamedQuery(name = "Receive.findById", query = "SELECT r FROM Receive r WHERE r.id = :id"),
    @NamedQuery(name = "Receive.findByFromEmail", query = "SELECT r FROM Receive r WHERE r.fromEmail = :fromEmail"),
    @NamedQuery(name = "Receive.findByToEmail", query = "SELECT r FROM Receive r WHERE r.toEmail = :toEmail"),
    @NamedQuery(name = "Receive.findByTitle", query = "SELECT r FROM Receive r WHERE r.title = :title"),
    @NamedQuery(name = "Receive.findByTime", query = "SELECT r FROM Receive r WHERE r.time = :time"),
    @NamedQuery(name = "Receive.findByEmailContent", query = "SELECT r FROM Receive r WHERE r.emailContent = :emailContent"),
    @NamedQuery(name = "Receive.findByAppendix", query = "SELECT r FROM Receive r WHERE r.appendix = :appendix"),
    @NamedQuery(name = "Receive.findByIsInReceiveBox", query = "SELECT r FROM Receive r WHERE r.isInReceiveBox = :isInReceiveBox"),
    @NamedQuery(name = "Receive.findByIsInFlagBox", query = "SELECT r FROM Receive r WHERE r.isInFlagBox = :isInFlagBox"),
    @NamedQuery(name = "Receive.findByIsInChangeBox", query = "SELECT r FROM Receive r WHERE r.isInChangeBox = :isInChangeBox"),
    @NamedQuery(name = "Receive.findByIsInTrashBox", query = "SELECT r FROM Receive r WHERE r.isInTrashBox = :isInTrashBox"),
    @NamedQuery(name = "Receive.findByIsInDeleteBox", query = "SELECT r FROM Receive r WHERE r.isInDeleteBox = :isInDeleteBox")})
public class Receive implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "FromEmail")
    private String fromEmail;
    @Column(name = "ToEmail")
    private String toEmail;
    @Column(name = "Title")
    private String title;
    @Column(name = "Time")
    private String time;
    @Column(name = "EmailContent")
    private String emailContent;
    @Column(name = "Appendix")
    private Short appendix;
    @Column(name = "IsInReceiveBox")
    private Integer isInReceiveBox;
    @Column(name = "IsInFlagBox")
    private Integer isInFlagBox;
    @Column(name = "IsInChangeBox")
    private Integer isInChangeBox;
    @Column(name = "IsInTrashBox")
    private Integer isInTrashBox;
    @Column(name = "IsInDeleteBox")
    private Integer isInDeleteBox;

    public Receive() {
    }

    public Receive(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEmailContent() {
        return emailContent;
    }

    public void setEmailContent(String emailContent) {
        this.emailContent = emailContent;
    }

    public Short getAppendix() {
        return appendix;
    }

    public void setAppendix(Short appendix) {
        this.appendix = appendix;
    }

    public Integer getIsInReceiveBox() {
        return isInReceiveBox;
    }

    public void setIsInReceiveBox(Integer isInReceiveBox) {
        this.isInReceiveBox = isInReceiveBox;
    }

    public Integer getIsInFlagBox() {
        return isInFlagBox;
    }

    public void setIsInFlagBox(Integer isInFlagBox) {
        this.isInFlagBox = isInFlagBox;
    }

    public Integer getIsInChangeBox() {
        return isInChangeBox;
    }

    public void setIsInChangeBox(Integer isInChangeBox) {
        this.isInChangeBox = isInChangeBox;
    }

    public Integer getIsInTrashBox() {
        return isInTrashBox;
    }

    public void setIsInTrashBox(Integer isInTrashBox) {
        this.isInTrashBox = isInTrashBox;
    }

    public Integer getIsInDeleteBox() {
        return isInDeleteBox;
    }

    public void setIsInDeleteBox(Integer isInDeleteBox) {
        this.isInDeleteBox = isInDeleteBox;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Receive)) {
            return false;
        }
        Receive other = (Receive) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Myservlet.Receive[ id=" + id + " ]";
    }

}
