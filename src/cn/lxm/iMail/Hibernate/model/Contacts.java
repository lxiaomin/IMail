package cn.lxm.iMail.Hibernate.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LXM
 */
@Entity
@Table(name = "contacts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contacts.findAll", query = "SELECT c FROM Contacts c"),
    @NamedQuery(name = "Contacts.findById", query = "SELECT c FROM Contacts c WHERE c.id = :id"),
    @NamedQuery(name = "Contacts.findByToEmail", query = "SELECT c FROM Contacts c WHERE c.toEmail = :toEmail"),
    @NamedQuery(name = "Contacts.findByName", query = "SELECT c FROM Contacts c WHERE c.name = :name"),
    @NamedQuery(name = "Contacts.findByCompanies", query = "SELECT c FROM Contacts c WHERE c.companies = :companies"),
    @NamedQuery(name = "Contacts.findByEmailAdreess", query = "SELECT c FROM Contacts c WHERE c.emailAdreess = :emailAdreess"),
    @NamedQuery(name = "Contacts.findByTele", query = "SELECT c FROM Contacts c WHERE c.tele = :tele"),
    @NamedQuery(name = "Contacts.findByAdreess", query = "SELECT c FROM Contacts c WHERE c.adreess = :adreess"),
    @NamedQuery(name = "Contacts.findByRemark", query = "SELECT c FROM Contacts c WHERE c.remark = :remark"),
    @NamedQuery(name = "Contacts.findByContactGroup", query = "SELECT c FROM Contacts c WHERE c.contactGroup = :contactGroup")})
public class Contacts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ToEmail")
    private String toEmail;
    @Column(name = "Name")
    private String name;
    @Column(name = "Companies")
    private String companies;
    @Column(name = "EmailAdreess")
    private String emailAdreess;
    @Column(name = "Tele")
    private String tele;
    @Column(name = "Adreess")
    private String adreess;
    @Column(name = "Remark")
    private String remark;
    @Column(name = "ContactGroup")
    private Integer contactGroup;

    public Contacts() {
    }

    public Contacts(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanies() {
        return companies;
    }

    public void setCompanies(String companies) {
        this.companies = companies;
    }

    public String getEmailAdreess() {
        return emailAdreess;
    }

    public void setEmailAdreess(String emailAdreess) {
        this.emailAdreess = emailAdreess;
    }

    public String getTele() {
        return tele;
    }

    public void setTele(String tele) {
        this.tele = tele;
    }

    public String getAdreess() {
        return adreess;
    }

    public void setAdreess(String adreess) {
        this.adreess = adreess;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getContactGroup() {
        return contactGroup;
    }

    public void setContactGroup(Integer contactGroup) {
        this.contactGroup = contactGroup;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contacts)) {
            return false;
        }
        Contacts other = (Contacts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Myservlet.Contacts[ id=" + id + " ]";
    }

}
