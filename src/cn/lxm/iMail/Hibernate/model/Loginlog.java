package cn.lxm.iMail.Hibernate.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LXM
 */
@Entity
@Table(name = "loginlog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Loginlog.findAll", query = "SELECT l FROM Loginlog l"),
    @NamedQuery(name = "Loginlog.findById", query = "SELECT l FROM Loginlog l WHERE l.id = :id"),
    @NamedQuery(name = "Loginlog.findByToEmail", query = "SELECT l FROM Loginlog l WHERE l.toEmail = :toEmail"),
    @NamedQuery(name = "Loginlog.findByLoginTime", query = "SELECT l FROM Loginlog l WHERE l.loginTime = :loginTime")})
public class Loginlog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)

    @Column(name = "ID")
    private Integer id;

    @Column(name = "ToEmail")
    private String toEmail;

    @Column(name = "LoginTime")
    private String loginTime;

    public Loginlog() {
    }

    public Loginlog(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Loginlog)) {
            return false;
        }
        Loginlog other = (Loginlog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Myservlet.Loginlog[ id=" + id + " ]";
    }

}