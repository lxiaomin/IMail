package cn.lxm.iMail.Hibernate.model;


import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LXM
 */
@Entity
@Table(name = "send")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Send.findAll", query = "SELECT s FROM Send s"),
    @NamedQuery(name = "Send.findById", query = "SELECT s FROM Send s WHERE s.id = :id"),
    @NamedQuery(name = "Send.findBySenderEmail", query = "SELECT s FROM Send s WHERE s.senderEmail = :senderEmail"),
    @NamedQuery(name = "Send.findByReciverEmail", query = "SELECT s FROM Send s WHERE s.reciverEmail = :reciverEmail"),
    @NamedQuery(name = "Send.findByTitle", query = "SELECT s FROM Send s WHERE s.title = :title"),
    @NamedQuery(name = "Send.findByEmailContent", query = "SELECT s FROM Send s WHERE s.emailContent = :emailContent"),
    @NamedQuery(name = "Send.findByAppendix", query = "SELECT s FROM Send s WHERE s.appendix = :appendix"),
    @NamedQuery(name = "Send.findBySendTime", query = "SELECT s FROM Send s WHERE s.sendTime = :sendTime"),
    @NamedQuery(name = "Send.findByIsInSendBox", query = "SELECT s FROM Send s WHERE s.isInSendBox = :isInSendBox"),
    @NamedQuery(name = "Send.findByIsInAreadySendBox", query = "SELECT s FROM Send s WHERE s.isInAreadySendBox = :isInAreadySendBox")})
public class Send implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)

    @Column(name = "ID")
    private Integer id;

    @Column(name = "SenderEmail")
    private String senderEmail;

    @Column(name = "ReciverEmail")
    private String reciverEmail;

    @Column(name = "Title")
    private String title;

    @Column(name = "EmailContent")
    private String emailContent;
    @Column(name = "Appendix")
    private Short appendix;

    @Column(name = "SendTime")
    private String sendTime;
    @Column(name = "IsInSendBox")
    private Integer isInSendBox;
    @Column(name = "IsInAreadySendBox")
    private Integer isInAreadySendBox;

    public Send() {
    }

    public Send(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getReciverEmail() {
        return reciverEmail;
    }

    public void setReciverEmail(String reciverEmail) {
        this.reciverEmail = reciverEmail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmailContent() {
        return emailContent;
    }

    public void setEmailContent(String emailContent) {
        this.emailContent = emailContent;
    }

    public Short getAppendix() {
        return appendix;
    }

    public void setAppendix(Short appendix) {
        this.appendix = appendix;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public Integer getIsInSendBox() {
        return isInSendBox;
    }

    public void setIsInSendBox(Integer isInSendBox) {
        this.isInSendBox = isInSendBox;
    }

    public Integer getIsInAreadySendBox() {
        return isInAreadySendBox;
    }

    public void setIsInAreadySendBox(Integer isInAreadySendBox) {
        this.isInAreadySendBox = isInAreadySendBox;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Send)) {
            return false;
        }
        Send other = (Send) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Myservlet.Send[ id=" + id + " ]";
    }

}