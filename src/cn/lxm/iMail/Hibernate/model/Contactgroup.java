package cn.lxm.iMail.Hibernate.model;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlRootElement;
@Entity
@Table(name = "contactgroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contactgroup.findAll", query = "SELECT c FROM Contactgroup c"),
    @NamedQuery(name = "Contactgroup.findById", query = "SELECT c FROM Contactgroup c WHERE c.id = :id"),
    @NamedQuery(name = "Contactgroup.findByToEmail", query = "SELECT c FROM Contactgroup c WHERE c.toEmail = :toEmail"),
    @NamedQuery(name = "Contactgroup.findByGroupName", query = "SELECT c FROM Contactgroup c WHERE c.groupName = :groupName")})
public class Contactgroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ToEmail")
    private String toEmail;
    @Column(name = "GroupName")
    private String groupName;

    public Contactgroup() {
    }

    public Contactgroup(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contactgroup)) {
            return false;
        }
        Contactgroup other = (Contactgroup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Myservlet.Contactgroup[ id=" + id + " ]";
    }

}