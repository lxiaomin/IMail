package cn.lxm.iMail.UI;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.lxm.iMail.Logic.WriteEmailUIEvents;
import cn.lxm.iMail.Email.AccountManage;
import cn.lxm.iMail.Email.Email;
import cn.lxm.iMail.Email.EmailUser;
import cn.lxm.iMail.Email.SmtpSendMails;
import cn.lxm.iMail.Hibernate.DAO.ContactsDAO;
import cn.lxm.iMail.Hibernate.DAO.ReceiveDAO;
import cn.lxm.iMail.Hibernate.DAO.SendDAO;
import cn.lxm.iMail.Hibernate.model.Receive;
import cn.lxm.iMail.Hibernate.model.Send;
import cn.lxm.iMail.Logic.CommitWebBroswer;
import cn.lxm.iMail.Logic.UIEvents;
import cn.lxm.iMail.Util.UIMethods;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import netscape.javascript.JSObject;

public class WriteEmail extends Application {

	// 声明变量
	public static Pane root;// 主面板
	private double xOffset = 0;
	private double yOffset = 0; // 位移
	private static WriteEmailUIEvents uiEvent = new WriteEmailUIEvents();
	AccountManage ac = new AccountManage();
	public static Email email = new Email();
	public static int FileCount = 0;// 文件的数量
	public static ArrayList<String> fileName = new ArrayList<>();

	static String editingName = "";
	static ContactsDAO contactDAO = new ContactsDAO();
	public static java.util.List<cn.lxm.iMail.Hibernate.model.Contacts> Contacts = new ArrayList<>();
	public static int selectItemNum = 0;
	public String changeTextBoxID = "";

	@FXML
	private Pane pane_oneuserHbox0;

	@FXML
	private Pane pane_contactMenu;

	@FXML
	private HBox pane_oneuserHbox2;

	@FXML
	private Pane pane_main;

	@FXML
	private HBox pane_oneuserHbox1;

	@FXML
	private TextField textbox_search;

	@FXML
	private Button btn_big;

	@FXML
	private WebView web;

	@FXML
	private ListView<?> list_showRightContact;

	@FXML
	private VBox pane_oneuser_list;

	@FXML
	private Button btn_send;

	@FXML
	private ImageView img_dropdown;

	@FXML
	private FlowPane pane_userMenu;

	@FXML
	private Button btn_menu2;

	@FXML
	private ImageView img_searchBox;

	@FXML
	private Button btn_addNewAccount;

	@FXML
	private HBox pane_addFile;

	@FXML
	private Button btn_small;

	@FXML
	private TextField textbox_theme;

	@FXML
	private ImageView img_menu2;

	@FXML
	private Button btn_replyTome;

	@FXML
	private ImageView img_menu1;

	@FXML
	private FlowPane flowPane_userFun;

	@FXML
	private TextField textbox_people;

	@FXML
	private Pane pane_contacts;

	@FXML
	private Button btn_menu0;

	@FXML
	private Button btn_menu1;

	@FXML
	private ImageView img_menu71;

	@FXML
	private Button btn_close;
	public static WebEngine webEngine;
	public static String reSendEmailContent = "";// 打算要重新写入Tinymce编辑器的内容，由于在没有加载网页前不能写进去，所以利用静态变量方法传值进去执行方法

	@FXML
	void send(ActionEvent event) {

		String to[] = textbox_people.getText().split(";");
		String subject = textbox_theme.getText();
		String script = "GetTinyMceContent()";
		webEngine.executeScript(script);// 执行脚本
		String content = email.getContent();
		SmtpSendMails ssmMails = new SmtpSendMails();
		File myfiles[] = new File[fileName.size()];
		for (int i = 0; i < fileName.size(); i++)
			myfiles[i] = new File(fileName.get(i));

		// SendDAO sendDAO = new SendDAO();
		// Send ISendEmail = new Send();
		// ISendEmail.setReciverEmail(((TextField)
		// (WriteEmail.root.lookup("#textbox_people"))).getText());
		// ISendEmail.setEmailContent(WriteEmail.email.getContent());
		// ISendEmail.setTitle(((TextField)
		// (WriteEmail.root.lookup("#textbox_theme"))).getText());
		// ISendEmail.setIsInSendBox(0);
		// ISendEmail.setIsInAreadySendBox(1);
		// ISendEmail.setSenderEmail(Login.eu.getUserName());
		// ISendEmail.setAppendix((short) 0);
		// ISendEmail.setSendTime((new Date().toLocaleString()));
		// sendDAO.save(ISendEmail);

		// 启动正在发送的
		// Platform.runLater(new Runnable() {
		// public void run() {
		// try {
		// new WriteEmailTipsBox2("").start(new Stage());
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// }
		//
		// });

		// EmailUser eu = new EmailUser();
		// eu.setUserName("13557330070@139.com");
		// eu.setUserPsw("81440223");
		// eu.setHost("pop.139.com");
		try {
			ssmMails.sendMail(Login.eu, to, subject, content, myfiles, true, false);
			// 启动正在发送的
			Platform.runLater(new Runnable() {
				public void run() {
					try {
						new WriteEmailTipsBox2("已发送").start(new Stage());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			});
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	public WriteEmail() {

	}

	public WriteEmail(String content) {

	}

	public static void main(String[] args) {
		launch(args);

	}

	@FXML
	private void initialize() {
		// pane_all.setStyle("-fx-background:transparent;");

		btn_replyTome.setGraphic(new ImageView("cn/lxm/iMail/img/contact0.png"));
		btn_small.setGraphic(new ImageView("cn/lxm/iMail/img/white_min_0.png"));
		btn_big.setGraphic(new ImageView("cn/lxm/iMail/img/white_max_0.png"));
		btn_close.setGraphic(new ImageView("cn/lxm/iMail/img/white_close_0.png"));

		// 右上角按钮的效果事件
		btn_close.setOnMouseEntered(right_top_Img_move);
		btn_big.setOnMouseEntered(right_top_Img_move);
		btn_small.setOnMouseEntered(right_top_Img_move);
		btn_replyTome.setOnMouseEntered(right_top_Img_move);

		btn_close.setOnMousePressed(right_top_Img_press);
		btn_big.setOnMousePressed(right_top_Img_press);
		btn_small.setOnMousePressed(right_top_Img_press);
		btn_replyTome.setOnMousePressed(right_top_Img_press);

		btn_close.setOnMouseExited(right_top_Img_exit);
		btn_big.setOnMouseExited(right_top_Img_exit);
		btn_small.setOnMouseExited(right_top_Img_exit);
		btn_replyTome.setOnMouseExited(right_top_Img_exit);

		btn_close.setOnMouseReleased(right_top_Img_relesed);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);

		btn_close.setOnMouseClicked(right_top_Img_click_close);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseClicked(right_top_Img_click_showContact);

		// 菜单事件
		pane_oneuserHbox1.setOnMouseEntered(menu_move);
		pane_oneuserHbox2.setOnMouseEntered(menu_move);

		pane_oneuserHbox1.setOnMouseExited(menu_exit);
		pane_oneuserHbox2.setOnMouseExited(menu_exit);



		btn_menu0.setOnMouseClicked(showList_click);

		// 搜索栏文本框事件
		textbox_search.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

				if (textbox_search.isFocused()) {
					textbox_search.setText("");
					textbox_search.setStyle("-fx-text-fill:gray;-fx-text-box-border:none;-fx-focus-color:none;");
					Image image = new Image(getClass().getResource("../img/search_barnew2.png").toExternalForm());
					img_searchBox.setImage(image);

				} else if (textbox_search.isFocusTraversable()) {
					textbox_search.setText("查找联系人");
					textbox_search.setStyle("-fx-text-fill:#aaa;-fx-text-box-border:none;-fx-focus-color:none;");
					// DropShadow dShadow=new DropShadow();
					// dShadow.setColor(value);
					Image image = new Image(getClass().getResource("../img/search_barnew.png").toExternalForm());
					// img_searchBox.setEffect(dShadow);
					img_searchBox.setImage(image);

				}

			}
		});

		textbox_people.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

				if (textbox_people.isFocused()) {
				} else if (textbox_people.isFocusTraversable()) {
					if ((!textbox_people.getText().trim().equals(""))) {

						String emails[] = textbox_people.getText().split(";");
						new Thread() {
							public void run() {
								int i = 0;
								for (i = 0; i < emails.length; i++) {
									if (!ac.IsValid(emails[i])) {
										// 启动主界面
										int k = i;
										Platform.runLater(new Runnable() {
											public void run() {
												try {
													new WriteEmailTipsBox("您输入的邮箱 " + emails[k] + " 格式不正确，请核对后重新输入")
															.start(new Stage());
												} catch (Exception e) {
													e.printStackTrace();
												}
											}

										});
										// textbox_people.requestFocus();
										break;

									} else {
										if ((!ac.accountIsExit(emails[i]))) {
											int k = i;
											textbox_people.requestFocus();
											// 启动主界面
											Platform.runLater(new Runnable() {
												public void run() {
													try {
														new WriteEmailTipsBox("您输入的邮箱" + emails[k] + "不存在，请核对后重新输入")
																.start(new Stage());
													} catch (Exception e) {
														e.printStackTrace();
													}
												}

											});
											break;

										} else {

											;

										}
									}
								}
							}
						}.start();

					}

				}

			}

		});

		// 载入浏览器
		webEngine = web.getEngine();
		webEngine.load(new File("tinymce_4.5.5/tinymce/js/tinymce/editor.html").toURI().toString());
		// File f = new File("src/cn/lxm/iMail/UI/viewer.html");
		// String a = file2String(f, "GBK");
		// System.out.println(a);
		// 与浏览器实现交互功能
		JSObject win = (JSObject) webEngine.executeScript("window");
		win.setMember("app", new CommitWebBroswer());

	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(Index.class.getResource("WriteEmail.fxml"));
			root = loader.load();
			Pane box = new Pane();
			box.getChildren().add(root);
			box.setStyle("-fx-background:transparent;");
			Scene scene = new Scene(box, 920, 728);
			scene.setFill(null);// 设置Sence透明
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			// stage.setTitle("");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../img/login_logo.png")));
			// 鼠标拖拽位移函数
			UIMethods.paneDragMove(root, stage);
			stage.show();
			// 载入联系人
			LoadContacts("13557330070@139.com");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 右上角4个按钮的4个事件
	EventHandler<MouseEvent> right_top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			WriteEmailUIEvents.right_top_mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> right_top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			WriteEmailUIEvents.right_top_mouseEvent_press(node);
		}

	};
	public static EventHandler<MouseEvent> right_top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			WriteEmailUIEvents.right_top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			WriteEmailUIEvents.right_top_mouseEvent_relesed(node);
		}

	};

	// 关闭符号，用于绑定的事件是是否保存到草稿箱。如果是，则保存到草稿箱，如果取消则不保存
	EventHandler<MouseEvent> right_top_Img_click_close = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Platform.runLater(new Runnable() {
				public void run() {
					try {
						new WriteEmailTipsBox3("是否保存到草稿箱呢？").start(new Stage());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			});

		}

	};
	EventHandler<MouseEvent> right_top_Img_click_showContact = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			WriteEmailUIEvents.right_top_Img_click_showContact(node);
		}

	};

	// 搜索栏3个事件
	public static EventHandler<MouseEvent> menu_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			WriteEmailUIEvents.menu_btn_move(node);
		}

	};

	public static EventHandler<MouseEvent> menu_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			WriteEmailUIEvents.menu_btn_exit(node);
		}

	};

	public static EventHandler<MouseEvent> menu_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			WriteEmailUIEvents.menu_btn_click((Node) event.getSource());

		}

	};
	public static EventHandler<MouseEvent> showList_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			WriteEmailUIEvents.showList_click((Node) event.getSource());

		}

	};

	private void LoadContacts(String userName) {

		Contacts = contactDAO.findUserContact(userName);
		try {
			((Pane) root.lookup("#pane_oneuser_list")).getChildren()
					.removeAll(((Pane) root.lookup("#pane_oneuser_list")).getChildren());
		} catch (Exception e) {
			System.out.println("越界");
		}

		for (int i = 0; i < Contacts.size(); i++) {
			HBox hbox_out = new HBox();
			hbox_out.setId("pane_oneuserHbox" + i);
			hbox_out.setPrefHeight(32);
			hbox_out.setPrefWidth(231);
			hbox_out.setAlignment(Pos.TOP_CENTER);
			// hbox_out.setOnMouseClicked(menu_click);

			Button Onecontact = new Button();
			Onecontact.setContentDisplay(ContentDisplay.RIGHT);
			Onecontact.setText(Contacts.get(i).getName() + "              ");
			Onecontact.setStyle("-fx-background-color:#fafbfc;-fx-text-alignment:left");
			Onecontact.setPrefHeight(32);
			Onecontact.setPrefWidth(231);
			Onecontact.setLayoutX(0);
			ImageView imageView = new ImageView(new Image("cn/lxm/iMail/img/add0.png"));
			imageView.setFitHeight(17);
			imageView.setFitWidth(17);
			imageView.setCursor(Cursor.HAND);
			imageView.setId("add" + i);
		    imageView.setOnMouseClicked(addContent);
			Onecontact.setGraphic(imageView);
			Onecontact.setId("btn_menu" + i);
			Onecontact.setOnMouseClicked(menu_click);
			Onecontact.setCursor(Cursor.HAND);

			hbox_out.getChildren().addAll(Onecontact);
			((VBox) root.lookup("#pane_oneuser_list")).getChildren().add(hbox_out);

		}
	}

	public static EventHandler<MouseEvent> addContent = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
;//暂时用不上这个函数了

		}

	};

}
