package cn.lxm.iMail.UI;

import java.io.File;
import java.util.Set;

import cn.lxm.iMail.Email.ConnectWithPop3;
import cn.lxm.iMail.Email.EmailUser;
import cn.lxm.iMail.Logic.CommitWebBroswer;
import cn.lxm.iMail.Logic.UIEvents;
import cn.lxm.iMail.Util.UIMethods;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import netscape.javascript.JSObject;

public class Index extends Application {

	// 声明变量
	public static Pane root;// 主面板
	private double xOffset = 0;
	private double yOffset = 0; // 位移

	@FXML
	private Pane pane_contactMenu;

	@FXML
	private WebView web_webview;

	@FXML
	private Label lab_content;

	@FXML
	private Button btn_reSend;

	@FXML
	private Button btn_message;

	@FXML
	private Pane pane_oneuser_list;

	@FXML
	private Button btn_search;

	@FXML
	private FlowPane pane_userMenu;

	@FXML
	private ImageView img_searchBox;

	@FXML
	private Button btn_small;

	@FXML
	private Pane pane_noemails;

	@FXML
	private Button btn_friends;

	@FXML
	private ImageView img_flag2;

	@FXML
	private Label lab_Sender;

	@FXML
	private Button btn_settings;

	@FXML
	private Button btn_close;

	@FXML
	private Pane pane_oneuserHbox0;

	@FXML
	private Pane pane_top_bar;

	@FXML
	private HBox pane_oneuserHbox4;

	@FXML
	private HBox pane_oneuserHbox3;

	@FXML
	private HBox pane_oneuserHbox2;

	@FXML
	private Pane pane_main;

	@FXML
	private HBox pane_oneuserHbox1;

	@FXML
	private Label lab_Title;

	@FXML
	private HBox pane_oneuserHbox7;

	@FXML
	private HBox pane_oneuserHbox6;

	@FXML
	private HBox pane_oneuserHbox5;

	@FXML
	private TextField textbox_search;

	@FXML
	private Button btn_big;

	@FXML
	private Label lab_time;

	@FXML
	private Button btn_delete;

	@FXML
	private Pane pane_webview;

	@FXML
	private Button btn_writeEmail;

	@FXML
	private ImageView img_dropdown;

	@FXML
	private Button btn_menu6;

	@FXML
	private Button btn_menu7;

	@FXML
	private Pane pane_messagesPreview;

	@FXML
	private ImageView img_menu7;

	@FXML
	private Button btn_writeEmail1;

	@FXML
	private Button btn_menu2;

	@FXML
	private ImageView img_menu6;

	@FXML
	private Button btn_menu3;

	@FXML
	private ImageView img_menu5;

	@FXML
	private Button btn_menu4;

	@FXML
	private ImageView img_menu4;

	@FXML
	private Button btn_addNewAccount;

	@FXML
	private Button btn_writeEmail2;

	@FXML
	private ImageView img_menu3;

	@FXML
	private Button btn_menu5;

	@FXML
	private ImageView img_menu2;

	@FXML
	private Button btn_replyTome;

	@FXML
	private ImageView img_menu1;

	@FXML
	private Pane pane_left_bar;

	@FXML
	private FlowPane flowPane_userFun;

	@FXML
	private Button btn_more;

	@FXML
	private ImageView img_flag;

	@FXML
	private ScrollPane pane_messageBox;

	@FXML
	private Button btn_menu0;

	@FXML
	private Button btn_menu1;

	@FXML
	private ImageView img_menu71;

	@FXML
	private Button btn_move;

	private static UIEvents uiEvent = new UIEvents();

	public Index() {

	}

	public Index(String titleString, String messageString) {

	}

	public static void main(String[] args) {
		System.out.println(new File("tinymce_4.5.5/tinymce/js/tinymce/viewer.html").toURI().toString());
		launch(args);

	}

	@FXML
	private void initialize() {
		// pane_all.setStyle("-fx-background:transparent;");
		btn_writeEmail.setGraphic(new ImageView("cn/lxm/iMail/img/write.png"));
		btn_writeEmail1.setGraphic(new ImageView("cn/lxm/iMail/img/refresh0.png"));
		btn_writeEmail2.setGraphic(new ImageView("cn/lxm/iMail/img/reply.png"));
		btn_reSend.setGraphic(new ImageView("cn/lxm/iMail/img/forward.png"));
		btn_delete.setGraphic(new ImageView("cn/lxm/iMail/img/delete.png"));
		btn_move.setGraphic(new ImageView("cn/lxm/iMail/img/move_mail.png"));
		btn_replyTome.setGraphic(new ImageView("cn/lxm/iMail/img/white_feedback_0.png"));
		btn_small.setGraphic(new ImageView("cn/lxm/iMail/img/white_min_0.png"));
		btn_big.setGraphic(new ImageView("cn/lxm/iMail/img/white_max_0.png"));
		btn_close.setGraphic(new ImageView("cn/lxm/iMail/img/white_close_0.png"));
		btn_more.setGraphic(new ImageView("cn/lxm/iMail/img/more.png"));

		// 左边工具栏增加四个按钮的显示效果
		//btn_message.setOnMouseEntered(left_Img_move);
		btn_friends.setOnMouseEntered(left_Img_move);
		btn_search.setOnMouseEntered(left_Img_move);
		btn_settings.setOnMouseEntered(left_Img_move);

		// btn_message.setOnMouseClicked(left_Img_press);
		btn_friends.setOnMouseClicked(left_Img_press);
		btn_search.setOnMouseClicked(left_Img_press);
		btn_settings.setOnMouseClicked(left_Img_press);

		// btn_message.setOnMouseExited(left_Img_exit);
		btn_friends.setOnMouseExited(left_Img_exit);
		btn_search.setOnMouseExited(left_Img_exit);
		btn_settings.setOnMouseExited(left_Img_exit);

		// 上边的6个按钮的显示效果,增加一个鼠标释放效果
		btn_writeEmail.setOnMouseEntered(top_Img_move);
		btn_writeEmail1.setOnMouseEntered(top_Img_move);
		btn_writeEmail2.setOnMouseEntered(top_Img_move);
		btn_reSend.setOnMouseEntered(top_Img_move);
		btn_delete.setOnMouseEntered(top_Img_move);
		btn_move.setOnMouseEntered(top_Img_move);
		btn_more.setOnMouseEntered(top_Img_move);

		btn_writeEmail.setOnMousePressed(top_Img_press);
		btn_writeEmail1.setOnMousePressed(top_Img_press);
		btn_writeEmail2.setOnMousePressed(top_Img_press);
		btn_reSend.setOnMousePressed(top_Img_press);
		btn_delete.setOnMousePressed(top_Img_press);
		btn_move.setOnMousePressed(top_Img_press);
		btn_more.setOnMousePressed(top_Img_press);

		btn_writeEmail.setOnMouseExited(top_Img_exit);
		btn_writeEmail1.setOnMouseExited(top_Img_exit);
		btn_writeEmail2.setOnMouseExited(top_Img_exit);
		btn_reSend.setOnMouseExited(top_Img_exit);
		btn_delete.setOnMouseExited(top_Img_exit);
		btn_move.setOnMouseExited(top_Img_exit);
		btn_more.setOnMouseExited(top_Img_exit);

		btn_writeEmail.setOnMouseReleased(top_Img_relesed);
		btn_writeEmail1.setOnMouseReleased(top_Img_relesed);
		btn_writeEmail2.setOnMouseReleased(top_Img_relesed);
		btn_reSend.setOnMouseReleased(top_Img_relesed);
		btn_delete.setOnMouseReleased(top_Img_relesed);
		btn_move.setOnMouseReleased(top_Img_relesed);
		btn_more.setOnMouseReleased(top_Img_relesed);

		// 右上角按钮的效果事件
		btn_close.setOnMouseEntered(right_top_Img_move);
		btn_big.setOnMouseEntered(right_top_Img_move);
		btn_small.setOnMouseEntered(right_top_Img_move);
		btn_replyTome.setOnMouseEntered(right_top_Img_move);

		btn_close.setOnMousePressed(right_top_Img_press);
		btn_big.setOnMousePressed(right_top_Img_press);
		btn_small.setOnMousePressed(right_top_Img_press);
		btn_replyTome.setOnMousePressed(right_top_Img_press);

		btn_close.setOnMouseExited(right_top_Img_exit);
		btn_big.setOnMouseExited(right_top_Img_exit);
		btn_small.setOnMouseExited(right_top_Img_exit);
		btn_replyTome.setOnMouseExited(right_top_Img_exit);

		btn_close.setOnMouseReleased(right_top_Img_relesed);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);

		btn_close.setOnMouseClicked(right_top_Img_click_close);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);

		// 搜索栏文本框事件
		textbox_search.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

				if (textbox_search.isFocused()) {
					textbox_search.setText("");
					textbox_search.setStyle("-fx-text-fill:gray;-fx-text-box-border:none;-fx-focus-color:none;");
					Image image = new Image(getClass().getResource("../img/search_barnew2.png").toExternalForm());
					img_searchBox.setImage(image);

				} else if (textbox_search.isFocusTraversable()) {
					textbox_search.setText("搜索邮件");
					textbox_search.setStyle("-fx-text-fill:#aaa;-fx-text-box-border:none;-fx-focus-color:none;");
					// DropShadow dShadow=new DropShadow();
					// dShadow.setColor(value);
					Image image = new Image(getClass().getResource("../img/search_barnew.png").toExternalForm());
					// img_searchBox.setEffect(dShadow);
					img_searchBox.setImage(image);

				}

			}
		});

		// 菜单事件
		pane_oneuserHbox1.setOnMouseEntered(menu_move);
		pane_oneuserHbox2.setOnMouseEntered(menu_move);
		pane_oneuserHbox3.setOnMouseEntered(menu_move);
		pane_oneuserHbox4.setOnMouseEntered(menu_move);
		pane_oneuserHbox5.setOnMouseEntered(menu_move);
		pane_oneuserHbox6.setOnMouseEntered(menu_move);
		pane_oneuserHbox7.setOnMouseEntered(menu_move);

		pane_oneuserHbox1.setOnMouseExited(menu_exit);
		pane_oneuserHbox2.setOnMouseExited(menu_exit);
		pane_oneuserHbox3.setOnMouseExited(menu_exit);
		pane_oneuserHbox4.setOnMouseExited(menu_exit);
		pane_oneuserHbox5.setOnMouseExited(menu_exit);
		pane_oneuserHbox6.setOnMouseExited(menu_exit);
		pane_oneuserHbox7.setOnMouseExited(menu_exit);

		pane_oneuserHbox1.setOnMouseClicked(menu_click);
		pane_oneuserHbox2.setOnMouseClicked(menu_click);
		pane_oneuserHbox3.setOnMouseClicked(menu_click);
		pane_oneuserHbox4.setOnMouseClicked(menu_click);
		pane_oneuserHbox5.setOnMouseClicked(menu_click);
		pane_oneuserHbox6.setOnMouseClicked(menu_click);
		pane_oneuserHbox7.setOnMouseClicked(menu_click);

		btn_menu1.setOnMouseClicked(menu_click);
		btn_menu2.setOnMouseClicked(menu_click);
		btn_menu3.setOnMouseClicked(menu_click);
		btn_menu4.setOnMouseClicked(menu_click);
		btn_menu5.setOnMouseClicked(menu_click);
		btn_menu6.setOnMouseClicked(menu_click);
		btn_menu7.setOnMouseClicked(menu_click);

		btn_menu0.setOnMouseClicked(showList_click);

		// 载入浏览器
		final WebEngine webEngine = web_webview.getEngine();
		webEngine.load(new File("tinymce_4.5.5/tinymce/js/tinymce/viewer.html").toURI().toString());

		// 设置滚动条需要d时候滚动
		pane_messageBox.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		pane_messageBox.getStyleClass().add("edge-to-edge");
		pane_messageBox.setOnMouseEntered(scroll_show);
		pane_messageBox.setOnMouseExited(scroll_hide);


		//浏览器交互
		JSObject win = (JSObject) webEngine.executeScript("window");
		win.setMember("app", new CommitWebBroswer());

	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(Index.class.getResource("index.fxml"));
			root = loader.load();
			Pane box = new Pane();
			box.getChildren().add(root);
			box.setStyle("-fx-background:transparent;");
			Scene scene = new Scene(box, 1158, 660);
			scene.setFill(null);// 设置Sence透明
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			// stage.setTitle("");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../img/login_logo.png")));
			// 鼠标拖拽位移函数
			UIMethods.paneDragMove(root, stage);
			stage.show();
//			EmailUser eu = new EmailUser();
//			eu.setUserName("13557330070@139.com");
//			eu.setUserPsw("81440223");
//			eu.setEmailCount(115);
//			eu.setHost("pop3.139.com");
//			UIEvents.LoadEmails(eu);
			// 载入邮件
			UIEvents.LoadEmails(Login.eu);
			// 启用线程刷新邮箱
			ReflashBoxInBack(Login.eu);
			// 更新滚动条的颜色样式事件
			UpdateScroll();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

	private void ReflashBoxInBack(EmailUser eu) {

		new Thread() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(60000);
//						Platform.runLater(new Runnable() {
//							public void run() {
								ConnectWithPop3.ReFlashEmailBox(eu.getEmailCount(), eu);
//							}
//
//						});

					} catch (Exception e) {
						break;
					}

				}

			}
		}.start();

	}

	@FXML
	void event_message(ActionEvent event) {

	}

	@FXML
	void event_friends(ActionEvent event) {

	}

	@FXML
	void event_search(ActionEvent event) {

	}

	@FXML
	void event_setting(ActionEvent event) {

	}

	// 左边工具栏的三个事件
	EventHandler<MouseEvent> left_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> left_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.left_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> left_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.left_mouseEvent_exit(node);
		}

	};

	// 上面工具栏的4个事件
	EventHandler<MouseEvent> top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.top_mouseEvent_ImgReplace(node);
		}

	};
	public static EventHandler<MouseEvent> top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.top_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.top_mouseEvent_relesed(node);
		}

	};

	// 右上角4个按钮的4个事件
	EventHandler<MouseEvent> right_top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.right_top_mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> right_top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.right_top_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> right_top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.right_top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.right_top_mouseEvent_relesed(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_click_close = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口
		}

	};

	// 用户菜单3个事件
	public static EventHandler<MouseEvent> menu_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.menu_btn_move(node);
		}

	};

	public static EventHandler<MouseEvent> menu_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			UIEvents.menu_btn_exit(node);
		}

	};

	public static EventHandler<MouseEvent> menu_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			try {
				UIEvents.menu_btn_click((Node) event.getSource());
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	};

	public static EventHandler<MouseEvent> showList_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			UIEvents.showList_click((Node) event.getSource());

		}

	};

	// 滚动条的样式
	private void UpdateScroll() {

		new Thread() {
			public void run() {

				Platform.runLater(new Runnable() {
					public void run() {
						Set<Node> nodes = Index.root.lookup("#pane_messageBox").lookupAll(".scroll-bar");
						for (final Node node : nodes) {
							if (node instanceof ScrollBar) {
								ScrollBar sb = (ScrollBar) node;
								if (sb.getOrientation() == Orientation.VERTICAL) {
									sb.setStyle(
											"-fx-border-color:derive(gray,80%);-fx-background-color: transparent;-fx-border-color:#fafbfc;");
									Node thumb = Index.root.lookup("#pane_messageBox").lookup(".thumb");
									thumb.setStyle("-fx-background-color:#d9eafa;-fx-background-radius: 1em;");
								}
							}
						}
					}
				});

			}

		}.start();

	}

	public static EventHandler<MouseEvent> scroll_show = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Set<Node> nodes = Index.root.lookup("#pane_messageBox").lookupAll(".scroll-bar");
			for (final Node node : nodes) {
				if (node instanceof ScrollBar) {
					ScrollBar sb = (ScrollBar) node;
					if (sb.getOrientation() == Orientation.VERTICAL) {
						sb.setVisible(true);
					}
				}
			}

		}

	};

	public static EventHandler<MouseEvent> scroll_hide = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Set<Node> nodes = Index.root.lookup("#pane_messageBox").lookupAll(".scroll-bar");
			for (final Node node : nodes) {
				if (node instanceof ScrollBar) {
					ScrollBar sb = (ScrollBar) node;
					if (sb.getOrientation() == Orientation.VERTICAL) {
						sb.setVisible(false);
					}
				}
			}
		}

	};

}
