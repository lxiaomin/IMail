package cn.lxm.iMail.UI;


import java.util.ArrayList;
import cn.lxm.iMail.Email.EmailUser;
import cn.lxm.iMail.Hibernate.DAO.ContactsDAO;
import cn.lxm.iMail.Logic.ContactEvents;
import cn.lxm.iMail.Util.UIMethods;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Contact extends Application {

	// 声明变量
	public static Pane root;// 主面板
	static ContactEvents ContactEvents = new ContactEvents();
	static String editingName = "";
	static ContactsDAO contactDAO=new ContactsDAO();
	public static java.util.List<cn.lxm.iMail.Hibernate.model.Contacts> Contacts=new ArrayList<>();
	public static int selectItemNum=0;
	public String changeTextBoxID="";
	/*
	 * 测试用例
	 * */

	EmailUser eUser=new EmailUser();

	@FXML
    private Pane pane_oneuserHbox0;

    @FXML
    private Pane pane_top_bar;

    @FXML
    private Pane pane_contactMenu;

    @FXML
    private HBox pane_1123;

    @FXML
    private MenuButton menu_list;

    @FXML
    private Pane pane_main;

    @FXML
    private HBox pane_111;

    @FXML
    private HBox pane_112;

    @FXML
    private HBox pane_1122;

    @FXML
    private HBox pane_1121;

    @FXML
    private TextField textbox_search;

    @FXML
    private TextField textBox_contactName;

    @FXML
    private Button btn_big;

    @FXML
    private Button btn_message;

    @FXML
    private TextField textBox_company1;

    @FXML
    private Hyperlink link_copy2;

    @FXML
    private TextField textBox_company12;

    @FXML
    private TextField textBox_company11;

    @FXML
    private Button btn_delete;

    @FXML
    private Hyperlink link_copy1;

    @FXML
    private Button btn_writeEmail;

    @FXML
    private VBox pane_oneuser_list;

    @FXML
    private TextArea textarea_mark;

    @FXML
    private Button btn_search;

    @FXML
    private ImageView img_dropdown;

    @FXML
    private FlowPane pane_userMenu;

    @FXML
    private Button btn_writeEmail1;

    @FXML
    private ImageView img_searchBox;

    @FXML
    private Button btn_addNewAccount;

    @FXML
    private Button btn_writeEmail2;

    @FXML
    private Button btn_small;

    @FXML
    private Button btn_replyTome;

    @FXML
    private Pane pane_left_bar;

    @FXML
    private HBox pane_11;

    @FXML
    private FlowPane flowPane_userFun;

    @FXML
    private HBox pane_1;

    @FXML
    private TextField textBox_company;

    @FXML
    private Button btn_friends;

    @FXML
    private Hyperlink link_writeEmail;

    @FXML
    private Button btn_menu_00;

    @FXML
    private ImageView img_menu71;

    @FXML
    private Hyperlink link_copy;

    @FXML
    private Button btn_settings;

    @FXML
    private Button btn_close;

	public Contact() {

	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(Contact.class.getResource("contact.fxml"));
			root = loader.load();
			Pane box = new Pane();
			box.getChildren().add(root);
			box.setStyle("-fx-background:transparent;");
			Scene scene = new Scene(box, 1158, 660);
			scene.setFill(null);// 设置Sence透明
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			// stage.setTitle("");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../img/login_logo.png")));
			// 鼠标拖拽位移函数
			UIMethods.paneDragMove(root, stage);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);

	}

	@FXML
	private void initialize() {

		btn_replyTome.setGraphic(new ImageView("cn/lxm/iMail/img/white_feedback_0.png"));
		btn_small.setGraphic(new ImageView("cn/lxm/iMail/img/white_min_0.png"));
		btn_big.setGraphic(new ImageView("cn/lxm/iMail/img/white_max_0.png"));
		btn_close.setGraphic(new ImageView("cn/lxm/iMail/img/white_close_0.png"));

		// 左边工具栏增加四个按钮的显示效果
		btn_message.setOnMouseEntered(left_Img_move);
		// btn_friends.setOnMouseEntered(left_Img_move);
		btn_search.setOnMouseEntered(left_Img_move);
		btn_settings.setOnMouseEntered(left_Img_move);

		btn_message.setOnMouseClicked(left_Img_press);
		// btn_friends.setOnMouseClicked(left_Img_press);
		btn_search.setOnMouseClicked(left_Img_press);
		btn_settings.setOnMouseClicked(left_Img_press);

		btn_message.setOnMouseExited(left_Img_exit);
		// btn_friends.setOnMouseExited(left_Img_exit);
		btn_search.setOnMouseExited(left_Img_exit);
		btn_settings.setOnMouseExited(left_Img_exit);

		// 上边的6个按钮的显示效果,增加一个鼠标释放效果
		btn_writeEmail.setOnMouseEntered(top_Img_move);
		btn_writeEmail.setOnMousePressed(top_Img_press);
		btn_writeEmail.setOnMouseExited(top_Img_exit);
		btn_writeEmail.setOnMouseClicked(save_contact);


		btn_writeEmail2.setOnMouseEntered(top_Img_move);
		btn_delete.setOnMouseEntered(top_Img_move);
		btn_delete.setOnMouseClicked(delete_contact);

		btn_writeEmail2.setOnMousePressed(top_Img_press);
		btn_delete.setOnMousePressed(top_Img_press);



		btn_writeEmail2.setOnMouseExited(top_Img_exit);
		btn_delete.setOnMouseExited(top_Img_exit);

		btn_writeEmail.setOnMouseReleased(top_Img_relesed);

		btn_writeEmail2.setOnMouseReleased(top_Img_relesed);
		btn_delete.setOnMouseReleased(top_Img_relesed);
		btn_writeEmail2.setOnMouseClicked(look_EachEmails);
		// 右上角按钮的效果事件
		btn_close.setOnMouseEntered(right_top_Img_move);
		btn_big.setOnMouseEntered(right_top_Img_move);
		btn_small.setOnMouseEntered(right_top_Img_move);
		btn_replyTome.setOnMouseEntered(right_top_Img_move);

		btn_close.setOnMousePressed(right_top_Img_press);
		btn_big.setOnMousePressed(right_top_Img_press);
		btn_small.setOnMousePressed(right_top_Img_press);
		btn_replyTome.setOnMousePressed(right_top_Img_press);

		btn_close.setOnMouseExited(right_top_Img_exit);
		btn_big.setOnMouseExited(right_top_Img_exit);
		btn_small.setOnMouseExited(right_top_Img_exit);
		btn_replyTome.setOnMouseExited(right_top_Img_exit);

		btn_close.setOnMouseReleased(right_top_Img_relesed);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);

		btn_close.setOnMouseClicked(right_top_Img_click_close);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);

		// 搜索栏文本框事件
		textbox_search.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

				if (textbox_search.isFocused()) {
					textbox_search.setText("");
					textbox_search.setStyle("-fx-text-fill:gray;-fx-text-box-border:none;-fx-focus-color:none;");
					Image image = new Image(getClass().getResource("../img/search_barnew2.png").toExternalForm());
					img_searchBox.setImage(image);

				} else if (textbox_search.isFocusTraversable()) {
					textbox_search.setText("搜索联系人");
					textbox_search.setStyle("-fx-text-fill:#aaa;-fx-text-box-border:none;-fx-focus-color:none;");
					Image image = new Image(getClass().getResource("../img/search_barnew.png").toExternalForm());
					img_searchBox.setImage(image);

				}

			}
		});

		btn_menu_00.setOnMouseClicked(showList_click);

		// 文本框的初始状态，均为背景灰色，与总背景一体
		textBox_company.setStyle("-fx-background-color: #fafbfc; -fx-focus-traversable: false;");
		// 文本框提示用户操作，背景变为白色
		textBox_contactName.setOnMouseEntered(textboxEntred);
		textBox_company.setOnMouseEntered(textboxEntred);
		textBox_company1.setOnMouseEntered(textboxEntred);
		textBox_company11.setOnMouseEntered(textboxEntred);
		textBox_company12.setOnMouseEntered(textboxEntred);

		textBox_contactName.setOnMouseExited(textboxExit);
		textBox_company.setOnMouseExited(textboxExit);
		textBox_company1.setOnMouseExited(textboxExit);
		textBox_company11.setOnMouseExited(textboxExit);
		textBox_company12.setOnMouseExited(textboxExit);

//		textBox_contactName.setOnMouseClicked(textboxClick);
//		textBox_company.setOnMouseClicked(textboxClick);
//		textBox_company1.setOnMouseClicked(textboxClick);
//		textBox_company11.setOnMouseClicked(textboxClick);
//		textBox_company12.setOnMouseClicked(textboxClick);


		//添加新的联系人
		btn_addNewAccount.setOnMouseClicked(add_contact);

		eUser.setUserName("13557330070@139.com");
		LoadContacts(eUser.getUserName());


	}


	@FXML
	void event_message(ActionEvent event) {

	}

	@FXML
	void event_friends(ActionEvent event) {

	}

	@FXML
	void event_search(ActionEvent event) {

	}

	@FXML
	void event_setting(ActionEvent event) {

	}

	// 左边工具栏的三个事件
	EventHandler<MouseEvent> left_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> left_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.left_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> left_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.left_mouseEvent_exit(node);
		}

	};

	// 上面工具栏的4个事件
	EventHandler<MouseEvent> top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.top_mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.top_mouseEvent_press(node);
			cn.lxm.iMail.Hibernate.model.Contacts newOne=new cn.lxm.iMail.Hibernate.model.Contacts();
			newOne.setId(Contacts.get(selectItemNum).getId());
			newOne.setName((((TextField) (Contact.root.lookup("#textBox_contactName"))).getText()));
			newOne.setCompanies(((TextField) (Contact.root.lookup("#textBox_company"))).getText());
			newOne.setEmailAdreess(((TextField) (Contact.root.lookup("#textBox_company1"))).getText());
			newOne.setTele(((TextField) (Contact.root.lookup("#textBox_company11"))).getText());
			newOne.setAdreess(((TextField) (Contact.root.lookup("#textBox_company12"))).getText());
			newOne.setRemark(((TextArea) (Contact.root.lookup("#textarea_mark"))).getText());
			newOne.setToEmail(eUser.getUserName());
			contactDAO.update(newOne);
			LoadContacts(eUser.getUserName());
		}

	};
	EventHandler<MouseEvent> top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.top_mouseEvent_relesed(node);
		}

	};

	// 右上角4个按钮的4个事件
	EventHandler<MouseEvent> right_top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.right_top_mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> right_top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.right_top_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> right_top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.right_top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.right_top_mouseEvent_relesed(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_click_close = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口
		}

	};

	// 用户菜单3个事件
	public static EventHandler<MouseEvent> menu_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.menu_btn_move(node);
		}

	};

	public static EventHandler<MouseEvent> menu_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			ContactEvents.menu_btn_exit(node);
		}

	};

	public static EventHandler<MouseEvent> menu_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			ContactEvents.menu_btn_click((Node) event.getSource());

		}

	};

	public static EventHandler<MouseEvent> showList_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			ContactEvents.showList_click((Node) event.getSource());

		}

	};
	// 文本框事件3个
	EventHandler<MouseEvent> textboxEntred = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			String name = ((Node) event.getSource()).getId();
			if (!name.equals("textarea_mark")) {
				if (name.equals("textBox_company1")) {
					pane_112.setStyle("-fx-background-color: #f2f3f5;-fx-background-radius:5px;");
					link_copy.setVisible(true);
					link_writeEmail.setVisible(true);
				}
				if (name.equals("textBox_company11")) {
					pane_1121.setStyle("-fx-background-color: #f2f3f5;-fx-background-radius:5px;");
					link_copy1.setVisible(true);
				}
				if (name.equals("textBox_company12")) {
					pane_1122.setStyle("-fx-background-color: #f2f3f5;-fx-background-radius:5px;");
					link_copy2.setVisible(true);
				}
				((TextField) root.lookup("#" + name)).setStyle("-fx-background-color: #fff;");
			}
		}

	};

	EventHandler<MouseEvent> textboxExit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			String name = ((Node) event.getSource()).getId();
			if (!name.equals("textarea_mark")) {
				if (name.equals("textBox_company1")) {
					pane_112.setStyle("-fx-background-color: #fafbfc;");
					link_copy.setVisible(false);
					link_writeEmail.setVisible(false);
				}
				if (name.equals("textBox_company11")) {
					pane_1121.setStyle("-fx-background-color: #fafbfc;");
					link_copy1.setVisible(false);
				}
				if (name.equals("textBox_company12")) {
					pane_1122.setStyle("-fx-background-color: #fafbfc;");
					link_copy2.setVisible(false);
				}
				((TextField) root.lookup("#" + name)).setStyle("-fx-background-color: #fafbfc;");
			}
		}

	};
	EventHandler<MouseEvent> textboxClick = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			String name = ((Node) event.getSource()).getId();
			if (!name.equals("textarea_mark")) {
				((TextField) root.lookup("#" + name)).setOnMouseExited(null);
				editingName = name;
			}
		}

	};

	EventHandler<KeyEvent> textboxSummit = new EventHandler<KeyEvent>() {

		@Override
		public void handle(KeyEvent event) {
			if (event.getCode() == KeyCode.ENTER) {
				// 提交并保存数据
				// 重新恢复它的鼠标移动开效果
				if (editingName.equals("textBox_company1")) {
					((TextField) root.lookup("#" + editingName)).setOnMouseExited(textboxExit);
				}
				if (editingName.equals("textBox_company11")) {
					((TextField) root.lookup("#" + editingName)).setOnMouseExited(textboxExit);
				}
				if (editingName.equals("textBox_company12")) {
					((TextField) root.lookup("#" + editingName)).setOnMouseExited(textboxExit);
				}

			}

		}
	};


	private void LoadContacts(String userName) {

		Contacts=contactDAO.findUserContact(userName);
		try {
			pane_oneuser_list.getChildren().removeAll(pane_oneuser_list.getChildren());
		} catch (Exception e) {
			System.out.println("越界");
		}

		for(int i=0;i<Contacts.size();i++)
		{
			HBox hbox_out=new HBox();
			hbox_out.setId("pane_oneuserHbox"+i);
			hbox_out.setPrefHeight(32);
			hbox_out.setPrefWidth(231);
			hbox_out.setAlignment(Pos.TOP_LEFT);
			hbox_out.setOnMouseClicked(menu_click);

			Button Onecontact=new Button();
			Onecontact.setText(Contacts.get(i).getName()+"              ");
			Onecontact.setStyle("-fx-background-color:#fafbfc;-fx-text-alignment:left");
			Onecontact.setPrefHeight(32);
			Onecontact.setPrefWidth(231);
			Onecontact.setLayoutX(0);
			ImageView imageView=new ImageView(new Image("cn/lxm/iMail/img/icon_gmail.png"));
			imageView.setFitHeight(16);
			imageView.setFitWidth(16);
			Onecontact.setGraphic(imageView);
			Onecontact.setId("btn_menu"+i);
			Onecontact.setOnMouseClicked(menu_click);


			hbox_out.getChildren().addAll(Onecontact);
			pane_oneuser_list.getChildren().add(hbox_out);

		}

	}

	//保存当前联系人
	EventHandler<MouseEvent> save_contact = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			cn.lxm.iMail.Hibernate.model.Contacts newOne=new cn.lxm.iMail.Hibernate.model.Contacts();
			newOne.setId(Contacts.get(selectItemNum).getId());
			newOne.setName((((TextField) (Contact.root.lookup("#textBox_contactName"))).getText()));
			newOne.setCompanies(((TextField) (Contact.root.lookup("#textBox_company"))).getText());
			newOne.setEmailAdreess(((TextField) (Contact.root.lookup("#textBox_company1"))).getText());
			newOne.setTele(((TextField) (Contact.root.lookup("#textBox_company11"))).getText());
			newOne.setAdreess(((TextField) (Contact.root.lookup("#textBox_company12"))).getText());
			newOne.setRemark(((TextArea) (Contact.root.lookup("#textarea_mark"))).getText());
			newOne.setToEmail(eUser.getUserName());
			contactDAO.update(newOne);
			LoadContacts(eUser.getUserName());
		}

	};

	//添加新的联系人
	EventHandler<MouseEvent> add_contact = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			cn.lxm.iMail.Hibernate.model.Contacts newOne=new cn.lxm.iMail.Hibernate.model.Contacts();
			newOne.setName("联系人");
			newOne.setCompanies("");
			newOne.setEmailAdreess("");
			newOne.setTele("");
			newOne.setAdreess("");
			newOne.setRemark("");
			newOne.setToEmail(eUser.getUserName());
			contactDAO.save(newOne);
			LoadContacts(eUser.getUserName());
		}

	};

	//删除当前联系人
	EventHandler<MouseEvent> delete_contact = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			contactDAO.delete(Contacts.get(selectItemNum).getId());
			LoadContacts(eUser.getUserName());
		}

	};

	//查看往来邮件
	EventHandler<MouseEvent> look_EachEmails = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					Platform.runLater(new Runnable() {
						public void run() {
							try {
								new lookEachotherEmails(Contacts.get(selectItemNum).getEmailAdreess(),Login.eu.getUserName(),Contacts.get(selectItemNum).getName()).start(new Stage());

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				}
			});


		}

	};


}
