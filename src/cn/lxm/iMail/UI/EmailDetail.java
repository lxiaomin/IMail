package cn.lxm.iMail.UI;

import java.io.File;

import cn.lxm.iMail.Logic.EmailDetailsEvents;
import cn.lxm.iMail.Util.UIMethods;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class EmailDetail extends Application {

	// 声明变量
	public static Pane root;// 主面板
	public static EmailDetailsEvents EmailDetailsEvents=new EmailDetailsEvents();
	@FXML
	private Button btn_reSend;

	@FXML
	private Button btn_more;

	@FXML
	private Button btn_big;

	@FXML
	private Button btn_writeEmail2;

	@FXML
	private Button btn_small;

	@FXML
	private Button btn_delete;

	@FXML
	private Button btn_replyTome;

	@FXML
	private WebView webview_detail;

	@FXML
	private Button btn_move;

	@FXML
	private Button btn_writeEmail;

	@FXML
	private Button btn_close;

	@FXML
	private void initialize() {
		// pane_all.setStyle("-fx-background:transparent;");
		btn_writeEmail.setGraphic(new ImageView("cn/lxm/iMail/img/write.png"));
		//btn_writeEmail1.setGraphic(new ImageView("cn/lxm/iMail/img/refresh0.png"));
		btn_writeEmail2.setGraphic(new ImageView("cn/lxm/iMail/img/reply.png"));
		btn_reSend.setGraphic(new ImageView("cn/lxm/iMail/img/forward.png"));
		btn_delete.setGraphic(new ImageView("cn/lxm/iMail/img/delete.png"));
		btn_move.setGraphic(new ImageView("cn/lxm/iMail/img/move_mail.png"));
		btn_replyTome.setGraphic(new ImageView("cn/lxm/iMail/img/white_feedback_0.png"));
		btn_small.setGraphic(new ImageView("cn/lxm/iMail/img/white_min_0.png"));
		btn_big.setGraphic(new ImageView("cn/lxm/iMail/img/white_max_0.png"));
		btn_close.setGraphic(new ImageView("cn/lxm/iMail/img/white_close_0.png"));
		btn_more.setGraphic(new ImageView("cn/lxm/iMail/img/more.png"));

		// 上边的6个按钮的显示效果,增加一个鼠标释放效果
		btn_writeEmail.setOnMouseEntered(top_Img_move);
		//btn_writeEmail1.setOnMouseEntered(top_Img_move);
		btn_writeEmail2.setOnMouseEntered(top_Img_move);
		btn_reSend.setOnMouseEntered(top_Img_move);
		btn_delete.setOnMouseEntered(top_Img_move);
		btn_move.setOnMouseEntered(top_Img_move);
		btn_more.setOnMouseEntered(top_Img_move);

		btn_writeEmail.setOnMousePressed(top_Img_press);
	//	btn_writeEmail1.setOnMousePressed(top_Img_press);
		btn_writeEmail2.setOnMousePressed(top_Img_press);
		btn_reSend.setOnMousePressed(top_Img_press);
		btn_delete.setOnMousePressed(top_Img_press);
		btn_move.setOnMousePressed(top_Img_press);
		btn_more.setOnMousePressed(top_Img_press);

		btn_writeEmail.setOnMouseExited(top_Img_exit);
		//btn_writeEmail1.setOnMouseExited(top_Img_exit);
		btn_writeEmail2.setOnMouseExited(top_Img_exit);
		btn_reSend.setOnMouseExited(top_Img_exit);
		btn_delete.setOnMouseExited(top_Img_exit);
		btn_move.setOnMouseExited(top_Img_exit);
		btn_more.setOnMouseExited(top_Img_exit);

		btn_writeEmail.setOnMouseReleased(top_Img_relesed);
		//btn_writeEmail1.setOnMouseReleased(top_Img_relesed);
		btn_writeEmail2.setOnMouseReleased(top_Img_relesed);
		btn_reSend.setOnMouseReleased(top_Img_relesed);
		btn_delete.setOnMouseReleased(top_Img_relesed);
		btn_move.setOnMouseReleased(top_Img_relesed);
		btn_more.setOnMouseReleased(top_Img_relesed);

		// 右上角按钮的效果事件
		btn_close.setOnMouseEntered(right_top_Img_move);
		btn_big.setOnMouseEntered(right_top_Img_move);
		btn_small.setOnMouseEntered(right_top_Img_move);
		btn_replyTome.setOnMouseEntered(right_top_Img_move);

		btn_close.setOnMousePressed(right_top_Img_press);
		btn_big.setOnMousePressed(right_top_Img_press);
		btn_small.setOnMousePressed(right_top_Img_press);
		btn_replyTome.setOnMousePressed(right_top_Img_press);

		btn_close.setOnMouseExited(right_top_Img_exit);
		btn_big.setOnMouseExited(right_top_Img_exit);
		btn_small.setOnMouseExited(right_top_Img_exit);
		btn_replyTome.setOnMouseExited(right_top_Img_exit);

		btn_close.setOnMouseReleased(right_top_Img_relesed);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);

		btn_close.setOnMouseClicked(right_top_Img_click_close);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);

		// 载入浏览器
		final WebEngine webEngine = webview_detail.getEngine();
		webEngine.load(new File("tinymce_4.5.5/tinymce/js/tinymce/viewer.html").toURI().toString());

	}



	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(EmailDetail.class.getResource("EmailDetail.fxml"));
			root = loader.load();
			Pane box = new Pane();
			box.getChildren().add(root);
			box.setStyle("-fx-background:transparent;");
			Scene scene = new Scene(box, 980, 737);
			scene.setFill(null);// 设置Sence透明
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			// stage.setTitle("");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../img/login_logo.png")));
			// 鼠标拖拽位移函数
			UIMethods.paneDragMove(root, stage);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 左边工具栏的三个事件
	EventHandler<MouseEvent> left_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> left_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.left_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> left_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.left_mouseEvent_exit(node);
		}

	};

	// 上面工具栏的4个事件
	EventHandler<MouseEvent> top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.top_mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.top_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.top_mouseEvent_relesed(node);
		}

	};

	// 右上角4个按钮的4个事件
	EventHandler<MouseEvent> right_top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.right_top_mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> right_top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.right_top_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> right_top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.right_top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.right_top_mouseEvent_relesed(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_click_close = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口
		}

	};




	// 用户菜单3个事件
	public static EventHandler<MouseEvent> menu_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.menu_btn_move(node);
		}

	};

	public static EventHandler<MouseEvent> menu_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			EmailDetailsEvents.menu_btn_exit(node);
		}

	};

	public static EventHandler<MouseEvent> menu_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			EmailDetailsEvents.menu_btn_click((Node) event.getSource());

		}

	};

	public static EventHandler<MouseEvent> showList_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			EmailDetailsEvents.showList_click((Node) event.getSource());

		}

	};

	public static void main(String[] args) {
		launch(args);

	}
}
