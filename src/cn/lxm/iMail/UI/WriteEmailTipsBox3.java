package cn.lxm.iMail.UI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.sun.xml.internal.ws.org.objectweb.asm.Label;

import cn.lxm.iMail.Email.EmailUser;
import cn.lxm.iMail.Email.SmtpSendMails;
import cn.lxm.iMail.Hibernate.DAO.ReceiveDAO;
import cn.lxm.iMail.Hibernate.DAO.SendDAO;
import cn.lxm.iMail.Hibernate.model.Receive;
import cn.lxm.iMail.Hibernate.model.Send;
import cn.lxm.iMail.Util.Log;
import cn.lxm.iMail.Util.MimeTypeFactory;
import cn.lxm.iMail.Util.ReadFile;
import cn.lxm.iMail.Util.UIMethods;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class WriteEmailTipsBox3 extends Application {

	// 声明变量
	public static Pane root;// 主面板
	private String mytips = "";
	private final static String BOUNDARY = "--=_PartSpilt_lxm_1014_";// MIME分格符,分割各个MIME消息
	private static boolean NEEDBoundary = false;// MIME分格符,分割各个MIME消息
	private final static String CHARSET = "GB2312";// 邮件发送的编码
	Log log = new Log();
	static ReceiveDAO rd = new ReceiveDAO();
	@FXML
	private Button btn_save2;

	@FXML
	private Button btn_cancel;

	@FXML
	private ImageView btn_close1;

	@FXML
	private Text lab;

	@FXML
	void event_save(ActionEvent event) {

		// SendDAO sendDAO=new SendDAO();
		// Send ISendEmail=new Send();
		// ISendEmail.setReciverEmail(((TextField)(WriteEmail.root.lookup("#textbox_people"))).getText());
		// ISendEmail.setEmailContent(WriteEmail.email.getContent());
		// ISendEmail.setTitle(((TextField)(WriteEmail.root.lookup("#textbox_theme"))).getText());
		// ISendEmail.setIsInSendBox(0);
		// ISendEmail.setIsInAreadySendBox(0);
		// ISendEmail.setSenderEmail(Login.eu.getUserName());
		// ISendEmail.setAppendix((short)1);//假装是草稿箱//不用假装了，抛弃这个用法。直接调用发送邮件的程序。将数据以eml的形式存在-3.23
		// ISendEmail.setSendTime((new Date().toLocaleString()));
		// sendDAO.save(ISendEmail);

		// 直接调用发送邮件的程序。将数据以eml的形式存在-3.23
		Receive receive = new Receive();
		String to[] = ((TextField) (WriteEmail.root.lookup("#textbox_people"))).getText().split(";");
		String subject = ((TextField) (WriteEmail.root.lookup("#textbox_theme"))).getText();
		String script = "GetTinyMceContent()";
		WriteEmail.webEngine.executeScript(script);// 执行脚本
		String content = WriteEmail.email.getContent();// 获取内容
		File myfiles[] = new File[WriteEmail.fileName.size()];
		for (int i = 0; i < WriteEmail.fileName.size(); i++)
			myfiles[i] = new File(WriteEmail.fileName.get(i));
		try {
			String path = TrySendEEmail(Login.eu, to, subject, content, myfiles, true, false);
			receive.setEmailContent(path);
			receive.setAppendix((short) 0);
			receive.setToEmail(Login.eu.getUserName());
			receive.setIsInChangeBox(1);
			receive.setIsInFlagBox(0);
			receive.setIsInReceiveBox(0);
			receive.setIsInTrashBox(0);
			receive.setIsInDeleteBox(0);
			receive.setTime("");
			rd.save(receive);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	private String TrySendEEmail(EmailUser eu, String[] to, String subject, String content, File[] attachments,
			boolean isHtml, boolean isUrgent) throws IllegalArgumentException, IOException, InterruptedException {
		String path = "";
		if (attachments.length > 0)
			NEEDBoundary = true;
		StringBuilder sb = new StringBuilder();
		sb.append("From: " + eu.getUserName() + "\r\n");
		sb.append("To: " + to[0] + "\r\n");
		sb.append("Subject: " + subject + "\r\n");
		sb.append("Date: " + new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z (z)", Locale.CHINA).format(new Date())
				+ "\r\n");
		sb.append("DMIME-Version: 1.0" + "\r\n");
		if (NEEDBoundary) {
			sb.append("Content-Type: multipart/mixed; BOUNDARY=\"" + BOUNDARY + "\"" + "\r\n");
		} else {
			if (isHtml) {
				sb.append("Content-Type: text/html; charset=\"" + CHARSET + "\"" + "\r\n");
			} else {
				sb.append("Content-Type: text/plain; charset=\"" + CHARSET + "\"" + "\r\n");
			}
		}

		if (isUrgent) {
			sb.append("X-Priority: 1\r\n");
		} else {
			sb.append("-Priority: 1\r\n");
		}
		if (!NEEDBoundary) {
			sb.append("Content-Transfer-Encoding: base64" + "\r\n");
			sb.append("\r\n");
			sb.append(Base64.encode(content.getBytes()));
			sb.append("\r\n");
			sb.append("\r\n");

		}

		if (NEEDBoundary) {
			sb.append("\r\n");
			sb.append("--" + BOUNDARY + "\r\n");
			sb.append("type=\"multipart/alternative\"");
			sb.append("Content-Transfer-Encoding: base64" + "\r\n");
			if (isHtml) {
				sb.append("Content-Type: text/html; charset=\"" + CHARSET + "\"" + "\r\n");
			} else {
				sb.append("Content-Type: text/plain; charset=\"" + CHARSET + "\"" + "\r\n");
			}
			sb.append("Content-Transfer-Encoding: base64" + "\r\n");
			sb.append("\r\n");
			sb.append(Base64.encode(content.getBytes()));
			sb.append("\r\n");
			sb.append("\r\n");
			sb.append("--" + BOUNDARY + "\r\n");
			// 正文结束开始附件E
			RandomAccessFile attachment = null;// 访问那些保存数据记录的文件的
			int fileIndex = 0;
			String fileName;
			int k;
			byte[] data;
			data = new byte[64];

			try {
				for (; fileIndex < attachments.length; fileIndex++) {
					sb.append("Content-Transfer-Encoding: base64" + "\r\n");
					fileName = attachments[fileIndex].getName();
					attachment = new RandomAccessFile(attachments[fileIndex], "r");
					sb.append("Content-Type: "
							+ MimeTypeFactory.getMimeType(fileName.indexOf(".") == -1 ? "*"
									: fileName.substring(fileName.lastIndexOf(".") + 1))
							+ "; name=\"" + fileName + "\"" + "\r\n");
					sb.append("Content-Disposition: attachment; filename=\"" + fileName + "\"" + "\r\n");
					/* 将文件编码成base64 */
					sb.append("\r\n");
					String sb2 = new String();
					sb2 = Base64.encode(SmtpSendMails.getBytes(attachments[fileIndex].getPath()));
					int k2 = 0;
					/* 大文件分段 */
					for (int i = 0; i < sb2.length(); i++) {
						if (i != 0 && i % 54 == 0)
							sb.append("\r\n");
						sb.append(sb2.charAt(i));
					}
					// sb.append(Base64.encode(getBytes(attachments[fileIndex].getPath()))
					// + "\r\n");
					sb.append("\r\n");
					sb.append("--" + BOUNDARY + "\r\n");
					path = ReadFile.WriteEMLFile(sb2.toString());
					return path;
				}
			} catch (FileNotFoundException e) {
				log.log("错误: 附件\"" + attachments[fileIndex].getAbsolutePath() + "\"不存在");

			} catch (IOException e) {
				log.log("错误: 无法读取附件\"" + attachments[fileIndex].getAbsolutePath() + "\"");

			} finally {
				if (attachment != null) {
					try {
						attachment.close();
					} catch (IOException e) {
					}
				}
			}

		}
		return path;

	}

	@FXML
	void event_cancel(ActionEvent event) {
		WriteEmail.root.getScene().getWindow().hide();
		((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉提示窗口
	}

	public WriteEmailTipsBox3() {

	}

	public WriteEmailTipsBox3(String tips) {
		mytips = tips;
	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(WriteEmailTipsBox3.class.getResource("WriteEmailtip_Box3.fxml"));
			root = loader.load();
			Pane box = new Pane();
			box.getChildren().add(root);
			box.setStyle("-fx-background:transparent;");
			Scene scene = new Scene(box, 397, 193);
			scene.setFill(null);// 设置Sence透明
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			// stage.setTitle("");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../img/login_logo.png")));
			// 鼠标拖拽位移函数
			UIMethods.paneDragMove(root, stage);
			stage.show();
			((Text) (root.lookup("#lab"))).setText(mytips);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);

	}

	@FXML
	private void initialize() {
		btn_close1.setOnMouseClicked(close);
		btn_close1.setOnMouseExited(exit);
		btn_close1.setOnMouseEntered(move);
		btn_close1.setOnMousePressed(press);
		btn_close1.setOnMouseReleased(relesed);

	}

	EventHandler<MouseEvent> close = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_2.png"));
			((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉提示窗口
		}

	};

	EventHandler<MouseEvent> press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_2.png"));
		}

	};
	EventHandler<MouseEvent> relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_1.png"));
		}

	};
	EventHandler<MouseEvent> exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_0.png"));
		}

	};
	EventHandler<MouseEvent> move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_1.png"));
		}

	};
}
