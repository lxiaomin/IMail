package cn.lxm.iMail.UI;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cn.lxm.iMail.Email.Email;
import cn.lxm.iMail.Email.EmailFJ;
import cn.lxm.iMail.Email.MakeupHtmlEmails;
import cn.lxm.iMail.Hibernate.DAO.ReceiveDAO;
import cn.lxm.iMail.Hibernate.DAO.SendDAO;
import cn.lxm.iMail.Hibernate.model.Receive;
import cn.lxm.iMail.Hibernate.model.Send;
import cn.lxm.iMail.Logic.LookUpEachEmails;
import cn.lxm.iMail.Util.CodeTranslate;
import cn.lxm.iMail.Util.UIMethods;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class lookEachotherEmails extends Application {

	// 声明变量
	public static Pane root;// 主面板
	static LookUpEachEmails LookUpEachEmails = new LookUpEachEmails();
	private String me = "13557330070@139.com", you = "598681919@qq.com", reallyYouName = "自己";
	public static List<Receive> list;
	static ReceiveDAO rd;
	static SendDAO sDao = new SendDAO();
	static int newEmailCount = 0;
	static String ClickID = "rebox_0";

	@FXML
	private Pane pane_top_bar;

	@FXML
	private ImageView img_Ire;

	@FXML
	private Label lab_Title;

	@FXML
	private Label lab_content;

	@FXML
	private Button btn_reSend;

	@FXML
	private Button btn_big;

	@FXML
	private Label lab_isend;

	@FXML
	private Label lab_time;

	@FXML
	private Button btn_delete;

	@FXML
	private VBox vbox_all1;

	@FXML
	private VBox vbox_all2;

	@FXML
	private WebView web_view_email;

	@FXML
	private Pane pane_messagesPreview;

	@FXML
	private Button btn_writeEmail1;

	@FXML
	private Button btn_writeEmail2;

	@FXML
	private Button btn_small;

	@FXML
	private Button btn_reEdit;

	@FXML
	private Button btn_replyTome;

	@FXML
	private VBox vbox_all;

	@FXML
	private ImageView img_iSEND;

	@FXML
	private Button btn_more;

	@FXML
	private Label lab_allEamils;

	@FXML
	private ImageView img_flag2;

	@FXML
	private Label lab_Sender;

	@FXML
	private Text label_who;

	@FXML
	private ImageView img_flag;

	@FXML
	private Label lab_irecive;

	@FXML
	private ScrollPane pane_messageBox;

	@FXML
	private ImageView img_allEmails;

	@FXML
	private Button btn_move;

	@FXML
	private Button btn_close;

	public lookEachotherEmails() {
		;
	}

	public lookEachotherEmails(String you, String me, String reallyYouName) {
		this.me = me;
		this.you = you;
		this.reallyYouName = reallyYouName;
	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(lookEachotherEmails.class.getResource("lookupEachOther2.fxml"));
			root = loader.load();
			Pane box = new Pane();
			box.getChildren().add(root);
			box.setStyle("-fx-background:transparent;");
			Scene scene = new Scene(box, 900, 680);
			scene.setFill(null);// 设置Sence透明
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			// stage.setTitle("");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../img/login_logo.png")));
			// 鼠标拖拽位移函数
			UIMethods.paneDragMove(root, stage);
			stage.show();
			LoadAllEmail(me, you);
			((Text) lookEachotherEmails.root.lookup("#label_who")).setText(reallyYouName);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);

	}

	@FXML
	private void initialize() {

		btn_reEdit.setGraphic(new ImageView("cn/lxm/iMail/img/editor.png"));
		// btn_writeEmail1.setGraphic(new
		// ImageView("cn/lxm/iMail/img/refresh0.png"));
		btn_writeEmail2.setGraphic(new ImageView("cn/lxm/iMail/img/reply.png"));
		btn_reSend.setGraphic(new ImageView("cn/lxm/iMail/img/forward.png"));
		btn_delete.setGraphic(new ImageView("cn/lxm/iMail/img/delete.png"));
		btn_move.setGraphic(new ImageView("cn/lxm/iMail/img/move_mail.png"));
		btn_replyTome.setGraphic(new ImageView("cn/lxm/iMail/img/white_feedback_0.png"));
		btn_small.setGraphic(new ImageView("cn/lxm/iMail/img/white_min_0.png"));
		btn_big.setGraphic(new ImageView("cn/lxm/iMail/img/white_max_0.png"));
		btn_close.setGraphic(new ImageView("cn/lxm/iMail/img/white_close_0.png"));
		btn_more.setGraphic(new ImageView("cn/lxm/iMail/img/more.png"));

		// 右上角按钮的效果事件
		btn_close.setOnMouseEntered(right_top_Img_move);
		btn_big.setOnMouseEntered(right_top_Img_move);
		btn_small.setOnMouseEntered(right_top_Img_move);
		btn_replyTome.setOnMouseEntered(right_top_Img_move);

		btn_close.setOnMousePressed(right_top_Img_press);
		btn_big.setOnMousePressed(right_top_Img_press);
		btn_small.setOnMousePressed(right_top_Img_press);
		btn_replyTome.setOnMousePressed(right_top_Img_press);

		btn_close.setOnMouseExited(right_top_Img_exit);
		btn_big.setOnMouseExited(right_top_Img_exit);
		btn_small.setOnMouseExited(right_top_Img_exit);
		btn_replyTome.setOnMouseExited(right_top_Img_exit);

		btn_close.setOnMouseReleased(right_top_Img_relesed);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);

		btn_close.setOnMouseClicked(right_top_Img_click_close);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseClicked(right_top_Img_click_showContact);

		// 剩下四个事件的绑定效果
		// 上边的6个按钮的显示效果,增加一个鼠标释放效果
		btn_reEdit.setOnMouseEntered(top_Img_move);
		btn_writeEmail1.setOnMouseEntered(top_Img_move);
		btn_writeEmail2.setOnMouseEntered(top_Img_move);
		btn_reSend.setOnMouseEntered(top_Img_move);
		btn_delete.setOnMouseEntered(top_Img_move);
		btn_move.setOnMouseEntered(top_Img_move);
		btn_more.setOnMouseEntered(top_Img_move);

		btn_reEdit.setOnMousePressed(top_Img_press);
		btn_writeEmail1.setOnMousePressed(top_Img_press);
		btn_writeEmail2.setOnMousePressed(top_Img_press);
		btn_reSend.setOnMousePressed(top_Img_press);
		btn_delete.setOnMousePressed(top_Img_press);
		btn_move.setOnMousePressed(top_Img_press);
		btn_more.setOnMousePressed(top_Img_press);

		btn_reEdit.setOnMouseExited(top_Img_exit);
		btn_writeEmail1.setOnMouseExited(top_Img_exit);
		btn_writeEmail2.setOnMouseExited(top_Img_exit);
		btn_reSend.setOnMouseExited(top_Img_exit);
		btn_delete.setOnMouseExited(top_Img_exit);
		btn_move.setOnMouseExited(top_Img_exit);
		btn_more.setOnMouseExited(top_Img_exit);

		btn_reEdit.setOnMouseReleased(top_Img_relesed);
		btn_writeEmail1.setOnMouseReleased(top_Img_relesed);
		btn_writeEmail2.setOnMouseReleased(top_Img_relesed);
		btn_reSend.setOnMouseReleased(top_Img_relesed);
		btn_delete.setOnMouseReleased(top_Img_relesed);
		btn_move.setOnMouseReleased(top_Img_relesed);
		btn_more.setOnMouseReleased(top_Img_relesed);

		// 全部邮件、我收到的、我发出的三个图片的初始状态，有两个是没有显示的，并且文字也是黑的
		img_Ire.setVisible(false);
		img_iSEND.setVisible(false);
		// 全部邮件、我收到的、我发出的三个图片的切换效果
		// 鼠标移入
		lab_allEamils.setOnMouseEntered(mails_move);
		lab_irecive.setOnMouseEntered(mails_move);
		lab_isend.setOnMouseEntered(mails_move);
		img_allEmails.setOnMouseEntered(mails_move);
		img_Ire.setOnMouseEntered(mails_move);
		img_iSEND.setOnMouseEntered(mails_move);
		// 鼠标点击
		lab_allEamils.setOnMouseClicked(mails_click);
		lab_irecive.setOnMouseClicked(mails_click);
		lab_isend.setOnMouseClicked(mails_click);
		img_allEmails.setOnMouseClicked(mails_click);
		img_Ire.setOnMouseClicked(mails_click);
		img_iSEND.setOnMouseClicked(mails_click);
		// 鼠标移出
		lab_allEamils.setOnMouseExited(mails_exit);
		lab_irecive.setOnMouseExited(mails_exit);
		lab_isend.setOnMouseExited(mails_exit);
		img_allEmails.setOnMouseExited(mails_exit);
		img_Ire.setOnMouseExited(mails_exit);
		img_iSEND.setOnMouseExited(mails_exit);

		// web载入邮件
		final WebEngine webEngine = web_view_email.getEngine();
		webEngine.load(new File("tinymce_4.5.5/tinymce/js/tinymce/viewer.html").toURI().toString());
	}

	// 右上角4个按钮的4个事件
	EventHandler<MouseEvent> right_top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			LookUpEachEmails.right_top_mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> right_top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			LookUpEachEmails.right_top_mouseEvent_press(node);
		}

	};
	public static EventHandler<MouseEvent> right_top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			LookUpEachEmails.right_top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			LookUpEachEmails.right_top_mouseEvent_relesed(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_click_close = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口
		}

	};
	EventHandler<MouseEvent> right_top_Img_click_showContact = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			LookUpEachEmails.right_top_Img_click_showContact(node);
		}

	};

	// 上面工具栏的4个事件
	EventHandler<MouseEvent> top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			LookUpEachEmails.top_mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			LookUpEachEmails.top_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			LookUpEachEmails.top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			LookUpEachEmails.top_mouseEvent_relesed(node);
		}

	};

	// 三个切换查看邮件的事件，3个，分别是进入、点击、退出
	EventHandler<MouseEvent> mails_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			String nodeName = ((Node) event.getSource()).getId();
			if (nodeName.equals("lab_allEamils") || nodeName.equals("img_allEmails")) {
				lab_allEamils.setStyle("-fx-text-fill:#60a3e6;");
				lab_irecive.setStyle("-fx-text-fill:#000;");
				lab_isend.setStyle("-fx-text-fill:#000;");
			}
			if (nodeName.equals("lab_irecive") || nodeName.equals("img_Ire")) {
				lab_allEamils.setStyle("-fx-text-fill:#000;");
				lab_irecive.setStyle("-fx-text-fill:#60a3e6;");
				lab_isend.setStyle("-fx-text-fill:#000;");
			}
			if (nodeName.equals("img_iSEND") || nodeName.equals("lab_isend")) {
				lab_allEamils.setStyle("-fx-text-fill:#000;");
				lab_irecive.setStyle("-fx-text-fill:#000;");
				lab_isend.setStyle("-fx-text-fill:#60a3e6;");
			}

		}

	};
	EventHandler<MouseEvent> mails_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			String nodeName = ((Node) event.getSource()).getId();
			System.out.println(nodeName);
			if (nodeName.equals("lab_allEamils") || nodeName.equals("img_allEmails")) {
				img_allEmails.setVisible(true);
				img_Ire.setVisible(false);
				img_iSEND.setVisible(false);
				lab_allEamils.setStyle("-fx-text-fill:#60a3e6;");
				lab_irecive.setStyle("-fx-text-fill:#000;");
				lab_isend.setStyle("-fx-text-fill:#000;");
				try {
					LoadAllEmail(me, you);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			if (nodeName.equals("lab_irecive") || nodeName.equals("img_Ire")) {
				img_allEmails.setVisible(false);
				img_Ire.setVisible(true);
				img_iSEND.setVisible(false);
				lab_allEamils.setStyle("-fx-text-fill:#000;");
				lab_irecive.setStyle("-fx-text-fill:#60a3e6;");
				lab_isend.setStyle("-fx-text-fill:#000;");
				try {
					LoadAllEmail(me, you);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (nodeName.equals("img_iSEND") || nodeName.equals("lab_isend")) {
				img_allEmails.setVisible(false);
				img_Ire.setVisible(false);
				img_iSEND.setVisible(true);
				lab_allEamils.setStyle("-fx-text-fill:#000;");
				lab_irecive.setStyle("-fx-text-fill:#000;");
				lab_isend.setStyle("-fx-text-fill:#60a3e6;");
				try {
					LoadIsendEmail(me, you);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

	};
	EventHandler<MouseEvent> mails_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {

		}

	};

	// 载入所有邮件
	private void LoadAllEmail(String me2, String you2) throws Exception {

		Receive receive = new Receive();
		rd = new ReceiveDAO();
		list = rd.UserAllEmail(me2, you2);
		String title, subject, content;
		VBox vBox = new VBox();
		vBox.setPrefWidth(330);
		vBox.setSpacing(5);
		vBox.setId("vbox_email");
		Email email = new Email();

		for (int i = list.size() - 1; i >= 0; i--) {
			// System.out.println(i);
			email = EmailFJ.DecomposeEmail(list.get(i).getEmailContent());

			Pane outPane = new Pane();
			outPane.setStyle("-fx-background-radius: 10px; -fx-background-color: #fafbfc;");
			outPane.setPrefWidth(330);
			outPane.setPrefHeight(98);
			outPane.setId("rebox_" + i);
			outPane.setOnMouseEntered(email_Enter);
			outPane.setOnMouseExited(email_Exit);
			outPane.setOnMouseClicked(email_click);

			Pane SenderPane = new Pane();
			SenderPane.setStyle("");
			SenderPane.setPrefWidth(280);
			SenderPane.setPrefHeight(25);
			SenderPane.setLayoutX(-1);
			SenderPane.setLayoutY(8);

			Label senderLabel = new Label();
			senderLabel.setLayoutX(18);
			senderLabel.setLayoutY(1);
			senderLabel.setPrefHeight(15);
			senderLabel.setPrefWidth(232);
			try {
				if (CodeTranslate.getEncoding(email.getFrom()).equals("UTF-8"))
					continue;
			} catch (Exception e) {
				continue;
			}

			// {email.setFrom(CodeTranslate.getUTF8XMLString(email.getFrom()));System.out.println(email.getFrom());}
			senderLabel.setText(email.getFrom());
			senderLabel.setTextFill(Color.rgb(50, 50, 50));
			senderLabel.setStyle("-fx-font-size:18px;-fx-font-weight: bold;");
			senderLabel.setWrapText(false);

			ImageView tipsImageView = new ImageView();
			tipsImageView.setImage(new Image("cn/lxm/iMail/img/1.png"));
			tipsImageView.setFitHeight(16);
			tipsImageView.setFitWidth(16);
			tipsImageView.setLayoutX(3);
			tipsImageView.setLayoutY(5);
			if (list.get(i).getAppendix() > 0) {
				tipsImageView.setVisible(true);
			} else
				tipsImageView.setVisible(false);
			tipsImageView.setId("Isnew_" + i);

			ImageView topImageView = new ImageView();
			if (list.get(i).getIsInFlagBox() > 0) {
				topImageView.setImage(new Image("cn/lxm/iMail/img/star1.png"));
				topImageView.setVisible(true);
			} else {
				topImageView.setImage(new Image("cn/lxm/iMail/img/star0.png"));
				topImageView.setVisible(false);
			}
			topImageView.setFitHeight(16);
			topImageView.setFitWidth(16);
			topImageView.setLayoutX(301);
			topImageView.setLayoutY(2);
			topImageView.setId("flag_" + i);
			topImageView.setCursor(Cursor.HAND);

			topImageView.setOnMouseClicked(flag_click);

			Label TitleLabel = new Label();
			TitleLabel.setLayoutX(17);
			TitleLabel.setLayoutY(42);
			TitleLabel.setPrefHeight(15);
			TitleLabel.setPrefWidth(281);
			TitleLabel.setText(email.getSubject());
			TitleLabel.setTextFill(Color.rgb(14, 14, 14));
			TitleLabel.setStyle("-fx-font-size:14px;");
			TitleLabel.setWrapText(false);

			ImageView centerImageView = new ImageView();
			centerImageView.setImage(new Image("cn/lxm/iMail/img/accountlist_inbox.png"));
			centerImageView.setFitHeight(16);
			centerImageView.setFitWidth(16);
			centerImageView.setLayoutX(301);
			centerImageView.setLayoutY(41);
			centerImageView.setId("IsAdd_" + i);
			centerImageView.setVisible(false);

			Label contentLabel = new Label();
			contentLabel.setLayoutX(17);
			contentLabel.setLayoutY(66);
			contentLabel.setPrefHeight(18);
			contentLabel.setPrefWidth(271);
			// 邮件有文字内容的话就放上去，没有的话提示用户没文字内容
			if (email.getReceived().equals(""))
				contentLabel.setText("此邮件没有文字内容");
			else
				contentLabel.setText(email.getReceived());
			contentLabel.setTextFill(Color.rgb(84, 84, 84));
			contentLabel.setStyle("-fx-font-size:13px;");
			contentLabel.setWrapText(false);

			Label Time = new Label();
			Time.setLayoutX(285);
			Time.setLayoutY(66);
			Time.setPrefHeight(18);
			Time.setPrefWidth(44);
			// 将时间分割后组装到时间标签中
			if (!email.getTime().equals("")) {
				String[] time = email.getTime().split("-");
				String[] time2 = time[2].split("\\s+");
				Time.setText(time[1] + "-" + time2[0]);
			}
			Time.setTextFill(Color.rgb(111, 111, 84));
			Time.setStyle("-fx-font-size:13px;");
			Time.setWrapText(false);

			SenderPane.getChildren().addAll(topImageView, tipsImageView);
			SenderPane.getChildren().add(senderLabel);
			outPane.getChildren().add(SenderPane);
			outPane.getChildren().addAll(TitleLabel, centerImageView, contentLabel, Time);
			vBox.getChildren().add(outPane);
		}
		((ScrollPane) lookEachotherEmails.root.lookup("#pane_messageBox")).setContent(vBox);
	}

	// 载入我发出的邮件
	private void LoadIsendEmail(String me2, String you2) throws Exception {

		Send send = new Send();
		List<Send> list;
		list = sDao.Isend(me2, you2);
		String title, subject, content;
		VBox vBox = new VBox();
		vBox.setPrefWidth(330);
		vBox.setSpacing(5);
		vBox.setId("vbox_email");
		Email email = new Email();

		for (int i = list.size() - 1; i >= 0; i--) {
			email = EmailFJ.DecomposeEmail(list.get(i).getEmailContent());

			Pane outPane = new Pane();
			outPane.setStyle("-fx-background-radius: 10px; -fx-background-color: #fafbfc;");
			outPane.setPrefWidth(330);
			outPane.setPrefHeight(98);
			outPane.setId("rebox_" + i);
			outPane.setOnMouseEntered(email_Enter);
			outPane.setOnMouseExited(email_Exit);
			outPane.setOnMouseClicked(email_click);

			Pane SenderPane = new Pane();
			SenderPane.setStyle("");
			SenderPane.setPrefWidth(280);
			SenderPane.setPrefHeight(25);
			SenderPane.setLayoutX(-1);
			SenderPane.setLayoutY(8);

			Label senderLabel = new Label();
			senderLabel.setLayoutX(18);
			senderLabel.setLayoutY(1);
			senderLabel.setPrefHeight(15);
			senderLabel.setPrefWidth(232);
			try {
				if (CodeTranslate.getEncoding(email.getFrom()).equals("UTF-8"))
					continue;
			} catch (Exception e) {
				continue;
			}

			// {email.setFrom(CodeTranslate.getUTF8XMLString(email.getFrom()));System.out.println(email.getFrom());}
			senderLabel.setText(email.getFrom());
			senderLabel.setTextFill(Color.rgb(50, 50, 50));
			senderLabel.setStyle("-fx-font-size:18px;-fx-font-weight: bold;");
			senderLabel.setWrapText(false);

			ImageView tipsImageView = new ImageView();
			tipsImageView.setImage(new Image("cn/lxm/iMail/img/1.png"));
			tipsImageView.setFitHeight(16);
			tipsImageView.setFitWidth(16);
			tipsImageView.setLayoutX(3);
			tipsImageView.setLayoutY(5);
			if (list.get(i).getAppendix() > 0) {
				tipsImageView.setVisible(true);
			} else
				tipsImageView.setVisible(false);
			tipsImageView.setId("Isnew_" + i);

			ImageView topImageView = new ImageView();
			topImageView.setImage(new Image("cn/lxm/iMail/img/star0.png"));
			topImageView.setVisible(false);
			topImageView.setFitHeight(16);
			topImageView.setFitWidth(16);
			topImageView.setLayoutX(301);
			topImageView.setLayoutY(2);
			topImageView.setId("flag_" + i);
			topImageView.setCursor(Cursor.HAND);

			topImageView.setOnMouseClicked(flag_click);

			Label TitleLabel = new Label();
			TitleLabel.setLayoutX(17);
			TitleLabel.setLayoutY(42);
			TitleLabel.setPrefHeight(15);
			TitleLabel.setPrefWidth(281);
			TitleLabel.setText(email.getSubject());
			TitleLabel.setTextFill(Color.rgb(14, 14, 14));
			TitleLabel.setStyle("-fx-font-size:14px;");
			TitleLabel.setWrapText(false);

			ImageView centerImageView = new ImageView();
			centerImageView.setImage(new Image("cn/lxm/iMail/img/accountlist_inbox.png"));
			centerImageView.setFitHeight(16);
			centerImageView.setFitWidth(16);
			centerImageView.setLayoutX(301);
			centerImageView.setLayoutY(41);
			centerImageView.setId("IsAdd_" + i);
			centerImageView.setVisible(false);

			Label contentLabel = new Label();
			contentLabel.setLayoutX(17);
			contentLabel.setLayoutY(66);
			contentLabel.setPrefHeight(18);
			contentLabel.setPrefWidth(271);
			// 邮件有文字内容的话就放上去，没有的话提示用户没文字内容
			if (email.getReceived().equals(""))
				contentLabel.setText("此邮件没有文字内容");
			else
				contentLabel.setText(email.getReceived());
			contentLabel.setTextFill(Color.rgb(84, 84, 84));
			contentLabel.setStyle("-fx-font-size:13px;");
			contentLabel.setWrapText(false);

			Label Time = new Label();
			Time.setLayoutX(285);
			Time.setLayoutY(66);
			Time.setPrefHeight(18);
			Time.setPrefWidth(44);
			// 将时间分割后组装到时间标签中
			if (!email.getTime().equals("")) {
				String[] time = email.getTime().split("-");
				String[] time2 = time[2].split("\\s+");
				Time.setText(time[1] + "-" + time2[0]);
			}
			Time.setTextFill(Color.rgb(111, 111, 84));
			Time.setStyle("-fx-font-size:13px;");
			Time.setWrapText(false);

			SenderPane.getChildren().addAll(topImageView, tipsImageView);
			SenderPane.getChildren().add(senderLabel);
			outPane.getChildren().add(SenderPane);
			outPane.getChildren().addAll(TitleLabel, centerImageView, contentLabel, Time);
			vBox.getChildren().add(outPane);
		}
		((ScrollPane) lookEachotherEmails.root.lookup("#pane_messageBox")).setContent(vBox);
	}

	// 标记邮件的事件
	public static EventHandler<MouseEvent> flag_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {

			String node = ((Node) event.getSource()).getId();
			String names[] = node.split("_");
			if (list.get(Integer.parseInt(names[1])).getIsInFlagBox() > 0) {
				((ImageView) lookEachotherEmails.root.lookup("#" + node)).setVisible(false);
				((ImageView) lookEachotherEmails.root.lookup("#" + node))
						.setImage(new Image("cn/lxm/iMail/img/star0.png"));
				list.get(Integer.parseInt(names[1])).setIsInFlagBox(0);
				rd.update(list.get(Integer.parseInt(names[1])));
			} else {
				((ImageView) lookEachotherEmails.root.lookup("#" + node)).setVisible(true);
				((ImageView) lookEachotherEmails.root.lookup("#" + node))
						.setImage(new Image("cn/lxm/iMail/img/star1.png"));
				list.get(Integer.parseInt(names[1])).setIsInFlagBox(1);
				rd.update(list.get(Integer.parseInt(names[1])));
			}

		}

	};

	// 三个邮件的点击、移动、离开事件
	public static EventHandler<MouseEvent> email_Enter = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			String node = ((Node) event.getSource()).getId();
			((Pane) lookEachotherEmails.root.lookup("#" + node))
					.setStyle("-fx-background-radius: 10px; -fx-background-color: #d9eafa;");
			String[] flagString = node.split("_");
			((ImageView) lookEachotherEmails.root.lookup("#flag_" + flagString[1])).setVisible(true);
		}

	};
	public static EventHandler<MouseEvent> email_Exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			String node = ((Node) event.getSource()).getId();
			((Pane) lookEachotherEmails.root.lookup("#" + node))
					.setStyle("-fx-background-radius: 10px; -fx-background-color: #fafbfc;");
			String[] flagString = node.split("_");
			if (list.get(Integer.parseInt(flagString[1])).getIsInFlagBox() <= 0)
				((ImageView) lookEachotherEmails.root.lookup("#flag_" + flagString[1])).setVisible(false);
		}

	};
	public static EventHandler<MouseEvent> email_click = new EventHandler<MouseEvent>() {
		String urlPath = "";

		@Override
		public void handle(MouseEvent event) {

			if (event.getClickCount() == 1) {
				String node = ((Node) event.getSource()).getId();
				String names[] = node.split("_");
				try {
					((Pane) lookEachotherEmails.root.lookup("#" + ClickID)).setOnMouseExited(email_Exit);
					((Pane) lookEachotherEmails.root.lookup("#" + ClickID))
							.setStyle("-fx-background-radius: 10px; -fx-background-color: #fafbfc;");
				} catch (Exception e) {
					System.out.println("找不到这个ID");
				}

				((Pane) lookEachotherEmails.root.lookup("#" + node))
						.setStyle("-fx-background-radius: 10px; -fx-background-color: #d9eafa;");
				((Pane) lookEachotherEmails.root.lookup("#" + node)).setOnMouseExited(null);
				ClickID = node;
				Email email = new Email();
				try {
					email = EmailFJ.DecomposeEmail(list.get(Integer.parseInt(names[1])).getEmailContent());
				} catch (NumberFormatException e1) {

					e1.printStackTrace();
				} catch (Exception e1) {

					e1.printStackTrace();
				}
				final WebEngine webEngine = ((WebView) lookEachotherEmails.root.lookup("#web_view_email")).getEngine();
				try {
					urlPath = MakeupHtmlEmails.MakeupEmail(email);
					webEngine.load(new File(urlPath).toURI().toString());
				} catch (IOException e) {
					e.printStackTrace();
				}

				// 去掉新邮件提醒
				if (list.get(Integer.parseInt(names[1])).getAppendix() > 0) {
					newEmailCount--;
					((ImageView) lookEachotherEmails.root.lookup("#" + "Isnew_" + Integer.parseInt(names[1])))
							.setVisible(false);
					((ImageView) lookEachotherEmails.root.lookup("#" + "Isnew_" + Integer.parseInt(names[1])))
							.setImage(null);
					list.get(Integer.parseInt(names[1])).setAppendix(Short.valueOf((short) 0));
					rd.update(list.get(Integer.parseInt(names[1])));
					if (newEmailCount > 0) {
						((Button) lookEachotherEmails.root.lookup("#btn_menu1")).setText("收件箱(" + newEmailCount + ")");
					} else {
						((Button) lookEachotherEmails.root.lookup("#btn_menu1")).setText("收件箱");
					}

				}

			}
			// 双击事件，弹出大邮件详情
			else if (event.getClickCount() == 2) {
				Platform.runLater(new Runnable() {
					public void run() {
						try {
							new EmailDetail().start(new Stage());
							final WebEngine webEngine = ((WebView) (EmailDetail.root.lookup("#webview_detail")))
									.getEngine();
							webEngine.load(new File(urlPath).toURI().toString());
						} catch (Exception e) {

							e.printStackTrace();
						}
					}

				});
			}
		}

	};

}
