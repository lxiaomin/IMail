package cn.lxm.iMail.UI;

import javafx.application.Application;
import javafx.scene.layout.Pane;

import java.io.File;

import cn.lxm.iMail.Logic.CalendarEvents;

import cn.lxm.iMail.Util.UIMethods;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Calendar extends Application {

	// 声明变量
	public static Pane root;// 主面板

	@FXML
	private Button btn_search;

	@FXML
	private WebView webview;

	@FXML
	private Pane pane_top_bar;

	@FXML
	private Button btn_writeEmail1;

	@FXML
	private Button btn_small;

	@FXML
	private Button btn_replyTome;

	@FXML
	private Pane pane_left_bar;

	@FXML
	private Button btn_big;

	@FXML
	private Button btn_message;

	@FXML
	private Button btn_friends;

	@FXML
	private Button btn_writeEmail;

	@FXML
	private Button btn_settings;

	@FXML
	private Button btn_close;

	@FXML
	void event_message(ActionEvent event) {

	}

	@FXML
	void event_friends(ActionEvent event) {

	}

	@FXML
	void event_search(ActionEvent event) {

	}

	@FXML
	void event_setting(ActionEvent event) {

	}

	public Calendar() {

	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(Calendar.class.getResource("calendar.fxml"));
			root = loader.load();
			Pane box = new Pane();
			box.getChildren().add(root);
			box.setStyle("-fx-background:transparent;");
			Scene scene = new Scene(box, 1158, 660);
			scene.setFill(null);// 设置Sence透明
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			// stage.setTitle("");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../img/login_logo.png")));
			// 鼠标拖拽位移函数
			UIMethods.paneDragMove(root, stage);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);

	}

	@FXML
	private void initialize() {

		btn_replyTome.setGraphic(new ImageView("cn/lxm/iMail/img/white_feedback_0.png"));
		btn_small.setGraphic(new ImageView("cn/lxm/iMail/img/white_min_0.png"));
		btn_big.setGraphic(new ImageView("cn/lxm/iMail/img/white_max_0.png"));
		btn_close.setGraphic(new ImageView("cn/lxm/iMail/img/white_close_0.png"));

		// 左边工具栏增加四个按钮的显示效果
		btn_message.setOnMouseEntered(left_Img_move);
		btn_friends.setOnMouseEntered(left_Img_move);
		// btn_search.setOnMouseEntered(left_Img_move);
		btn_settings.setOnMouseEntered(left_Img_move);

		btn_message.setOnMouseClicked(left_Img_press);
		btn_friends.setOnMouseClicked(left_Img_press);
		// btn_search.setOnMouseClicked(left_Img_press);
		btn_settings.setOnMouseClicked(left_Img_press);

		btn_message.setOnMouseExited(left_Img_exit);
		btn_friends.setOnMouseExited(left_Img_exit);
		// btn_search.setOnMouseExited(left_Img_exit);
		btn_settings.setOnMouseExited(left_Img_exit);

		// 上边的6个按钮的显示效果,增加一个鼠标释放效果
		btn_writeEmail.setOnMouseEntered(top_Img_move);
		btn_writeEmail.setOnMousePressed(top_Img_press);
		btn_writeEmail.setOnMouseExited(top_Img_exit);

		btn_writeEmail.setOnMouseReleased(top_Img_relesed);

		// 右上角按钮的效果事件
		btn_close.setOnMouseEntered(right_top_Img_move);
		btn_big.setOnMouseEntered(right_top_Img_move);
		btn_small.setOnMouseEntered(right_top_Img_move);
		btn_replyTome.setOnMouseEntered(right_top_Img_move);

		btn_close.setOnMousePressed(right_top_Img_press);
		btn_big.setOnMousePressed(right_top_Img_press);
		btn_small.setOnMousePressed(right_top_Img_press);
		btn_replyTome.setOnMousePressed(right_top_Img_press);

		btn_close.setOnMouseExited(right_top_Img_exit);
		btn_big.setOnMouseExited(right_top_Img_exit);
		btn_small.setOnMouseExited(right_top_Img_exit);
		btn_replyTome.setOnMouseExited(right_top_Img_exit);

		btn_close.setOnMouseReleased(right_top_Img_relesed);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);

		btn_close.setOnMouseClicked(right_top_Img_click_close);
		btn_big.setOnMouseReleased(right_top_Img_relesed);
		btn_small.setOnMouseReleased(right_top_Img_relesed);
		btn_replyTome.setOnMouseReleased(right_top_Img_relesed);

		// 载入浏览器
		final WebEngine webEngine = webview.getEngine();
		webEngine.load(new File("tinymce_4.5.5/tinymce/js/calendar/calendar.html").toURI().toString());

	}

	// 左边工具栏的三个事件
	EventHandler<MouseEvent> left_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> left_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.left_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> left_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.left_mouseEvent_exit(node);
		}

	};

	// 上面工具栏的4个事件
	EventHandler<MouseEvent> top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.top_mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.top_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.top_mouseEvent_relesed(node);
		}

	};

	// 右上角4个按钮的4个事件
	EventHandler<MouseEvent> right_top_Img_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.right_top_mouseEvent_ImgReplace(node);
		}

	};
	EventHandler<MouseEvent> right_top_Img_press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.right_top_mouseEvent_press(node);
		}

	};
	EventHandler<MouseEvent> right_top_Img_exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.right_top_mouseEvent_exit(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			CalendarEvents.right_top_mouseEvent_relesed(node);
		}

	};

	EventHandler<MouseEvent> right_top_Img_click_close = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口
		}

	};

}
