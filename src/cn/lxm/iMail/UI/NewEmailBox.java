package cn.lxm.iMail.UI;

import cn.lxm.iMail.BGM.ButtonBGM;
import cn.lxm.iMail.Util.UIMethods;
import javafx.animation.FadeTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class NewEmailBox extends Application {

	public static Pane root;// 主面板
	static String sender = "发件人", reciever = "收件人", content = "邮件内容";
	static ButtonBGM m=new ButtonBGM();

	@FXML
	private Text text_sender;
	@FXML
	private Text text_recieve;
	@FXML
	private Text text_content;

	@FXML
	private ImageView btn_close1;

	@FXML
	private Pane text_email;

	@FXML
	private Pane pane_newEmail;

	public NewEmailBox() {
	}

	public NewEmailBox(String recieve, String sender, String content) {
		this.sender = sender;
		this.reciever = recieve;
		this.content = content;
	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(NewEmailBox.class.getResource("newEmailBox.fxml"));
			root = loader.load();
			Pane box = new Pane();
			box.getChildren().add(root);
			box.setStyle("-fx-background:transparent;");
			Scene scene = new Scene(box, 397, 193);
			scene.setFill(null);// 设置Sence透明
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			// stage.setTitle("");
			stage.setResizable(true);// 能最大化
			stage.setX(1072);
			stage.setY(635);
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../img/login_logo.png")));
			// 鼠标拖拽位移函数
			UIMethods.paneDragMove(root, stage);
			stage.show();
			// 淡入淡出效果
			FadeAnimation(10000);
			((Text) (root.lookup("#text_sender"))).setText(sender);
			((Text) (root.lookup("#text_content"))).setText(content);
			((Text) (root.lookup("#text_recieve"))).setText(reciever);
			RegisterBGM();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void FadeAnimation(int time) {
		FadeTransition ft = new FadeTransition(Duration.millis(time), root);
		ft.setFromValue(1.0);
		ft.setToValue(0.000001);
		// ft.setCycleCount(Timeline.INDEFINITE);
		ft.play();

	}

	public static void main(String[] args) {
		launch(args);

	}

	@FXML
	private void initialize() {

		btn_close1.setOnMouseClicked(close);
		btn_close1.setOnMouseExited(exit);
		btn_close1.setOnMouseEntered(move);
		btn_close1.setOnMousePressed(press);
		btn_close1.setOnMouseReleased(relesed);
		pane_newEmail.setOnMouseExited(pane_out);
		pane_newEmail.setOnMouseEntered(pane_move);
		pane_newEmail.setOnMouseClicked(pane_click);

	}

	EventHandler<MouseEvent> close = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_2.png"));
			((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉提示窗口
		}

	};

	EventHandler<MouseEvent> press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_2.png"));
		}

	};
	EventHandler<MouseEvent> relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_1.png"));
		}

	};
	EventHandler<MouseEvent> exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_0.png"));
		}

	};
	EventHandler<MouseEvent> move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_1.png"));
		}

	};

	EventHandler<MouseEvent> pane_move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			pane_newEmail.setStyle("-fx-background-color:#d9eafa");
			FadeAnimation(100000);
		}

	};
	EventHandler<MouseEvent> pane_out = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			pane_newEmail.setStyle("-fx-background-color:#fafbfc");
			FadeAnimation(10000);
		}

	};
	EventHandler<MouseEvent> pane_click = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			;
		}

	};
	private void RegisterBGM() {
		new Thread() {
			public void run() {
				m.PlayBubblesMusic();
			}
		}.start();
	}

}
