package cn.lxm.iMail.UI;

import com.sun.xml.internal.ws.org.objectweb.asm.Label;

import cn.lxm.iMail.Util.UIMethods;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class WriteEmailTipsBox2 extends Application {

	// 声明变量
	public static Pane root;// 主面板
	private String mytips="";
	@FXML
	private ImageView btn_close1;

	@FXML
	private Button btn_save;

	@FXML
	private Text lab;

	@FXML
	void event_save(ActionEvent event) {
		((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口
	}

	public WriteEmailTipsBox2() {

	}

	public WriteEmailTipsBox2(String tips) {
		mytips=tips;
	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(WriteEmailTipsBox2.class.getResource("WriteEmailtip_Box2.fxml"));
			root = loader.load();
			Pane box = new Pane();
			box.getChildren().add(root);
			box.setStyle("-fx-background:transparent;");
			Scene scene = new Scene(box, 397, 193);
			scene.setFill(null);// 设置Sence透明
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			// stage.setTitle("");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../img/login_logo.png")));
			// 鼠标拖拽位移函数
			UIMethods.paneDragMove(root, stage);
			stage.show();
			((Text)(root.lookup("#lab"))).setText(mytips);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);

	}

	@FXML
	private void initialize() {
		btn_close1.setOnMouseClicked(close);
		btn_close1.setOnMouseExited(exit);
		btn_close1.setOnMouseEntered(move);
		btn_close1.setOnMousePressed(press);
		btn_close1.setOnMouseReleased(relesed);


	}
	EventHandler<MouseEvent> close = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_2.png"));
			((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉提示窗口
		}

	};

	EventHandler<MouseEvent> press = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_2.png"));
		}

	};
	EventHandler<MouseEvent> relesed = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_1.png"));
		}

	};
	EventHandler<MouseEvent> exit = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_0.png"));
		}

	};
	EventHandler<MouseEvent> move = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			btn_close1.setImage(new Image("cn/lxm/iMail/img/black_close_1.png"));
		}

	};

}
