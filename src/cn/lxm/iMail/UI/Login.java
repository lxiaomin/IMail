package cn.lxm.iMail.UI;

import java.io.File;
import java.util.concurrent.ExecutionException;

import cn.lxm.iMail.Email.AccountManage;
import cn.lxm.iMail.Email.ConnectWithPop3;
import cn.lxm.iMail.Email.EmailUser;
import cn.lxm.iMail.Hibernate.DAO.LoginlogDAO;
import cn.lxm.iMail.Hibernate.model.Loginlog;
import cn.lxm.iMail.Util.UIMethods;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Login extends Application {

	// 声明变量
	public static Pane root;// 主面板
	static boolean isvalid = true;
	private AccountManage ac = new AccountManage();
	private LoginlogDAO lld = new LoginlogDAO();
	public static EmailUser eu = new EmailUser();
	public static boolean isExit=false;
	Loginlog lg;
	@FXML
	private TextField textBox_name;

	@FXML
	private ImageView btn_setting;

	@FXML
	private PasswordField textbox_psw;

	@FXML
	private ImageView img_loading;

	@FXML
	private Button btn_login;

	@FXML
	private Label lab_tips;

	@FXML
	private ImageView btn_close;

	@FXML
	void event_login(ActionEvent event) {
		String host = textBox_name.getText().substring(textBox_name.getText().trim().indexOf('@') + 1);
		boolean flag = AccountManage.IsConnectToPOP3Server(host, textBox_name.getText(), textbox_psw.getText());
		if (flag) {
			eu.setUserName(textBox_name.getText());
			eu.setHost("pop3." + textBox_name.getText().split("@")[1]);
			eu.setUserPsw(textbox_psw.getText());
			// 如果登陆相应成功
			lg = lld.getByEmail(textBox_name.getText());

			if (lg!= null) {
				// 如果不是第一次登陆的话

				// 启动主界面
				Platform.runLater(new Runnable() {
					public void run() {
						try {
							eu.setEmailCount(Integer.parseInt(lg.getLoginTime()));
							new Index().start(new Stage());
							((Button)Index.root.lookup("#btn_menu0")).setText(eu.getUserName());//设置现在登陆的用户名
							((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				});
			} else {
				// 如果是第一次登陆的话，先下载邮件，再打开主界面
				lg=new Loginlog();
				try {
					eu.setEmailCount(0);
					ConnectWithPop3.downLoadAllEmailFromPOP3(eu);
					lg.setToEmail(eu.getUserName());
					eu.setEmailCount(ConnectWithPop3.TotalCount);
					lg.setLoginTime(String.valueOf(ConnectWithPop3.TotalCount));
					lld.save(lg);
					Platform.runLater(new Runnable() {
						public void run() {
							try {
								new Index().start(new Stage());
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

					});
				} catch (InterruptedException e) {

					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
			loading();
			lab_tips.setText("");
			return;
		} else {
			lab_tips.setText("您输入的邮箱账号或密码错误");
			textBox_name.requestFocus();
			loading();
		}
	}

	private void testTextBoxIsFull() {
		new Thread() {
			public void run() {
				while (true) {
					Platform.runLater(new Runnable() {
						public void run() {
							if (textBox_name.getText().trim().equals("") || textbox_psw.getText().trim().equals("")) {
								btn_login.setDisable(true);
							} else {
								btn_login.setDisable(false);
							}
						}
					});
					try {
						sleep(1000);
					} catch (InterruptedException e) {

						e.printStackTrace();
					}

				}
			}
		}.start();
	}

	@FXML
	private void initialize() {
		btn_login.setOnMouseEntered(moveEventHandler);
		btn_setting.setOnMouseEntered(moveEventHandler);
		btn_close.setOnMouseEntered(moveEventHandler);

		btn_login.setOnMousePressed(pressEventHandler);
		btn_setting.setOnMousePressed(pressEventHandler);
		btn_close.setOnMousePressed(pressEventHandler);

		btn_login.setOnMouseReleased(relesedEventHandler);
		btn_setting.setOnMouseReleased(relesedEventHandler);
		btn_close.setOnMouseReleased(relesedEventHandler);

		btn_login.setOnMouseExited(exitEventHandler);
		btn_setting.setOnMouseExited(exitEventHandler);
		btn_close.setOnMouseExited(exitEventHandler);

		// btn_login.setOnMouseEntered(moveEventHandler);
		// btn_setting.setOnMouseEntered(moveEventHandler);
		btn_close.setOnMouseClicked(close_eventEventHandler);

		// 取消一开始焦点集中
		textBox_name.setFocusTraversable(true);
		// 检测文本框是否有内容
		testTextBoxIsFull();
		// 设置提示隐藏
		lab_tips.setVisible(false);
		// loading图片隐藏
		img_loading.setVisible(false);

		textBox_name.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

				if (textBox_name.isFocused()) {

				} else if (textBox_name.isFocusTraversable()) {
					lab_tips.setVisible(true);
					loading();
					if ((!textBox_name.getText().trim().equals(""))) {

						lab_tips.setVisible(true);
						lab_tips.setText("");
						if (!ac.IsValid(textBox_name.getText().trim())) {

							lab_tips.setVisible(true);
							lab_tips.setText("您输入的邮箱格式不正确，请核对后重新输入");
							textBox_name.requestFocus();
							loading();

						} else {

							new Thread(){
								public  void run(){
									isExit=ac.accountIsExit(textBox_name.getText().trim());
								}
							}.start();

							if (!isExit) {
								lab_tips.setVisible(true);
								//textBox_name.requestFocus();
								lab_tips.setText("您输入的邮箱地址不存在，请核对后重新输入");
								loading();

							} else {
								lab_tips.setVisible(true);

								loading();
								return;

							}
						}

					}

				}

			}

		});

	}

	private void loading() {
		new Thread() {
			public void run() {
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Platform.runLater(new Runnable() {
					public void run() {

						img_loading.setVisible(false);
					}
				});

			}
		}.start();

	}

	@Override
	public void start(Stage stage) throws Exception {

		try {

			FXMLLoader loader = new FXMLLoader(Login.class.getResource("login.fxml"));
			root = loader.load();
			Pane box = new Pane();
			box.getChildren().add(root);
			box.setStyle("-fx-background:transparent;");
			Scene scene = new Scene(box, 314, 394);
			scene.setFill(null);// 设置Sence透明
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.setScene(scene);
			// stage.setTitle("");
			stage.setResizable(true);// 能最大化
			stage.centerOnScreen();// 屏幕正中间
			stage.getIcons().add(new Image(getClass().getResourceAsStream("../img/login_logo.png")));
			// 鼠标拖拽位移函数
			UIMethods.paneDragMove(root, stage);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);

	}

	// 右上角2个按钮的6个事件
	EventHandler<MouseEvent> moveEventHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			String name = node.getId();
			if (name.equals("btn_login")) {
				((Button) Login.root.lookup("#" + name)).setStyle(
						"-fx-background-color: #4f90d1; -fx-font-family: Microsoft YaHei; -fx-text-fill: #fff; -fx-background-radius: 0; -fx-font-size: 14px;");
			} else if (name.equals("btn_setting")) {
				((ImageView) root.lookup("#" + name)).setImage(new Image("cn/lxm/iMail/img/proxy_1.png"));
			} else if (name.equals("btn_close")) {
				((ImageView) root.lookup("#" + name)).setImage(new Image("cn/lxm/iMail/img/black_close_1.png"));
			}

		}

	};
	EventHandler<MouseEvent> pressEventHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			String name = node.getId();
			if (name.equals("btn_login")) {
				((Button) Login.root.lookup("#" + name)).setStyle(
						"-fx-background-color: #4585c5; -fx-font-family: Microsoft YaHei; -fx-text-fill: #fff; -fx-background-radius: 0; -fx-font-size: 14px;");
			} else if (name.equals("btn_setting")) {
				((ImageView) root.lookup("#" + name)).setImage(new Image("cn/lxm/iMail/img/proxy_2.png"));
			} else if (name.equals("btn_close")) {
				((ImageView) root.lookup("#" + name)).setImage(new Image("cn/lxm/iMail/img/black_close_2.png"));
			}

		}

	};
	EventHandler<MouseEvent> relesedEventHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			String name = node.getId();
			if (name.equals("btn_login")) {
				((Button) Login.root.lookup("#" + name)).setStyle(
						"-fx-background-color: #4f90d1; -fx-font-family: Microsoft YaHei; -fx-text-fill: #fff; -fx-background-radius: 0; -fx-font-size: 14px;");
			} else if (name.equals("btn_setting")) {
				((ImageView) root.lookup("#" + name)).setImage(new Image("cn/lxm/iMail/img/proxy_1.png"));
			} else if (name.equals("btn_close")) {
				((ImageView) root.lookup("#" + name)).setImage(new Image("cn/lxm/iMail/img/black_close_1.png"));
			}

		}

	};

	EventHandler<MouseEvent> exitEventHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();
			String name = node.getId();
			if (name.equals("btn_login")) {
				((Button) Login.root.lookup("#" + name)).setStyle(
						"-fx-background-color: #60a3e6; -fx-font-family: Microsoft YaHei; -fx-text-fill: #fff; -fx-background-radius: 0; -fx-font-size: 14px;");
			} else if (name.equals("btn_setting")) {
				((ImageView) root.lookup("#" + name)).setImage(new Image("cn/lxm/iMail/img/proxy_0.png"));
			} else if (name.equals("btn_close")) {
				((ImageView) root.lookup("#" + name)).setImage(new Image("cn/lxm/iMail/img/black_close_0.png"));
			}

		}

	};

	EventHandler<MouseEvent> close_eventEventHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			((Node) (event.getSource())).getScene().getWindow().hide();// 隐藏掉登陆窗口
		}

	};

}
