# 基于Java Socket的邮件客户端开发

[![N|Solid](http://git.oschina.net/lxiaomin/IMail/raw/master/src/cn/lxm/iMail/img/1.png)]()
# 计算机网络课程设计

  - 基于JavaFx平台开发
  - Socket编程直接与POP3、SMTP服务器交互
  - MIME邮件组织结构

# 界面设计

  - 模仿网易简洁、大方的设计风格
  - 邮件编辑器使用TinyMCE并修改了一下画风

# 功能简介

  - 收发邮件
  - 邮件分类
  - 群发邮件
  - 通讯录
  - 日程安排
  - 往来邮件

 - 软件截图
 ### 登陆

 [![N|Solid](http://git.oschina.net/lxiaomin/IMail/raw/master/Screenshot/6.png)]()
 
 ### 主界面  
 
 [![N|Solid](http://git.oschina.net/lxiaomin/IMail/raw/master/Screenshot/8.png)]()
 
 ### 邮件详情  
 
 [![N|Solid](http://git.oschina.net/lxiaomin/IMail/raw/master/Screenshot/1.png)]()
 
 ### 发送邮件
 
 [![N|Solid](http://git.oschina.net/lxiaomin/IMail/raw/master/Screenshot/2.png)]()
 
 [![N|Solid](http://git.oschina.net/lxiaomin/IMail/raw/master/Screenshot/5.png)]()
 
 [![N|Solid](http://git.oschina.net/lxiaomin/IMail/raw/master/Screenshot/9.png)]()
 
 ### 通讯录
 
 [![N|Solid](http://git.oschina.net/lxiaomin/IMail/raw/master/Screenshot/3.png)]()
 
 ### 往来邮件
 
 [![N|Solid](http://git.oschina.net/lxiaomin/IMail/raw/master/Screenshot/3.png)]()
 
 ### 日历
 
 [![N|Solid](http://git.oschina.net/lxiaomin/IMail/raw/master/Screenshot/4.png)]()
 
 
 
 
  

